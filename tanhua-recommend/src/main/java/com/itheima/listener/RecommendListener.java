package com.itheima.listener;

import com.itheima.domain.mongo.MovementScore;
import com.itheima.domain.mongo.VideoScore;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class RecommendListener {

    @Autowired
    private MongoTemplate mongoTemplate;

    @RabbitListener(queuesToDeclare = @Queue("tanhua.recommend.movement"))
    public void listenRecommendMovement(MovementScore movementScore){
        // 去队列中拉取消息
        System.out.println("监听推荐动态队列开始");
        // 设置保存时间
        movementScore.setDate(System.currentTimeMillis());
        System.out.println("监听推荐动态队列结束");

        // 保存到mongo
        mongoTemplate.save(movementScore);
    }

    @RabbitListener(queuesToDeclare = @Queue("tanhua.recommend.video"))
    public void listenRecommendVideo(VideoScore videoScore){
        // 去队列中拉取消息
        System.out.println("监听推荐视频队列开始");
        // 设置保存时间
        videoScore.setDate(System.currentTimeMillis());
        System.out.println("监听推荐视频队列结束");

        // 保存到mongo
        mongoTemplate.save(videoScore);
    }

}
