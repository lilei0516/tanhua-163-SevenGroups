package com.itheima.job;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.RecommendMovement;
import com.itheima.domain.mongo.RecommendVideo;
import com.itheima.domain.mongo.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component  // 交给容器
public class RecommendJob {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MongoTemplate mongoTemplate;

    // 定时任务，指定间隔时间更新推荐用户表
    @Scheduled(cron = "0 0 0/1 * * ?")
    // @Scheduled(cron = "0/30 * * * * ?")
    public void RecommendMovementTask(){

        Set<String> keys = stringRedisTemplate.keys("QUANZI_PUBLISH_RECOMMEND_*");

        // 判空遍历
        if (CollectionUtil.isNotEmpty(keys)) {
            for (String key : keys) {
                // 拿到用户id
                String userId = key.replaceAll("QUANZI_PUBLISH_RECOMMEND_", "");

                // 去清空这个用户的推荐表
                mongoTemplate.remove(Query.query(Criteria.where("userId").is(Long.parseLong(userId)))
                        , RecommendMovement.class);

                // 拿到这个key对应的value
                String value = stringRedisTemplate.opsForValue().get(key);

                // 分割这个value
                String[] pids = value.split(",");

                // 判空遍历
                if (ArrayUtil.isNotEmpty(pids)) {
                    for (String pid : pids) {

                        // 封装到recommendMovement
                        RecommendMovement recommendMovement = new RecommendMovement();
                        // 推荐动态的时间
                        recommendMovement.setCreated(System.currentTimeMillis());
                        // 推荐的用户id
                        recommendMovement.setUserId(Long.parseLong(userId));
                        // 动态pid
                        recommendMovement.setPid(Long.parseLong(pid));
                        // 根据pid查询动态详情
                        Movement movement = mongoTemplate.findOne(Query.query(Criteria.where("pid").is(Long.parseLong(pid)))
                                , Movement.class);
                        // 动态主键id
                        recommendMovement.setPublishId(movement.getId());
                        // 推荐得分（写死）
                        recommendMovement.setScore(RandomUtil.randomDouble(60,90));

                        // 存入推荐动态表
                        mongoTemplate.save(recommendMovement);

                    }
                }
            }
        }
    }

    // 定时任务，指定间隔时间更新推荐视频表
    @Scheduled(cron = "0 0 0/1 * * ?")
    // @Scheduled(cron = "0/30 * * * * ?")
    public void RecommendVideoTask(){

        // 拿到redis中的所有key
        Set<String> keys = stringRedisTemplate.keys("QUANZI_VIDEO_RECOMMEND_*");

        // 判空遍历
        if (CollectionUtil.isNotEmpty(keys)) {
            for (String key : keys) {

                // 取出用户id
                long userId = Long.parseLong(key.replaceAll("QUANZI_VIDEO_RECOMMEND_", ""));

                // 删除之前的推荐视频
                mongoTemplate.remove(Query.query(
                        Criteria.where("userId").is(userId)
                ),RecommendVideo.class);

                // 获取到key对应的values
                String values = stringRedisTemplate.opsForValue().get(key);

                // 对values进行分割
                String[] vids = values.split(",");

                // 判空遍历
                if (ArrayUtil.isNotEmpty(vids)) {
                    for (String vid : vids) {
                        // 封装推荐视频
                        RecommendVideo recommendVideo = new RecommendVideo();
                        // 推荐时间
                        recommendVideo.setDate(System.currentTimeMillis());
                        // 大数据vid
                        recommendVideo.setVid(Long.parseLong(vid));
                        // 推荐目标人的userid
                        recommendVideo.setUserId(userId);
                        // 查询这个视频详情
                        Video video = mongoTemplate.findOne(Query.query(
                                Criteria.where("vid").is(Long.parseLong(vid))),
                                Video.class);
                        // 视频id
                        recommendVideo.setVideoId(video.getId());
                        // 推荐得分（写死）
                        recommendVideo.setScore(RandomUtil.randomDouble(60,90));

                        // 将rv保存到数据库
                        mongoTemplate.save(recommendVideo);
                    }
                }

            }
        }
    }

}
