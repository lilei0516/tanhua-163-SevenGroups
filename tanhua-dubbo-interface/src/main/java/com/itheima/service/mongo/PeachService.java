package com.itheima.service.mongo;

import com.itheima.domain.mongo.PickupCount;
import com.itheima.domain.mongo.Sound;

import java.util.List;

public interface PeachService {
    void saveSound(Sound sound);

    List<Sound> getPeachblossom();

    PickupCount findPickupCount(Long soundUserId);

    void save(PickupCount pickupCount);

    void resetCount();
}
