package com.itheima.service.mongo;

import com.itheima.vo.PageBeanVo;

public interface MyStatisticsService {

    // 查询互相喜欢的数量（好友数量）
    Long findEachLoveCount(Long userId);

    // 查询喜欢的数量（我喜欢的人）
    Long findLoveCount(Long userId);

    // 查询粉丝数量（喜欢我的人）
    Long findFanCount(Long userId);


    // 分页查询我喜欢的人
    PageBeanVo findLoveUser(Integer pageNum,Integer pageSize,Long userId);

    // 分页查询喜欢我的人
    PageBeanVo findFanUser(Integer pageNum,Integer pageSize,Long userId);

    // 分页查询历史访客
    PageBeanVo findVisitor(Integer pageNum,Integer pageSize,Long userId);

    // 判断对方是否喜欢我
    Boolean findLike(Long userId,Long fansId);

}
