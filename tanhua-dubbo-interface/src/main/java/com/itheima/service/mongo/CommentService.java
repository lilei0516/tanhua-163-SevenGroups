package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;

public interface CommentService {

    // 保存comment
    Integer saveComment(Comment comment);
    // 删除comment
    Integer removeComment(ObjectId publishId, Long userId, Integer commentType);

    PageBeanVo findCommentsListByPage(Integer pageNum, Integer pageSize, ObjectId objectId, int i);

    PageBeanVo findCommentVoByPage(Long userId, int contentType, Integer pageNum, Integer pageSize);
    // 分页查询视频评论
    PageBeanVo findByPage(ObjectId publishId, Integer commentType, Integer pageNum, Integer pageSize);

}
