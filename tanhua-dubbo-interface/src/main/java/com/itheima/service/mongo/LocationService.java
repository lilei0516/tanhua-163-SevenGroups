package com.itheima.service.mongo;

import com.itheima.domain.mongo.UserLocation;
import com.itheima.vo.UserLocationWithInfoVo;

import java.util.List;
import java.util.Map;

public interface LocationService {
    void saveNearUserLocation(Double latitude, Double longitude, String addrStr, Long userId);

    List<Long> serarchNearUser(String gender, Long distance, Long userId);
    //根据userId查询对应用户地理信息及用户详细信息
    UserLocationWithInfoVo findByUserId(Long userId);
    //根据id查询用户地理位置
    Map<String,Double> findLocation(Long id);
}
