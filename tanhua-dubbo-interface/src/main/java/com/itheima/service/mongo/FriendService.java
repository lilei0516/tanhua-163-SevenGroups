package com.itheima.service.mongo;

import com.itheima.domain.mongo.Friend;
import com.itheima.vo.PageBeanVo;

import java.util.List;

public interface FriendService {
    void addContacts(Long userId, Long friendId);

    PageBeanVo findMyFriendByPage(Integer pageNum, Integer pageSize, Long userId);


    // 不分页查询好友列表(app端探花卡片功能使用)
    List<Friend> findCards(Long userId);


    // 删除好友
    void deleteFriend(Long userId,Long friendId);

    // 删除好友动态
    void deleteFriendDynamic(Long userId,Long friendId);

}
