package com.itheima.service.mongo;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.vo.PageBeanVo;

public interface RecommendUserService {
    RecommendUser getTodayBest(Long toUserId);

    PageBeanVo getRecommendationList(Long toUserId, Integer pageNum, Integer pageSize);

    RecommendUser getRecommendUserDetail(Long userId, Integer recommendUserId);
}
