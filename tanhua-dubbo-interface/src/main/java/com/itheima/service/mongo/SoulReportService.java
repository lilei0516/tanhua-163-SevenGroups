package com.itheima.service.mongo;

import com.itheima.domain.mongo.SoulReport;
import org.bson.types.ObjectId;

import java.util.List;

public interface SoulReportService {
    void saveReport(SoulReport report);

    SoulReport findReport(Long userId, Long questionnaireId);

    SoulReport findReportById(ObjectId reportId);

    List<SoulReport> findSimilarYouList(Long conclusionId);
}
