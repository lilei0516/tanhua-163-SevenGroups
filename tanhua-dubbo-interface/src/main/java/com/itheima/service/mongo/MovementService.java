package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;

public interface MovementService {
    void saveMovement(Movement movement);

    PageBeanVo getMyMovements(Integer pageNum, Integer pageSize, Long userId);

    PageBeanVo getFriendMovements(Integer pageNum, Integer pageSize, Long userId);

    PageBeanVo getRecommendMovements(Integer pageNum, Integer pageSize, Long userId);

    Movement findMovementById(ObjectId publishId);

    PageBeanVo findMovementByPage(Integer pageNum, Integer pageSize, Long uid, Integer state);

    Comment commentLike(ObjectId objectId);

    void saveComment(Comment comment, Comment commentLikeForThis);

    void removeLikeCount(ObjectId objectId, Long userId, Comment comment);

    void update(Movement movement);

}
