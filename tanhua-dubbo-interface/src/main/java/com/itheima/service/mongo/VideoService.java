package com.itheima.service.mongo;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.FocusUser;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.Video;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;

public interface VideoService {

    PageBeanVo getSmallVideos(Integer pageNum, Integer pageSize, Long userId);

    void saveSmallVideos(Video video);

    void userFocus(FocusUser focusUser);

    void userUnFocus(Long userId, Long focusUserId);

    //查询小视频推荐列表
    PageBeanVo getMySmallVideos(Integer pageNum, Integer pageSize, Long userId);

    //查询小视频详情
    Video findVideoById(ObjectId videoId);


    Comment commentLike(ObjectId objectId);
    void saveComment(Comment comment, Comment commentLikeForThis);

    void removeLikeCount(ObjectId objectId, Long userId, Comment comment);

    void update(Video video);

}
