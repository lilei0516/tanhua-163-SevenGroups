package com.itheima.service.mongo;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;

import java.util.List;

public interface CardsService {

    // 查询探花卡片推荐用户
    List<RecommendUser> findCards(Long toUserId);

    // 卡片右滑（查询喜欢表）
    UserLike findCardsLove(Long userId,Long likeUserId);

    // 喜欢以后添加进喜欢表
    void saveCardsLove(UserLike userLike);

    // 删除用户推荐表中的数据
    void deleteRecommendUser(Long userId,Long toUserId);

    // 删除喜欢表中的数据
    void deleteCardsLove(Long userId,Long noLikeUserId);



}
