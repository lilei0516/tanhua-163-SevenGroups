package com.itheima.service.db;

import com.itheima.vo.PageBeanVo;

public interface AnnouncementService {
    PageBeanVo findAnnouncementsByPage(Integer pageNum, Integer pageSize);
}
