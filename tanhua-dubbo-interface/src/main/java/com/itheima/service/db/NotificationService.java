package com.itheima.service.db;

import com.itheima.domain.db.Notification;

public interface NotificationService {
    Notification findNotificationById(Long id);

    void save(Notification notification);

    void update(Notification notificationById);
}
