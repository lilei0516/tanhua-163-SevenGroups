package com.itheima.service.db;

import com.itheima.vo.PageBeanVo;

public interface BlackListService {
    PageBeanVo getBlackList(Long userId,Integer pageNum, Integer pageSize);

    void deleteOne(Long userId, Long blackUId);
}
