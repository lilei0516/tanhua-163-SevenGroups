package com.itheima.service.db;

import com.itheima.domain.db.UserInfo;
import com.itheima.vo.PageBeanVo;

public interface UserInfoService {
    void save(UserInfo userInfo);

    void addPhoto(UserInfo userInfo);

    UserInfo findUserInfoById(Long id);

    void update(UserInfo userInfo);

    PageBeanVo findUserByPage(Integer pageNum, Integer pageSize);


    // app探花卡片功能查询个人信息
    UserInfo findCards(Long id,Long[] notId);
}
