package com.itheima.service.db;

import com.itheima.domain.db.Admin;
import com.itheima.vo.UserLocationWithInfoVo;

public interface AdminService {
    //根据用户名查找用户
    Admin getAdminByUsername(String username);
}
