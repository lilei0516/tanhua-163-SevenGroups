package com.itheima.service.db;

import com.itheima.vo.AnalysisSummaryVo;
import com.itheima.vo.DataVo;

public interface AnalysisByDayService {

    // 分析每日数据
    void logToAnalysisByDay();

    AnalysisSummaryVo summary();

    //查找数据
    Object[] findData(String startTime, String endTime, Integer type);
}
