package com.itheima.service.db;

import com.itheima.domain.db.SoulConclusion;
import com.itheima.domain.db.SoulExamination;
import com.itheima.domain.db.SoulOptions;
import com.itheima.domain.db.SoulQuestionnaire;

import java.util.List;

public interface TestSoulService {
    List<SoulQuestionnaire> findQuestionnaire();

    List<SoulExamination> findExamination(Long questionnaireId);

    List<SoulOptions> findOptions(Long examinationId);

    Integer findScore(String questionId, String optionId);

    Long findQuestionnaireId(String questionId);

    Long findConclusionId(Integer totalScore);

    SoulConclusion findConclusionById(Long conclusionId);
}
