package com.itheima.service.db;

import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;

public interface UserService {

    //保存用户(并返回主键)
    Long save(User user);
    //根据手机号查询用户
    User findUserByPhone(String phone);

    // 根据用户id查询手机号
    User findUserById(Long userId);

    // 修改手机号
    void updatePhone(User user);

}
