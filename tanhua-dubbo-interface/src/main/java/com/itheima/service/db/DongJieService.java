package com.itheima.service.db;

import com.itheima.domain.db.DongJie;

import java.util.List;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-11 16:49
 * @version:
 */
public interface DongJieService {

    // 用户冻结
    void dongJie(DongJie dongJie);

    // 更新冻结用户信息
    void update(DongJie dongJie);

    // 查询冻结用户
    List<DongJie> findDjById(Long userId);
}
