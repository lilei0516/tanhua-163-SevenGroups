package com.itheima.service.db;

import com.itheima.domain.db.SoulExamination;
import com.itheima.domain.db.SoulOptions;
import com.itheima.domain.db.SoulQuestionnaire;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SoulExaminationVoW;

import java.util.List;

/**
 * @Author wzh
 * @Date 2021/11/12 15:20
 * @Version 1.0
 */
public interface ManagerService {
    //分页查询测灵魂题目
    PageBeanVo findTestByPage(Integer pageNum, Integer pageSize,Integer lev);
    //根据questionnaire_id查询题目等级
    SoulQuestionnaire findById(Long id);
    //根据题目id删除对应题目
    void deleteByQuestionId(Long id);
    //查看题目详情
    SoulExaminationVoW findQuestionDetail(Long id);
    //修改题目
    void updateQuestion(SoulExamination soulExamination);
    //修改选项内容
    void updateOption(SoulOptions soulOptions);
    //删除选项
    void deleteOption(Long id);
    //添加选项
    void saveOption(SoulOptions soulOptions);
    //添加测试题目
    void saveTest(SoulExamination soulExamination);
    /*//修改密码
    void updatePW(String newPw);*/
}
