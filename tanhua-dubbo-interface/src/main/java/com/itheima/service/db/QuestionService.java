package com.itheima.service.db;

import com.itheima.domain.db.Question;

public interface QuestionService {
    Question findQuestionById(Long id);

    void save(Question question);

    void update(Question question);
}
