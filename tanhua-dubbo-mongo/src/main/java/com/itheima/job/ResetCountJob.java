package com.itheima.job;

import com.itheima.service.mongo.PeachService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ResetCountJob {

    @DubboReference
    private PeachService peachService;

    // 每天0点都要将mongo数据库中pickupcount表中的count字段重置为10
    @Scheduled(cron = "0 0 0 * * ?")
    public void resetCount(){
        System.out.println("将所有用户的count重置10");
        peachService.resetCount();
    }
}
