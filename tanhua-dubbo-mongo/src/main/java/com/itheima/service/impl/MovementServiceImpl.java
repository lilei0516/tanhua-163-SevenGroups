package com.itheima.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.*;
import com.itheima.service.mongo.MovementService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class MovementServiceImpl implements MovementService {

    @Autowired
    private RedisService redisService;

    @Autowired
    private MongoTemplate mongoTemplate;

    // 存入数据库
    // @Override
    // public void saveMovement(Movement movement) {
    //     // 生成pid
    //     Long pid = redisService.getNextPid(ConstantUtil.MOVEMENT_ID);
    //     // 设置pid
    //     movement.setPid(pid);
    //     // 存入动态详情表
    //     mongoTemplate.save(movement);
    //
    //     MyMovement myMovement = new MyMovement();
    //     // 设置发布id
    //     myMovement.setPublishId(movement.getId());
    //     // 设置发布时间
    //     myMovement.setCreated(movement.getCreated());
    //     // 存入个人动态表
    //     mongoTemplate.save(myMovement,ConstantUtil.MOVEMENT_MINE+movement.getUserId());
    //
    //     // 查询朋友关系表
    //     Query query = new Query(
    //             Criteria.where("userId").is(movement.getUserId())
    //     );
    //     List<Friend> friendsList = mongoTemplate.find(query, Friend.class);
    //     // 遍历得到这个朋友集合中每一个朋友的id
    //     if (CollectionUtil.isNotEmpty(friendsList)) {
    //         // 将数据存入朋友的朋友动态表
    //         for (Friend friend : friendsList) {
    //             FriendMovement friendMovement = new FriendMovement();
    //             friendMovement.setUserId(friend.getUserId());
    //             friendMovement.setPublishId(movement.getId());
    //             friendMovement.setCreated(movement.getCreated());
    //             mongoTemplate.save(friendMovement,ConstantUtil.MOVEMENT_FRIEND+friend.getFriendId());
    //         }
    //     }
    // }

    // 练习-发布（保存）动态
    @Override
    public void saveMovement(Movement movement) {
        // 设置pid
        Long pid = redisService.getNextPid(ConstantUtil.MOVEMENT_ID);
        movement.setPid(pid);
        // // 设置一个主键id
        // movement.setId(ObjectId.get());
        // 存入动态详情表
        mongoTemplate.save(movement);
        // 存入（创建）自己的动态表
        MyMovement myMovement = new MyMovement();
        // 设置pubid
        myMovement.setPublishId(movement.getId());
        // 设置创建时间
        myMovement.setCreated(movement.getCreated());
        // 存入
        mongoTemplate.save(myMovement,ConstantUtil.MOVEMENT_MINE+movement.getUserId());
        // 查询朋友关系表
        Query query = new Query(
                //条件
                Criteria.where("userId").is(movement.getUserId())
        );
        // 拿到朋友信息集合
        List<Friend> friendsList = mongoTemplate.find(query, Friend.class);
        // 判空遍历
        if (CollectionUtil.isNotEmpty(friendsList)) {
            for (Friend friend : friendsList) {
                // 获取每个朋友的id，并且设置到对象中
                Long friendId = friend.getFriendId();
                FriendMovement friendMovement = new FriendMovement();
                // 设置好友id
                friendMovement.setUserId(movement.getUserId());
                // 设置发布id
                friendMovement.setPublishId(movement.getId());
                // 设置发布时间
                friendMovement.setCreated(movement.getCreated());
                // 存入（创建）朋友的朋友动态表
                mongoTemplate.save(friendMovement,ConstantUtil.MOVEMENT_FRIEND+friendId);
            }
        }
    }

    // 查询自己的动态
    @Override
    public PageBeanVo getMyMovements(Integer pageNum, Integer pageSize, Long userId) {
        // 定义索引
        Integer index = (pageNum-1)*pageSize;
        // 定义分页查询条件拿到我的动态集合
        Query query = new Query().with(Sort.by(Sort.Order.desc("created"))).skip(index).limit(pageSize);
        List<MyMovement> myMovementsList = mongoTemplate.find(
                query, MyMovement.class, ConstantUtil.MOVEMENT_MINE + userId);
        // 要设置items
        List<Movement> list = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(myMovementsList)) {
            // 遍历
            for (MyMovement myMovement : myMovementsList) {
                // 拿着pubid去detail里找详细数据并返回
                ObjectId publishId = myMovement.getPublishId();
                // Query q = new Query(
                //         Criteria.where("")
                // );
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 存入集合
                list.add(movement);
            }
        }
        // 查我的动态总数
        long count = mongoTemplate.count(query, MyMovement.class, ConstantUtil.MOVEMENT_MINE + userId);
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,list);
        return pageBeanVo;
    }

    // 查询朋友的动态
    @Override
    public PageBeanVo getFriendMovements(Integer pageNum, Integer pageSize, Long userId) {
        // 设置分页条件
        Integer index = (pageNum-1)*pageSize;

        // 我的做法一（未完成）
        // // 查询朋友关系表
        // Query query = new Query(
        //         Criteria.where("userId").is(userId)
        // ).with(Sort.by(Sort.Order.desc("created"))).skip(index).limit(pageSize);
        // List<Friend> friends = mongoTemplate.find(query, Friend.class);
        // // 判空
        // if (CollectionUtil.isNotEmpty(friends)) {
        //     // 遍历得到朋友的id
        //     for (Friend friend : friends) {
        //         Long friendId = friend.getFriendId();
        //         // 通过friid去查询详细
        //         Query query1 = new Query(
        //                 Criteria.where("userId").is(friendId)
        //         );
        //         List<Movement> movements = mongoTemplate.find(query1, Movement.class);
        //     }
        // }

        // 做法二
        // 查询朋友关系表
        Query query = new Query().with(Sort.by(Sort.Order.desc("created"))).skip(index).limit(pageSize);
        // 拿到所有的朋友动态数据
        List<FriendMovement> friendMovements = mongoTemplate.find(
                query, FriendMovement.class, ConstantUtil.MOVEMENT_FRIEND + userId);
        // 新的items集合
        List<Movement> movements = new ArrayList<>();
        //判空遍历
        if (CollectionUtil.isNotEmpty(friendMovements)) {
            for (FriendMovement friendMovement : friendMovements) {
                // 拿到每一个数据的pubid
                ObjectId publishId = friendMovement.getPublishId();
                // 根据pubid查询详情(就是主键)
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 放到新的items集合
                movements.add(movement);
            }
        }
        // 查询数据总记录
        long count = mongoTemplate.count(query, FriendMovement.class);
        // 设置pagebeanvo
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum, pageSize, count, movements);
        return pageBeanVo;
    }

    // 查询推荐动态详情
    @Override
    public PageBeanVo getRecommendMovements(Integer pageNum, Integer pageSize, Long userId) {
        // 创建条件
        Integer index = (pageNum-1)*pageSize;
        Query query = new Query(
                Criteria.where("userId").is(userId)
        ).with(Sort.by(Sort.Order.desc("created"))).skip(index).limit(pageSize);
        // 查询推荐表
        List<RecommendMovement> recommendMovements = mongoTemplate.find(query, RecommendMovement.class);
        // 创建一个新的集合装载动态详情
        List<Movement> list = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(recommendMovements)) {
            for (RecommendMovement recommendMovement : recommendMovements) {
                // 拿到pubid去查询动态详情表
                ObjectId publishId = recommendMovement.getPublishId();
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 拿到对应的动态详情
                list.add(movement);
            }
        }
        // 查询记录总数
        long count = mongoTemplate.count(query, RecommendMovement.class);
        // 创建pagebeanvo并且返回
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,list);
        return pageBeanVo;
    }

    // 根据id查询动态详情
    @Override
    public Movement findMovementById(ObjectId publishId) {
        Movement movement = mongoTemplate.findById(publishId, Movement.class);
        return movement;
    }

    // 分页查询指定的某个人的动态
    @Override
    public PageBeanVo findMovementByPage(Integer pageNum, Integer pageSize, Long uid, Integer state) {
        // 创建条件
        Query query = new Query()
                .with(Sort.by(Sort.Order.desc("created")))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        Query query1 = new Query();
        // 判断
        if (uid!=null) {
            query.addCriteria(Criteria.where("userId").is(uid));
            query1.addCriteria(Criteria.where("userId").is(uid));
        }
        if (state!=null) {
            query.addCriteria(Criteria.where("state").is(state));
            query1.addCriteria(Criteria.where("state").is(state));
        }
        List<Movement> movementList = mongoTemplate.find(query, Movement.class);
        // 查询总数
        long count = mongoTemplate.count(query1, Movement.class);
        // 设置pbv
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,movementList);
        return pageBeanVo;
    }

    // 查询指定的评论
    @Override
    public Comment commentLike(ObjectId objectId) {
        Comment comment = mongoTemplate.findById(objectId, Comment.class);
        return comment;
    }

    // 保存和更新comment表
    @Override
    public void saveComment(Comment comment, Comment commentLikeForThis) {
        // 保存这两条记录
        mongoTemplate.save(comment);
        mongoTemplate.save(commentLikeForThis);
    }

    // 删除点赞记录，更新并保存原评论
    @Override
    public void removeLikeCount(ObjectId objectId, Long userId, Comment comment) {
        // 创建条件
        Query query = new Query(
                Criteria.where("publishId").is(objectId).and("userId").is(userId)
        );
        // 执行删除
        mongoTemplate.remove(query,Comment.class);
        // 顺便更新comment
        mongoTemplate.save(comment);
    }

    // 更新动态
    @Override
    public void update(Movement movement) {
        mongoTemplate.save(movement);
    }
}
