package com.itheima.service.impl;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.service.mongo.RecommendUserService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class RecommendUserServiceImpl implements RecommendUserService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 获取今日佳人
    @Override
    public RecommendUser getTodayBest(Long toUserId) {
        // 设置条件查询
        Query query = Query.query(Criteria.where("toUserId").is(toUserId))
                .with(Sort.by(Sort.Order.desc("score")))
                .skip(0).limit(1);
        // 执行查询获取
        RecommendUser todayBest = mongoTemplate.findOne(query, RecommendUser.class);
        return todayBest;
    }

    // 分页查询推荐用户的前10条
    @Override
    public PageBeanVo getRecommendationList(Long toUserId, Integer pageNum, Integer pageSize) {
        // 创建条件
        Query query = Query.query(Criteria.where("toUserId").is(toUserId))
                .with(Sort.by(Sort.Order.desc("score")))
                .skip((pageNum - 1) * pageSize + 1).limit(pageSize);
        // 执行查询
        List<RecommendUser> recommendUserList = mongoTemplate.find(query, RecommendUser.class);
        // 查询总数
        long count = mongoTemplate.count(query, RecommendUser.class);
        // 封装pbv
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum, pageSize, count, recommendUserList);
        return pageBeanVo;
    }

    // 获取指定的推荐用户详情
    @Override
    public RecommendUser getRecommendUserDetail(Long userId, Integer recommendUserId) {
        // 创建条件
        Query query = Query.query(Criteria.where("toUserId").is(userId).and("userId").is(recommendUserId));
        // 执行查询
        RecommendUser recommendUser = mongoTemplate.findOne(query, RecommendUser.class);
        return recommendUser;
    }
}
