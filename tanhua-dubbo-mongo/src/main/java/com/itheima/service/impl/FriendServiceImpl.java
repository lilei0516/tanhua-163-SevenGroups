package com.itheima.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.Friend;
import com.itheima.domain.mongo.FriendMovement;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.MyMovement;
import com.itheima.service.mongo.FriendService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import net.sf.jsqlparser.statement.select.FromItemVisitorAdapter;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class FriendServiceImpl implements FriendService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 将新关系存到mongo中
    @Override
    public void addContacts(Long userId, Long friendId) {
        // 我去与别人建立关系，操作人是我
        // 通过userid和friendid查询friend表，是否是朋友
        Query meAddAnotherQuery = new Query(Criteria.where("userId")
                .is(userId).and("friendId").is(friendId));
        boolean flag = mongoTemplate.exists(meAddAnotherQuery, Friend.class);
        // 没有朋友关系，创建关系
        if (!flag) {
            // 设置好友关系详细
            Friend mineFriend = new Friend();
            mineFriend.setUserId(userId);
            mineFriend.setFriendId(friendId);
            // 系统当前时间
            mineFriend.setCreated(System.currentTimeMillis());
            // 将好友关系存入到好友表
            mongoTemplate.save(mineFriend);
            // 查询好友的动态表获取动态信息id
            Query query1 = new Query();
            // 我的好友的最近动态
            List<MyMovement> myFriendMovementList = mongoTemplate.find(query1,
                    MyMovement.class, ConstantUtil.MOVEMENT_MINE + friendId);
            // 判空遍历
            if (CollectionUtil.isNotEmpty(myFriendMovementList)) {
                for (MyMovement myFriendMovement : myFriendMovementList) {
                    // 封装信息存入到我的好友动态表
                    FriendMovement myMovement = new FriendMovement();
                    // 设置好友id
                    myMovement.setUserId(friendId);
                    // 设置好友动态的动态id到我的好友动态表
                    myMovement.setPublishId(myFriendMovement.getPublishId());
                    // 好友发布动态的时间
                    myMovement.setCreated(myFriendMovement.getCreated());
                    // 存入
                    mongoTemplate.save(myMovement,ConstantUtil.MOVEMENT_FRIEND+userId);
                }
            }
        }

        // 别人和我建立关系，操作人是别人
        // 创建条件查询朋友关系表是否有别人和我的关系
        Query anotherAddMeQuery = new Query(Criteria.where("userId")
                .is(friendId).and("friendId").is(userId));
        // 去mongo中查询是否存在
        boolean flag1 = mongoTemplate.exists(anotherAddMeQuery, Friend.class);
        // 如果等于false，建立关系
        if (!flag1) {
            // 先去建立朋友关系
            Friend friendToMe = new Friend();
            // 操作人id
            friendToMe.setUserId(friendId);
            // 我的id
            friendToMe.setFriendId(userId);
            // 建立关系的时间
            friendToMe.setCreated(System.currentTimeMillis());
            // 存入mongo
            mongoTemplate.save(friendToMe);
            // 查询我的最近动态表
            List<MyMovement> myMovementList = mongoTemplate.findAll(MyMovement.class,
                    ConstantUtil.MOVEMENT_MINE + userId);
            // 判空遍历
            if (CollectionUtil.isNotEmpty(myMovementList)) {
                for (MyMovement myMovement : myMovementList) {
                    // 设置好友的好友动态表
                    FriendMovement friendMovement = new FriendMovement();
                    // userid是操作人的id
                    friendMovement.setUserId(friendId);
                    // 将我的动态id放入到好友的好友动态表
                    friendMovement.setPublishId(myMovement.getPublishId());
                    // 设置我发动态的时间
                    friendMovement.setCreated(myMovement.getCreated());
                    // 存入好友的好友动态mongo
                    mongoTemplate.save(friendMovement,ConstantUtil.MOVEMENT_FRIEND+friendId);
                }
            }
        }

    }

    // 查询我的朋友关系表
    @Override
    public PageBeanVo findMyFriendByPage(Integer pageNum, Integer pageSize, Long userId) {
        // 创建条件
        Query query = new Query(Criteria.where("userId").is(userId))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 执行查询
        List<Friend> friendList = mongoTemplate.find(query, Friend.class);
        // 查询关系总数
        long count = mongoTemplate.count(query, Friend.class);
        // 封装pageBeanvo
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,friendList);
        return pageBeanVo;
    }



    // 不分页查询好友列表(app端探花卡片功能使用)
    @Override
    public List<Friend> findCards(Long userId) {
        // 1.构建查询条件（根据登录人id查询他的好友）
        Query query = Query.query(Criteria.where("userId").is(userId));
        // 2.查询结果
        List<Friend> friendList = mongoTemplate.find(query, Friend.class);
        // 3.返回结果
        return friendList;
    }



    // 删除好友
    @Override
    public void deleteFriend(Long userId, Long friendId) {
        // 1.构建条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("friendId").is(friendId));
        // 2.删除好友
        mongoTemplate.remove(query, Friend.class);
    }



    // 删除好友动态
    @Override
    public void deleteFriendDynamic(Long userId, Long friendId) {
        // 1.构建条件
        Query query = Query.query(Criteria.where("userId").is(friendId));
        // 2.删除动态
        mongoTemplate.remove(query,ConstantUtil.MOVEMENT_FRIEND + userId);
    }
}
