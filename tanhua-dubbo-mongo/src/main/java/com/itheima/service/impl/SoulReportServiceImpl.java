package com.itheima.service.impl;

import com.itheima.domain.mongo.SoulReport;
import com.itheima.service.mongo.SoulReportService;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class SoulReportServiceImpl implements SoulReportService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 将报告存入mongo
    @Override
    public void saveReport(SoulReport report) {
        // 保存或更新
        // 根据主键会自动覆盖
        mongoTemplate.save(report);
    }

    // 查询指定用户报告
    @Override
    public SoulReport findReport(Long userId, Long questionnaireId) {
        // 创建条件
        Query query = new Query(
                Criteria.where("userId").is(userId).and("questionnaireId").is(questionnaireId)
        );
        SoulReport report = mongoTemplate.findOne(query, SoulReport.class);
        return report;
    }

    // 根据报告id查询报告
    @Override
    public SoulReport findReportById(ObjectId reportId) {
        SoulReport soulReport = mongoTemplate.findById(reportId, SoulReport.class);
        return soulReport;
    }

    // 查询推荐的人集合
    @Override
    public List<SoulReport> findSimilarYouList(Long conclusionId) {
        // 创建条件
        Query query = new Query(
                Criteria.where("conclusionId").is(conclusionId)
        );
        // 拿到相似的人集合
        List<SoulReport> reportList = mongoTemplate.find(query, SoulReport.class);
        return reportList;
    }

}
