package com.itheima.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.UserLocation;
import com.itheima.service.mongo.LocationService;
import com.itheima.vo.UserLocationWithInfoVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.*;

@DubboService
public class LocationServiceImpl implements LocationService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 上传地理位置
    @Override
    public void saveNearUserLocation(Double latitude, Double longitude, String addrStr, Long userId) {
        // 构建查询条件
        Query query = Query.query(Criteria.where("userId").is(userId));
        // 执行查询
        UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);
        // 根据查询结果判断信息是否存在
        if (userLocation != null) {
            // 存在，更新
            userLocation.setLocation(new GeoJsonPoint(longitude,latitude));
            userLocation.setAddress(addrStr);
            // 上次更新时间
            userLocation.setLastUpdated(userLocation.getUpdated());
            // 本次更新时间
            userLocation.setUpdated(System.currentTimeMillis());
        } else {
            // 不存在，创建
            userLocation = new UserLocation();
            userLocation.setAddress(addrStr);
            userLocation.setCreated(System.currentTimeMillis());
            userLocation.setUpdated(System.currentTimeMillis());
            userLocation.setUserId(userId);
            userLocation.setLocation(new GeoJsonPoint(longitude,latitude));
            // 设置上一次的更新时间（就是本次时间）
            userLocation.setLastUpdated(System.currentTimeMillis());
        }
        // 向mongo中保存或更新
        mongoTemplate.save(userLocation);
    }

    // 搜寻附近的人
    @Override
    public List<Long> serarchNearUser(String gender, Long distance, Long userId) {
        // 定圆心
        GeoJsonPoint myLocation = mongoTemplate.findOne(Query.query(Criteria.where("userId")
                .is(userId)), UserLocation.class).getLocation();
        // 画半径
        Distance distance1 = new Distance(distance / 1000, Metrics.KILOMETERS);
        // 画圆
        Circle circle = new Circle(myLocation, distance1);
        // 搜索
        Query query = new Query(Criteria.where("location").withinSphere(circle));
        List<UserLocation> userLocationList = mongoTemplate.find(query                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              , UserLocation.class);
        // 用来存储附近的人id的集合
        List<Long> nearUserIdList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(userLocationList)) {
            for (UserLocation userLocation : userLocationList) {
                nearUserIdList.add(userLocation.getUserId());
            }
        }
        // 返回
        return nearUserIdList;
    }
    //根据userId查询对应用户地理信息
    @Override
    public UserLocationWithInfoVo findByUserId(Long userId) {
        //查询地理位置信息
        Query query = Query.query(Criteria.where("userId").is(userId));
        UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);
        //封装
        UserLocationWithInfoVo vo = new UserLocationWithInfoVo();
        //没有地理信息的用户使用假数据
        if (userLocation==null){
            userLocation = new UserLocation();
            userLocation.setAddress("mock");
            userLocation.setLocation(new GeoJsonPoint(116.635672,40.169419));
        }
            vo.setAddress(userLocation.getAddress());//用户所在地理位置描述
            vo.setLongitude(userLocation.getLocation().getX());//经度
            vo.setLatitude(userLocation.getLocation().getY());//纬度

        return vo;
    }
    //根据id查询用户地理位置
    @Override
    public Map<String, Double> findLocation(Long id) {
        Query query = Query.query(Criteria.where("userId").is(id));
        UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);
        Map<String,Double> userMap = new HashMap<>();
        if (userLocation==null){
            userLocation = new UserLocation();
            userLocation.setAddress("mock");
            userLocation.setLocation(new GeoJsonPoint(116.635672,40.169419));
        }
            userMap.put("longitude",userLocation.getLocation().getX());
            userMap.put("latitude",userLocation.getLocation().getY());
        return userMap;
    }
}
