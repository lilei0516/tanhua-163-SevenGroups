package com.itheima.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.PickupCount;
import com.itheima.domain.mongo.Sound;
import com.itheima.service.mongo.PeachService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class PeachServiceImpl implements PeachService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 保存sound数据
    @Override
    public void saveSound(Sound sound) {
        // 存入mongo
        mongoTemplate.save(sound);
    }

    // 找出指定的sound数据集合
    @Override
    public List<Sound> getPeachblossom() {
        // 创建条件
        Query query = Query.query(
                Criteria.where("pickup").is(0)
        );
        List<Sound> soundList = mongoTemplate.find(query, Sound.class);
        return soundList;
    }

    // 查询指定用户的pickupcount
    @Override
    public PickupCount findPickupCount(Long soundUserId) {
        // 创建条件
        Query query = Query.query(Criteria.where("userId").is(soundUserId));
        PickupCount pickupCount = mongoTemplate.findOne(query, PickupCount.class);
        return pickupCount;
    }

    // 保存或更新
    @Override
    public void save(PickupCount pickupCount) {
        mongoTemplate.save(pickupCount);
    }

    // 重置所有用户的count
    @Override
    public void resetCount() {
        // 查询所有用户记录
        List<PickupCount> pickupCountList = mongoTemplate.findAll(PickupCount.class);
        // 遍历判空
        if (CollectionUtil.isNotEmpty(pickupCountList)) {
            for (PickupCount pickupCount : pickupCountList) {
                pickupCount.setCount(10);
                // 更新到数据库
                save(pickupCount);
            }
        }
    }
}
