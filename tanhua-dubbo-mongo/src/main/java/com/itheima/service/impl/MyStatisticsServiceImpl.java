package com.itheima.service.impl;

import com.itheima.domain.mongo.Friend;
import com.itheima.domain.mongo.UserLike;
import com.itheima.domain.mongo.Visitor;
import com.itheima.service.mongo.MyStatisticsService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class MyStatisticsServiceImpl implements MyStatisticsService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 查询互相喜欢的数量（好友数量）
    @Override
    public Long findEachLoveCount(Long userId) {
        // 1.构建查询条件
        Query query = Query.query(Criteria.where("userId").is(userId));
        // 2.查询数据
        long eachLoveCount = mongoTemplate.count(query, Friend.class);
        // 3.返回数据
        return eachLoveCount;
    }


    // 查询喜欢的数量（我喜欢的人）
    @Override
    public Long findLoveCount(Long userId) {
        // 1.构建查询条件
        Query query = Query.query(Criteria.where("userId").is(userId));
        // 2.查询数据
        long loveCount = mongoTemplate.count(query, UserLike.class);
        // 3.返回数据
        return loveCount;
    }


    // 查询粉丝数量（喜欢我的人）
    @Override
    public Long findFanCount(Long userId) {
        // 1.构建查询条件
        Query query = Query.query(Criteria.where("likeUserId").is(userId));
        // 2.查询数据
        long fanCount = mongoTemplate.count(query, UserLike.class);
        // 3.返回数据
        return fanCount;
    }


    // 分页查询我喜欢的人
    @Override
    public PageBeanVo findLoveUser(Integer pageNum,Integer pageSize,Long userId) {
        // 1.构造查询条件
        Query query = Query.query(Criteria.where("userId").is(userId))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 2.查询list
        List<UserLike> userLikes = mongoTemplate.find(query, UserLike.class);
        // 3.查询total
        long total = mongoTemplate.count(query, UserLike.class);
        // 4.封装并返回pageBenVo
        return new PageBeanVo(pageNum,pageSize,total,userLikes);
    }


    // 分页查询喜欢我的人
    @Override
    public PageBeanVo findFanUser(Integer pageNum,Integer pageSize,Long userId) {
        // 1.构造查询条件
        Query query = Query.query(Criteria.where("likeUserId").is(userId))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 2.查询list
        List<UserLike> userLikes = mongoTemplate.find(query, UserLike.class);
        // 3.查询total
        long total = mongoTemplate.count(query, UserLike.class);
        // 4.封装并返回pageBenVo
        return new PageBeanVo(pageNum,pageSize,total,userLikes);
    }


    // 分页查询历史访客
    @Override
    public PageBeanVo findVisitor(Integer pageNum, Integer pageSize, Long userId) {
        // 1.构造查询条件
        Query query = Query.query(Criteria.where("userId").is(userId))
                .with(Sort.by(Sort.Order.desc("date"))) // 降序
                .skip((pageNum - 1) * pageSize).limit(pageSize);// 分页
        // 2.查询数据
        List<Visitor> visitorList = mongoTemplate.find(query, Visitor.class);
        // 3.查询total
        long total = mongoTemplate.count(query, Visitor.class);
        // 4.封装并返回pageBeanVo
        return new PageBeanVo(pageNum,pageSize,total,visitorList);
    }



    // 判断我是否喜欢过我的粉丝
    @Override
    public Boolean findLike(Long userId, Long fansId) {
        // 1.构造查询条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(fansId));
        // 2.查询结果
        UserLike userLike = mongoTemplate.findOne(query, UserLike.class);
        // 3.判断是否喜欢
        if (userLike == null){  // 没查到就是不喜欢
            return false;
        }else {     // 查到了就是喜欢过
            return true;
        }
    }


}
