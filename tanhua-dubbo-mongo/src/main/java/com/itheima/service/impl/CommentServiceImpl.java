package com.itheima.service.impl;

import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.Video;
import com.itheima.service.mongo.CommentService;
import com.itheima.vo.PageBeanVo;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class CommentServiceImpl implements CommentService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 保存comment
    @Override
    public Integer saveComment(Comment comment) {
        // 向数据库中保存这个comment
        mongoTemplate.save(comment);
        // 判断这个commenttype
        switch (comment.getCommentType()){
            case 1:{
                // 点赞
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 对点赞次数加一并保存返回
                movement.setLikeCount(movement.getLikeCount()+1);
                mongoTemplate.save(movement);
                return movement.getLikeCount();
            }
            case 2:{
                // 评论
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 对评论次数加一并保存返回
                movement.setCommentCount(movement.getCommentCount()+1);
                mongoTemplate.save(movement);
                return movement.getCommentCount();
            }
            case 3:{
                // 喜欢
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(comment.getPublishId(), Movement.class);
                // 对喜欢次数加一并保存返回
                movement.setLoveCount(movement.getLoveCount()+1);
                mongoTemplate.save(movement);
                return movement.getLoveCount();
            }
            case 4:{
                //对视频点赞
                // 根据视频id查询动态详情
                Video video = mongoTemplate.findById(comment.getPublishId(), Video.class);
                // 对点赞次数加一并保存返回
                video.setLikeCount(video.getLikeCount()+1);
                mongoTemplate.save(video);
                return video.getLikeCount();
            }
            case 5:{
                // 对视频评论
                // 根据视频id查询动态详情
                Video video = mongoTemplate.findById(comment.getPublishId(), Video.class);
                // 对评论次数加一并保存返回
                video.setCommentCount(video.getCommentCount()+1);
                mongoTemplate.save(video);
                return video.getCommentCount();
            }
        }
        return 0;
    }

    // 删除comment
    @Override
    public Integer removeComment(
            // 确定动态范围
            ObjectId publishId,
            // 确定用户范围
            Long userId,
            // 确定用户操作范围
            Integer commentType) {
        // 根据三个条件去删除指定的comment
        Query query = new Query(
                Criteria.where("publishId").is(publishId)
                        .and("userId").is(userId)
                        .and("commentType").is(commentType)
        );
        mongoTemplate.remove(query,Comment.class);
        // 判断操作类型
        switch (commentType){
            case 1:{
                // 点赞
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 对点赞次数加一并保存返回
                movement.setLikeCount(movement.getLikeCount()-1);
                mongoTemplate.save(movement);
                return movement.getLikeCount();
            }
            case 2:{
                // 评论
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 对评论次数加一并保存返回
                movement.setCommentCount(movement.getCommentCount()-1);
                mongoTemplate.save(movement);
                return movement.getCommentCount();
            }
            case 3:{
                // 喜欢
                // 根据动态id查询动态详情
                Movement movement = mongoTemplate.findById(publishId, Movement.class);
                // 对喜欢次数加一并保存返回
                movement.setLoveCount(movement.getLoveCount()-1);
                mongoTemplate.save(movement);
                return movement.getLoveCount();
            }
            case 4:{
                //取消视频点赞
                Video video = mongoTemplate.findById(publishId, Video.class);
                // 对点赞次数减一并保存返回
                video.setLikeCount(video.getLikeCount()-1);
                mongoTemplate.save(video);
                return video.getLikeCount();
            }
            case 5:{
                // 取消视频评论
                Video video = mongoTemplate.findById(publishId, Video.class);
                // 对评论次数减一并保存返回
                video.setCommentCount(video.getCommentCount()-1);
                mongoTemplate.save(video);
                return video.getCommentCount();
            }
        }
        return 0;
    }

    // 分页查询某条动态的评论
    @Override
    public PageBeanVo findCommentsListByPage(Integer pageNum, Integer pageSize, ObjectId objectId, int commentType) {
        // 创建条件
        Query query = new Query(
                Criteria.where("publishId").is(objectId)
                        .and("commentType").is(commentType))
                .with(Sort.by(Sort.Order.desc("created")))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 去查询comment表
        List<Comment> list = mongoTemplate.find(query, Comment.class);
        // 查询评论总数
        long count = mongoTemplate.count(query, Comment.class);
        // 封装pagebeanvo
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,list);
        return pageBeanVo;
    }

    // 分页查询comment表
    @Override
    public PageBeanVo findCommentVoByPage(Long publishUserId,
                                          int contentType, Integer pageNum, Integer pageSize) {
        // 创建条件
        Query query = new Query(Criteria.where("publishUserId").is(publishUserId)
                .and("commentType").is(contentType))
                .with(Sort.by(Sort.Order.desc("created")))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 执行查询
        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        // 查询总数
        long count = mongoTemplate.count(query, Comment.class);
        // 封装pbv
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,commentList);
        return pageBeanVo;
    }

    // 分页查询视频评论
    @Override
    public PageBeanVo findByPage(ObjectId publishId, Integer commentType, Integer pageNum, Integer pageSize) {
        // 1.构建查询条件
        Query query = new Query(  // 条件
                Criteria.where("publishId").is(publishId)
                        .and("commentType").is(commentType)
        ).with(Sort.by(Sort.Order.desc("created"))) // 排序
                .skip((pageNum - 1) * pageSize).limit(pageSize); // 分页

        // 2.查询comment集合
        List<Comment> commentList = mongoTemplate.find(query, Comment.class);
        // 3.查询总记录数
        long total = mongoTemplate.count(query, Comment.class);
        // 4.返回并封装分页对象
        return new PageBeanVo(pageNum, pageSize, total, commentList);

    }

}
