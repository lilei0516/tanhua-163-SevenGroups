package com.itheima.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

@Component
public class RedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public Long getNextPid(String collectionName){
        //创建自增对象
        RedisAtomicLong redisAtomicLong = new RedisAtomicLong("pid:"
                +collectionName,stringRedisTemplate.getConnectionFactory());
        //自增》返回
        return redisAtomicLong.incrementAndGet();
    }
}
