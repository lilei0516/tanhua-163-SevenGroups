package com.itheima.service.impl;

import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;
import com.itheima.service.mongo.CardsService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class CardsServiceImpl implements CardsService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 查询探花卡片推荐用户
    @Override
    public List<RecommendUser> findCards(Long toUserId) {
        // 1.构建查询条件（toUserId就是登录人的id）
        Query query = Query.query(Criteria.where("toUserId").is(toUserId));
        // 2.查询用户推荐表数据
        List<RecommendUser> recommendUserList = mongoTemplate.find(query, RecommendUser.class);
        // 3.返回数据
        return recommendUserList;
    }


    // 卡片右滑（喜欢）:查询喜欢用户表
    @Override
    public UserLike findCardsLove(Long userId, Long likeUserId) {
        // 1.构造查询条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("likeUserId").is(likeUserId));
        // 2.查询喜欢用户表
        UserLike userLike = mongoTemplate.findOne(query, UserLike.class);
        // 3.返回结果
        return userLike;
    }


    // 喜欢以后添加进喜欢表
    @Override
    public void saveCardsLove(UserLike userLike) {
        mongoTemplate.save(userLike);
    }


    // 删除用户推荐表中的数据
    @Override
    public void deleteRecommendUser(Long userId, Long toUserId) {
        // 1.定义条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("toUserId").is(toUserId));
        // 2.删除用户推荐表数据
        mongoTemplate.remove(query,RecommendUser.class);
    }



    // 删除喜欢表中的数据
    @Override
    public void deleteCardsLove(Long userId, Long noLikeUserId) {
        // 构建查询条件，因为是她喜欢过我，我不可能喜欢过她，我喜欢过她就刷不到她了，所以userId是对方的id
        Query query = Query.query(Criteria.where("userId").is(noLikeUserId).and("likeUserId").is(userId));
        // 删除数据
        mongoTemplate.remove(query,UserLike.class);
    }


}
