package com.itheima.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.domain.mongo.*;
import com.itheima.service.mongo.VideoService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

@DubboService
public class VideoServiceImpl implements VideoService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private RedisService redisService;

    // 获取一个装载video集合的pbv
    @Override
    public PageBeanVo getSmallVideos(Integer pageNum, Integer pageSize, Long userId) {
        // 创建查询条件
        Query query = new Query(
                Criteria.where("userId").is(userId))
                .with(Sort.by(Sort.Order.desc("date")))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 执行查询
        List<RecommendVideo> recommendVideoList = mongoTemplate.find(query, RecommendVideo.class);
        // 新的容器
        List<Video> videoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(recommendVideoList)) {
            for (RecommendVideo recommendVideo : recommendVideoList) {

                // 遍历查询详情
                Video video = mongoTemplate.findById(recommendVideo.getVideoId(), Video.class);
                // 添加到集合
                videoList.add(video);
            }
        }
        // 查询总记录数
        long count = mongoTemplate.count(query, RecommendVideo.class);
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,videoList);
        return pageBeanVo;
    }

    // 上传视频到video并且将自己的视频推荐给自己
    @Override
    public void saveSmallVideos(Video video) {
        // 设置vid
        Long vid = redisService.getNextPid(ConstantUtil.VIDEO_ID);
        video.setVid(vid);
        // 将记录保存到video中
        mongoTemplate.save(video);

        // 保存到推荐视频表
        RecommendVideo recommendVideo = new RecommendVideo();
        // 设置属性
        recommendVideo.setDate(video.getCreated());
        recommendVideo.setVid(vid);
        recommendVideo.setUserId(video.getUserId());
        recommendVideo.setVideoId(video.getId());
        // 保存到数据库
        mongoTemplate.save(recommendVideo);
    }

    // 将关注用户关系保存到数据库
    @Override
    public void userFocus(FocusUser focusUser) {
        mongoTemplate.save(focusUser);
    }

    // 删除这个关注关系
    @Override
    public void userUnFocus(Long userId, Long focusUserId) {
        // 创建条件
        Query query = new Query(
                Criteria.where("userId").is(userId).and("focusUserId").is(focusUserId)
        );
        // 执行语句
        mongoTemplate.remove(query,FocusUser.class);
    }

    // 获取一个装载video集合的pbv
    @Override
    public PageBeanVo getMySmallVideos(Integer pageNum, Integer pageSize, Long userId) {
        // 创建查询条件
        Query query = new Query(
                Criteria.where("userId").is(userId))
                .with(Sort.by(Sort.Order.desc("created")))
                .skip((pageNum-1)*pageSize).limit(pageSize);
        // 执行查询
        List<Video> videoList = mongoTemplate.find(query, Video.class);
        // 查询总记录数
        long count = mongoTemplate.count(query, Video.class);
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,count,videoList);
        return pageBeanVo;
    }

    // 根据主键查询video详情
    @Override
    public Video findVideoById(ObjectId videoId) {
        Video video = mongoTemplate.findById(videoId, Video.class);
        return video;
    }



    @Override
    public Comment commentLike(ObjectId objectId) {
        Comment comment = mongoTemplate.findById(objectId, Comment.class);
        return comment;
    }

    // 保存和更新comment表
    @Override
    public void saveComment(Comment comment, Comment commentLikeForThis) {
        // 保存这两条记录
        mongoTemplate.save(comment);
        mongoTemplate.save(commentLikeForThis);
    }

    // 删除点赞记录，更新并保存原评论
    @Override
    public void removeLikeCount(ObjectId objectId, Long userId, Comment comment) {
        // 创建条件
        Query query = new Query(
                Criteria.where("publishId").is(objectId).and("userId").is(userId)
        );
        // 执行删除
        mongoTemplate.remove(query,Comment.class);
        // 顺便更新comment
        mongoTemplate.save(comment);
    }

    // 更新动态
    @Override
    public void update(Video video) {

        mongoTemplate.save(video);
    }



}
