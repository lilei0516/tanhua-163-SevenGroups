package com.itheima.service.impl;

import com.itheima.domain.mongo.Visitor;
import com.itheima.service.mongo.VisitorService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class VisitorServiceImpl implements VisitorService {

    @Autowired
    private MongoTemplate mongoTemplate;

    // 得到最近访客
    @Override
    public List<Visitor> getVisitors(Long userId, long lastLoginTime) {
        // 创建条件
        Query query = Query.query(Criteria.where("userId").is(userId).and("date").gt(lastLoginTime))
                .with(Sort.by(Sort.Order.desc("date")))
                .skip(0).limit(5);
        // 执行
        List<Visitor> visitorList = mongoTemplate.find(query, Visitor.class);
        return visitorList;
    }
}
