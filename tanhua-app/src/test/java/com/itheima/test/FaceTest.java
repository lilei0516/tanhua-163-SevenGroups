package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.itheima.autoconfig.face.AipFaceTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FaceTest {
    @Autowired
    private AipFaceTemplate aipFaceTemplate;
    @Test
    public void test01(){
        String url = "C:\\Users\\86132\\Desktop\\lbs.png";
        File file = new File(url);
        byte[] bytes = FileUtil.readBytes(file);
        System.out.println(aipFaceTemplate.detect(bytes));
    }
}
