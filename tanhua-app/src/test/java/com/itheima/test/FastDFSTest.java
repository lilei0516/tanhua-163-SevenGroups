package com.itheima.test;

import cn.hutool.core.io.FileUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FastDFSTest {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;

    @Autowired
    private FdfsWebServer fdfsWebServer;

    @Test
    public void test01() throws FileNotFoundException {
        File file = new File("C:\\Users\\86132\\Desktop\\lbs.png");
        FileInputStream fileInputStream = new FileInputStream(file);
        // 文件流，文件大小，文件扩展名，文件信息（null就行）
        StorePath storePath = fastFileStorageClient
                .uploadFile(fileInputStream, file.length(), FileUtil.extName(file), null);
        System.out.println("图片的访问地址"+fdfsWebServer.getWebServerUrl()+storePath.getFullPath());
    }

}
