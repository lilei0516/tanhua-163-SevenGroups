package com.itheima.test;

import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HuanxinTest {

    @Autowired
    private HuanXinTemplate huanXinTemplate;


    @Test
    public void test01(){

        // 指定url地址
        String url = "https://a1.easemob.com/1170211104040494/tanhua02/token";

        // 创建resttem
        RestTemplate restTemplate = new RestTemplate();

        // 创建参数集合
        Map map = new HashMap<>();
        // 向集合中添加参数
        map.put("grant_type", "client_credentials");
        map.put("client_id", "YXA6SfkTYS1gTr-cguKFgMqxMw");
        map.put("client_secret", "YXA6HKQ_0weRnzRA1LiOg9-gzpCT6vU");

        // 发送post请求
        Map resultMap = restTemplate.postForObject(url, map, Map.class);

        // 输出获取的token
        Object access_token = resultMap.get("access_token");
        System.err.println(access_token);
    }

    @Test
    public void test02(){
        huanXinTemplate.register("110");
    }
}
