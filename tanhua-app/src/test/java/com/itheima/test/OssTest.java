package com.itheima.test;

import com.itheima.autoconfig.oss.OssTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OssTest {

    @Autowired
    private OssTemplate ossTemplate;

    @Test
    public void test01() throws FileNotFoundException {
        File file = new File("C:\\Users\\86132\\Desktop\\tanhua.png");
        FileInputStream fis = new FileInputStream(file);
        ossTemplate.upload(file.getName(),fis);
    }
}
