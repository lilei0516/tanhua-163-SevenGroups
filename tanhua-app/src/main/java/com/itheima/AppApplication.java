package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(exclude = MongoAutoConfiguration.class)
@EnableCaching  // 开启缓存功能
public class AppApplication {
    //dev分支推送测试第二次
    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class,args);
    }
}
