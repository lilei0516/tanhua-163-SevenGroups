package com.itheima.app.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import com.itheima.domain.db.Announcement;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Friend;
import com.itheima.service.db.AnnouncementService;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CommentService;
import com.itheima.service.mongo.FriendService;
import com.itheima.vo.*;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.*;

@Component
public class MessageManager {

    @Autowired
    private HuanXinTemplate huanXinTemplate;

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private QuestionService questionService;

    @DubboReference
    private FriendService friendService;

    @DubboReference
    private CommentService commentService;

    @DubboReference
    private AnnouncementService announcementService;

    // 聊一下（好友申请）
    public void strangerQuestions(String reply, Long jiarenId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 查询当前用户的userinfo
        UserInfo userInfoById = userInfoService.findUserInfoById(userId);
        // 查询jiaren的陌生人问题
        Question questionById = questionService.findQuestionById(jiarenId);
        // 陌生人问题可能是空
        if (questionById == null) {
            // 设置默认
            questionById = new Question();
        }
        // 封装发送给环信的信息
        Map map = new HashMap();
        map.put("userId",userId);
        map.put("huanXinId", "HX" +userId);
        map.put("nickname",userInfoById.getNickname());
        map.put("strangerQuestion",questionById.getStrangerQuestion());
        map.put("reply",reply);
        // 将map转为json发送给环信云
        String params = JSON.toJSONString(map);
        // 指定目标信息人的环信id
        huanXinTemplate.sendMsg("HX"+jiarenId,params);

    }

    // 向mongo中及环信云中建立好友关系
    public void addContacts(Long friendId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 保存到mongo
        friendService.addContacts(userId,friendId);
        // 保存到环信云
        huanXinTemplate.addContacts("HX"+userId,"HX"+friendId);
    }

    // 查询联系人列表
    public ResponseEntity getContacts(Integer pageNum, Integer pageSize) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用friendservice
        PageBeanVo pageBeanVo = friendService.findMyFriendByPage(pageNum,pageSize,userId);
        // 获取friendlist
        List<Friend> myFriendList = (List<Friend>) pageBeanVo.getItems();
        // 新的items集合
        List<ContactVo> contactVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(myFriendList)) {
            for (Friend myFriend : myFriendList) {
                // 前端需要myfriend+huanxinid
                ContactVo contactVo = new ContactVo();
                // 获取我的好友用户详情
                UserInfo myFriendUserInfo = userInfoService.findUserInfoById(myFriend.getFriendId());
                // 封装数据
                contactVo.setUserInfo(myFriendUserInfo);
                // 好友环信id
                contactVo.setUserId("HX"+myFriend.getFriendId());
                // 添加到集合中去
                contactVoList.add(contactVo);
            }
        }
        // 重新设置items
        pageBeanVo.setItems(contactVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查看点赞评论喜欢
    public ResponseEntity findCommentVoByPage(Integer pageNum, Integer pageSize, int contentType) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service获得pagebeanvo
        PageBeanVo pageBeanVo = commentService.findCommentVoByPage(userId,contentType,pageNum,pageSize);
        // 获取items
        List<Comment> commentList = (List<Comment>) pageBeanVo.getItems();
        // usercommentvo容器
        List<UserCommentVo> userCommentVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(commentList)) {
            for (Comment comment : commentList) {
                UserCommentVo userCommentVo = new UserCommentVo();
                // 查询评论者的详情
                UserInfo userInfo = userInfoService.findUserInfoById(comment.getUserId());
                // 设置信息
                userCommentVo.setAvatar(userInfo.getAvatar());
                userCommentVo.setNickname(userInfo.getNickname());
                userCommentVo.setCreateDate(DateUtil.formatDateTime(new Date(comment.getCreated())));
                // 放入list
                userCommentVoList.add(userCommentVo);
            }
        }
        // CommentVo
        pageBeanVo.setItems(userCommentVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查看公告
    public ResponseEntity findAnnouncements(Integer pageNum, Integer pageSize) {
        // 调用service
        PageBeanVo pageBeanVo = announcementService.findAnnouncementsByPage(pageNum,pageSize);
        // 获取items
        List<Announcement> announcementList = (List<Announcement>) pageBeanVo.getItems();
        // 新的items容器
        List<AnnouncementVo> announcementVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(announcementList)) {
            for (Announcement announcement : announcementList) {
                // 设置avo
                AnnouncementVo announcementVo = new AnnouncementVo();
                announcementVo.setId(announcement.getId().toString());
                announcementVo.setTitle(announcement.getTitle());
                announcementVo.setDescription(announcement.getDescription());
                announcementVo.setCreateDate(DateUtil.formatDateTime(announcement.getCreated()));
                // 添加到集合中
                announcementVoList.add(announcementVo);
            }
        }
        // 重新设置items
        pageBeanVo.setItems(announcementVoList);
        // 返回
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查询对方的环信id
    public ResponseEntity findUserInfoByHuanXinId(String huanxinId) {
        String userId = huanxinId.replaceAll("HX", "");
        // 查询指定用户详情
        UserInfo userInfo = userInfoService.findUserInfoById(Long.parseLong(userId));
        // 新建前端需要的vo
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtil.copyProperties(userInfo,userInfoVo);
        // 返回
        return ResponseEntity.ok(userInfoVo);
    }
}
