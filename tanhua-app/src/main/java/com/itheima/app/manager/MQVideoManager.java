package com.itheima.app.manager;

import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.MovementScore;
import com.itheima.domain.mongo.Video;
import com.itheima.domain.mongo.VideoScore;
import com.itheima.service.mongo.MovementService;
import com.itheima.service.mongo.VideoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//负责向mq发送消息
@Component
public class MQVideoManager {

    //针对动态的操作
    public static final Integer VIDEO_PUBLISH = 1;// 发动态
    public static final Integer VIDEO_BROWSE = 2;// 浏览动态
    public static final Integer VIDEO_LIKE = 3;// 点赞
    public static final Integer VIDEO_LOVE = 4;// 喜欢
    public static final Integer VIDEO_COMMENT = 5;// 评论
    public static final Integer VIDEO_DISLIKE = 6;// 取消点赞
    public static final Integer VIDEO_DISLOVE = 7;// 取消喜欢

    @DubboReference
    private VideoService videoService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 发送mq消息
    public void sendMovement(Long userId, ObjectId videoId, Integer type) {
        // 创建动态日志实体
        VideoScore videoScore = new VideoScore();
        videoScore.setUserId(userId); // 设置操作人
        // 查询动态详情
        // Movement movement = movementService.findMovementById(videoId);
        Video video = videoService.findVideoById(videoId);
        videoScore.setVideoId(video.getVid()); // 设置操作动态

        switch (type) {
            case 1: {
                videoScore.setScore(20d);
                break;
            }
            case 2: {
                videoScore.setScore(1d);
                break;
            }
            case 3: {
                videoScore.setScore(5d);
                break;
            }
            case 4: {
                videoScore.setScore(8d);
                break;
            }
            case 5: {
                videoScore.setScore(10d);
                break;
            }
            case 6: {
                videoScore.setScore(-5d);
                break;
            }
            case 7: {
                videoScore.setScore(-8d);
                break;
            }
        }

        // 发送mq消息
        rabbitTemplate.convertAndSend("tanhua.recommend.video", videoScore);
    }
}