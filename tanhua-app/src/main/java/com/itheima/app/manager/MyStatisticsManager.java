package com.itheima.app.manager;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Friend;
import com.itheima.domain.mongo.UserLike;
import com.itheima.domain.mongo.Visitor;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CardsService;
import com.itheima.service.mongo.FriendService;
import com.itheima.service.mongo.MyStatisticsService;
import com.itheima.vo.MyStatisticsDetailsVo;
import com.itheima.vo.MyStatisticsVo;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyStatisticsManager {

    @DubboReference
    private MyStatisticsService myStatisticsService;

    // 互相喜欢，喜欢，粉丝 - 统计
    public ResponseEntity findCounts() {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.查询互相喜欢的数量
        Long eachLoveCount = myStatisticsService.findEachLoveCount(userId);
        // 3.查询我喜欢的数量
        Long loveCount = myStatisticsService.findLoveCount(userId);
        // 4.查询喜欢我的数量
        Long fanCount = myStatisticsService.findFanCount(userId);
        // 5.封装vo
        MyStatisticsVo vo = new MyStatisticsVo();
        vo.setEachLoveCount(eachLoveCount.intValue());  // 互相喜欢
        vo.setLoveCount(loveCount.intValue());  // 我喜欢的
        vo.setFanCount(fanCount.intValue());    // 喜欢我的
        // 6.返回vo
        return ResponseEntity.ok(vo);
    }


    @DubboReference
    private FriendService friendService;

    @DubboReference
    private UserInfoService userInfoService;

    // 互相喜欢、喜欢、粉丝、谁看过我 - 查看详情
    public ResponseEntity findFriends(Integer pageNum, Integer pageSize, Integer type) {
        // 1.获取登录人的userId
        Long userId = UserHolder.get().getId();
        // 2.判断type类型
        switch (type){
            case 1:{    // 查询互相喜欢
                // 1.1 获取分页对象
                PageBeanVo pageBeanVo = friendService.findMyFriendByPage(pageNum, pageSize, userId);
                // 1.2 获取friendList
                List<Friend> friendList = (List<Friend>) pageBeanVo.getItems();
                // 1.3封装MyStatisticsVo
                // 1.3.1声明voList
                List<MyStatisticsDetailsVo> voList = new ArrayList<>();
                // 1.3.2 遍历
                if (CollectionUtil.isNotEmpty(friendList)) {
                    for (Friend friend : friendList) {
                        Long friendId = friend.getFriendId();   // 获取好友id
                        // 根据好友id去查询userInfo
                        UserInfo userInfo = userInfoService.findUserInfoById(friendId);
                        //  声明vo并封装
                        MyStatisticsDetailsVo vo = new MyStatisticsDetailsVo();
                        vo.setUserInfo(userInfo);
                        vo.setAlreadyLove(true);    // 是否喜欢他
                        vo.setMatchRate(RandomUtil.randomInt(60,99));  // 匹配度

                        // 将vo添加到voList
                        voList.add(vo);
                    }
                }
                // 1.4将voList添加到分页对象
                pageBeanVo.setItems(voList);
                // 1.5返回分页对象
                return ResponseEntity.ok(pageBeanVo);
            }
            case 2:{    // 查询我喜欢的
                // 2.1 获取分页对象
                PageBeanVo pageBeanVo = myStatisticsService.findLoveUser(pageNum, pageSize, userId);
                // 2.2 获取userLikeList
                List<UserLike> userLikeList = (List<UserLike>) pageBeanVo.getItems();
                // 2.3 封装MyStatisticsVo
                // 2.3.1 声明voList
                List<MyStatisticsDetailsVo> voList = new ArrayList<>();
                // 2.3.2 遍历
                if (CollectionUtil.isNotEmpty(userLikeList)) {
                    for (UserLike userLike : userLikeList) {
                        // 获取我喜欢的人的id
                        Long likeUserId = userLike.getLikeUserId();
                        // 根据我喜欢人的id查询他的userInfo
                        UserInfo userInfo = userInfoService.findUserInfoById(likeUserId);

                        //  声明vo并封装
                        MyStatisticsDetailsVo vo = new MyStatisticsDetailsVo();
                        vo.setUserInfo(userInfo);
                        vo.setAlreadyLove(true);    // 是否喜欢他
                        vo.setMatchRate(RandomUtil.randomInt(60,99));  // 匹配度
                        // 将vo添加到voList
                        voList.add(vo);
                    }
                }

                // 2.4将voList添加到分页对象
                pageBeanVo.setItems(voList);
                // 2.5返回分页对象
                return ResponseEntity.ok(pageBeanVo);
            }
            case 3:{    // 查询喜欢我的
                // 3.1 获取分页对象
                PageBeanVo pageBeanVo = myStatisticsService.findFanUser(pageNum, pageSize, userId);
                // 3.2 获取userLikeList
                List<UserLike> userLikeList = (List<UserLike>) pageBeanVo.getItems();
                // 3.3 封装MyStatisticsVo
                // 3.3.1 声明voList
                List<MyStatisticsDetailsVo> voList = new ArrayList<>();
                // 3.3.2 遍历
                if (CollectionUtil.isNotEmpty(userLikeList)) {
                    for (UserLike userLike : userLikeList) {
                        // 获取喜欢我的人的id
                        Long likeMeUserId = userLike.getUserId();
                        // 根据我喜欢人的id查询他的userInfo
                        UserInfo userInfo = userInfoService.findUserInfoById(likeMeUserId);

                        //  声明vo并封装
                        MyStatisticsDetailsVo vo = new MyStatisticsDetailsVo();
                        vo.setUserInfo(userInfo);
                        vo.setMatchRate(RandomUtil.randomInt(60,99));  // 匹配度
                        // 查询自己是否喜欢对方
                        Boolean like = myStatisticsService.findLike(userId, likeMeUserId);
                        vo.setAlreadyLove(like);    // 是否喜欢他
                        // 将vo添加到voList
                        voList.add(vo);
                    }
                }
                // 3.4 将voList添加进分页对象
                pageBeanVo.setItems(voList);
                // 3.5 返回分页对象
                return ResponseEntity.ok(pageBeanVo);
            }
            case 4:{    // 查询历史访客
                // 4.2 获取分页对象
                PageBeanVo pageBeanVo = myStatisticsService.findVisitor(pageNum, pageSize, userId);
                // 4.2 获取visitorList
                List<Visitor> visitorList = (List<Visitor>) pageBeanVo.getItems();
                // 4.3 封装MyStatisticsVo
                // 4.3.1 声明voList
                List<MyStatisticsDetailsVo> voList = new ArrayList<>();
                // 4.3.2 遍历
                if (CollectionUtil.isNotEmpty(visitorList)) {
                    for (Visitor visitor : visitorList) {
                        // 获取访问人id
                        Long visitorUserId = visitor.getVisitorUserId();
                        // 根据访问人id查询userInfo
                        UserInfo userInfo = userInfoService.findUserInfoById(visitorUserId);
                        // 声明vo并封装
                        MyStatisticsDetailsVo vo = new MyStatisticsDetailsVo();
                        vo.setUserInfo(userInfo);   // 访问人详情
                        vo.setAlreadyLove(true);    // 是否喜欢他
                        vo.setMatchRate(RandomUtil.randomInt(60,99));   // 匹配度

                        //将vo添加到voList
                        voList.add(vo);
                    }
                }
                // 4.4 将voList添加进分页对象
                pageBeanVo.setItems(voList);
                // 4.5 返回分页对象
                return ResponseEntity.ok(pageBeanVo);
            }
        }
        // 3.封装vo
        return ResponseEntity.ok(0);
    }


    @Autowired
    private HuanXinTemplate huanXinTemplate;

    @DubboReference
    private CardsService cardsService;


    // 粉丝 - 取消喜欢
    public void noLikeFans(Long noLikeUserId) {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.删除用户好友
        // 2.1 我删除对方的好友
        friendService.deleteFriend(userId,noLikeUserId);
        // 2.2 对方删除我的好友
        friendService.deleteFriend(noLikeUserId,userId);
        // 3.删除朋友圈动态
        // 3.1 我删除我的好友动态表中对方的动态
        friendService.deleteFriendDynamic(userId,noLikeUserId);
        // 3.2 对方的好友动态表里删除我的动态
        friendService.deleteFriendDynamic(noLikeUserId,userId);
        // 4.删除好友环信好友
        // 4.1 我删除对方的好友
        huanXinTemplate.deleteContacts("HX" + userId,"HX" + noLikeUserId);
        // 4.2 对方删除我的好友
        //huanXinTemplate.deleteContacts("HX" + noLikeUserId,"HX" + userId);
        // 5.删除喜欢列表(只需要删除我自己的就可以了)
        cardsService.deleteCardsLove(noLikeUserId,userId);

    }
}
