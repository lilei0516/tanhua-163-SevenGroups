package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.itheima.app.controller.MessageController;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Friend;
import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CardsService;
import com.itheima.service.mongo.FriendService;
import com.itheima.vo.RecommendUserVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.*;

// app端探花卡片功能
@Component
public class CardsManager {

    @DubboReference
    private CardsService cardsService;

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private FriendService friendService;


    // 卡片推荐用户
    public ResponseEntity findCards() {
        // 1.获取当前用户userId
        Long userId = UserHolder.get().getId();
        // 2.查询RecommendUserList
        List<RecommendUser> recommendUserList = cardsService.findCards(userId);
        // 3.封装RecommendUserVo
        // 3.1声明voList
        List<RecommendUserVo> voList = new ArrayList<>();

        // 3.2 获取登录用户的所有好友id
        List<Friend> friendList = friendService.findCards(userId);

        // 3.3 将你的所有好友id循环放入一个数组中
        Long[] arr = new Long[friendList.size()+1];
        arr[0] = 0L;
        Integer i = 1;
        if (friendList.size()>0){
            for (Friend friend : friendList) {
                arr[i] = friend.getFriendId();
                i++;
            }
        }


       /* for (Long aLong : arr) {
            System.err.println(aLong);
        }
*/
        // 3.4遍历
        if (CollectionUtil.isNotEmpty(recommendUserList)) {
            for (RecommendUser recommendUser : recommendUserList) {
                RecommendUserVo recommendUserVo = new RecommendUserVo();
                // 获取userInfo(根据推荐的用户id查询信息,并排除推荐表中自己的好友)
                UserInfo userInfo = userInfoService.findCards(recommendUser.getUserId(), arr);

                // 封装recommendUserVo
                if (userInfo != null) {     // userInfo查询的有null值
                    recommendUserVo.setUserInfo(userInfo);

                    // 将vo添加到voList
                    voList.add(recommendUserVo);
                }


            }
        }

        // 每次发送给前端直接都打乱，实现每次展示的数据都顺序不同
        Collections.shuffle(voList);
        System.err.println(voList.size());
        System.out.println(voList);

        // 4.返回参数
        return ResponseEntity.ok(voList);
    }


    @Autowired
    private MessageController messageController;

    @Autowired
    private MessageManager messageManager;



    // app端探花卡片右滑喜欢
    public void findCardsLove(Long likeUserId) {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.根据你的id和你喜欢的id查询喜欢表
        UserLike userLike = cardsService.findCardsLove(likeUserId, userId);
        // 3 判断
        if (userLike == null){  // 如果她没有喜欢过你的话，添加好友
            /*// 3.1调用以前写的类，里面可以发送好友请求
            Map<String,String> map = new HashMap<>();
            map.put("userId",likeUserId.toString());
            map.put("reply","我在探花卡片喜欢了你哦~~~");
            // 发送加好友请求
            messageController.strangerQuestions(map);*/


            // 3.1发送完以后删除推荐用户表中的数据
            cardsService.deleteRecommendUser(likeUserId,userId);
            // 3.2在喜欢表中添加数据
            userLike = new UserLike();
            userLike.setCreated(System.currentTimeMillis()); // 创建时间
            userLike.setUserId(userId);  // 当前用户id
            userLike.setLikeUserId(likeUserId);  // 喜欢的用户id
            // 添加数据
            cardsService.saveCardsLove(userLike);
        }else {     // 如果她也喜欢过你那就直接成为好友
            // 3.1调用以前写的类，里面可以添加好友请
            Map<String,Long> map = new HashMap<>();
            map.put("userId",likeUserId);
            messageController.addContacts(map);
            // 3.2发送完以后删除推荐用户表中的数据
            cardsService.deleteRecommendUser(likeUserId,userId);
            // 3.3在喜欢表中添加数据
            userLike = new UserLike();
            userLike.setCreated(System.currentTimeMillis()); // 创建时间
            userLike.setUserId(userId);  // 当前用户id
            userLike.setLikeUserId(likeUserId);  // 喜欢的用户id
            // 添加数据
            cardsService.saveCardsLove(userLike);
        }



    }


    // app端探花卡片左滑不喜欢
    public void findCardsNoLove(Long noLikeUserId) {
        // 1.获取当前登录用户id
        Long userId = UserHolder.get().getId();
        // 2.删除推荐列表中过的数据
        cardsService.deleteRecommendUser(noLikeUserId,userId);
        // 3.反正以后不会刷到推荐了，如果她喜欢过我就给喜欢表里的数据删掉吧
        cardsService.deleteCardsLove(userId,noLikeUserId);
    }
}
