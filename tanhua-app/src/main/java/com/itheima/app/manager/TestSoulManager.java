package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.*;
import com.itheima.domain.mongo.SoulReport;
import com.itheima.service.db.TestSoulService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.db.UserService;
import com.itheima.service.mongo.SoulReportService;
import com.itheima.vo.*;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TestSoulManager {

    @DubboReference
    private TestSoulService testSoulService;

    @DubboReference
    private SoulReportService reportService;

    @DubboReference
    private UserInfoService userInfoService;

    // 获取调查问卷
    public ResponseEntity getQuestionnaire() {
        // 拿到当前userid
        Long userId = UserHolder.get().getId();
        // 查询问卷表，拿到一个问卷集合
        List<SoulQuestionnaire> questionnaireList = testSoulService.findQuestionnaire();

        // 返回前端的questionnaireVo的集合
        List<SoulQuestionnaireVo> questionnaireVoList = new ArrayList<>();

        // 判空遍历
        if (CollectionUtil.isNotEmpty(questionnaireList)) {
            for (SoulQuestionnaire questionnaire : questionnaireList) {
                // 拿到试卷id
                Long questionnaireId = questionnaire.getId();
                // 查询对应的题目表拿到题目集合(将问卷id传入作为条件查询题目)
                List<SoulExamination> examinationList = testSoulService.findExamination(questionnaireId);

                // 题目vo容器
                List<SoulExaminationVo> examinationVoList = new ArrayList<>();
                // 对题目集合进行遍历
                for (SoulExamination examination : examinationList) {
                    // 拿到一道题的id
                    Long examinationId = examination.getId();
                    // 查询这道题对应的所有选项(将题目id传入作为条件查询选项)
                    List<SoulOptions> optionList = testSoulService.findOptions(examinationId);

                    // 选项vo容器
                    List<SoulOptionsVo> optionVoList = new ArrayList<>();
                    // 遍历这个选项集合
                    for (SoulOptions option : optionList) {
                        // 封装选项vo
                        SoulOptionsVo optionVo = new SoulOptionsVo();
                        // 编号
                        optionVo.setId(option.getId().toString());
                        // 内容
                        optionVo.setOption(option.getOptions());
                        // 将选项vo添加到选项vo容器
                        optionVoList.add(optionVo);
                    }

                    // 封装题目Vo
                    SoulExaminationVo examinationVo = new SoulExaminationVo();
                    // 设置题目id
                    examinationVo.setId(examination.getId().toString());
                    // 设置题目
                    examinationVo.setQuestion(examination.getQuestion());
                    // 设置选项集合
                    examinationVo.setOptions(optionVoList);
                    // 将试题vo放入list
                    examinationVoList.add(examinationVo);
                }

                // 设置试卷vo
                SoulQuestionnaireVo questionnaireVo = new SoulQuestionnaireVo();
                // 设置id
                questionnaireVo.setId(questionnaire.getId().toString());
                // 设置图片
                questionnaireVo.setCover(questionnaire.getCover());

                // 设置难度等级
                questionnaireVo.setLevel(questionnaire.getLevel());
                // 设置名字
                questionnaireVo.setName(questionnaire.getName());
                // 设置试卷vo集合
                questionnaireVo.setQuestions(examinationVoList);
                // 设置星别
                questionnaireVo.setStar(questionnaire.getStar());
                // 设置最新报告id
                // 通过userId和questionnaireId获取此用户最新报告
                SoulReport report = reportService.findReport(userId, questionnaireId);

                if (report != null) {
                    // 如果有就设置
                    questionnaireVo.setReportId(report.getId().toHexString());
                }

                // TODO 解锁功能暂留
                // 如果是第一套，解锁
                if (questionnaire.getId() == 1) {
                    questionnaireVo.setIsLock(0);
                }
                // 如果上一套题，有了报告，那么这一套题就被解锁
                SoulReport lastReport = reportService.findReport(userId, questionnaireId - 1);
                if (lastReport != null) {
                    questionnaireVo.setIsLock(0);
                }

                // 添加到试卷集合
                questionnaireVoList.add(questionnaireVo);
            }
        }

        // 返回一个问卷Vo
        return ResponseEntity.ok(questionnaireVoList);
    }

    // 提交问卷
    public ResponseEntity saveQuestionnaire(List<Answers> answersList) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 总分计算
        Integer totalScore = 0;
        // 遍历这个答案集合
        for (Answers answers : answersList) {
            // 题目id
            String questionId = answers.getQuestionId();
            // 选项id
            String optionId = answers.getOptionId();
            // 拿到此题得分
            Integer score = testSoulService.findScore(questionId, optionId);
            // 计算总分
            totalScore = totalScore + score;
        }
        // 通过list集合其中一个选项获取这个问卷的id，判断是初、中、高级
        Long questionnaireId = testSoulService.findQuestionnaireId(answersList.get(0).getQuestionId());
        // 通过总分去查询描述表，获取对应的描述id
        Long conclusionId = testSoulService.findConclusionId(totalScore);

        // 通过userid和问卷id去查询此表存不存在
        SoulReport report = reportService.findReport(userId, questionnaireId);

        // 判断此报告是否存在
        if (report != null) {
            // 存在，更新
            // 设置总分
            report.setScore(totalScore);
            // 设置用户描述id
            report.setConclusionId(conclusionId);
            // 设置发布时间
            report.setCreated(System.currentTimeMillis());
            // 更新到mongo
            reportService.saveReport(report);
        } else {
            // 不存在，保存
            report = new SoulReport();
            // 设置主键id
            report.setId(ObjectId.get());
            // 设置总分
            report.setScore(totalScore);
            // 设置userid
            report.setUserId(userId);
            // 设置用户描述id
            report.setConclusionId(conclusionId);
            // 设置问卷编号id
            report.setQuestionnaireId(questionnaireId);
            // 设置发布时间
            report.setCreated(System.currentTimeMillis());
            // 保存到mongodb
            reportService.saveReport(report);
        }

        // 将报告主键返回
        return ResponseEntity.ok(report.getId().toHexString());
    }

    // 查询报告
    public ResponseEntity getReport(ObjectId reportId) {
        // 拿到自己的userid
        Long userId = UserHolder.get().getId();
        // 根据报告id直接查询
        SoulReport report = reportService.findReportById(reportId);
        // 拿到这个人的描述id
        Long conclusionId = report.getConclusionId();
        // 根据描述id查询相似的人（报告表里中描述id和此id相同所有的用户的集合）
        List<SoulReport> reportList = reportService.findSimilarYouList(conclusionId);
        // 装载similarYouVo的容器
        List<SimilarYouVo> similarYouVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(reportList)) {
            for (SoulReport similarReport : reportList) {
                // 得到相似的人的用户id
                Long similarUserId = similarReport.getUserId();
                // 不推荐自己或者不是同一套题
                if (similarUserId == userId || report.getQuestionnaireId() != similarReport.getQuestionnaireId() ) {
                    continue;
                }
                // 查询他的userinfo
                UserInfo userInfo = userInfoService.findUserInfoById(similarUserId);
                // 封装similaryouvo
                SimilarYouVo similarYouVo = new SimilarYouVo();
                similarYouVo.setId(similarUserId.intValue());
                similarYouVo.setAvatar(userInfo.getAvatar());
                // 封装到集合
                similarYouVoList.add(similarYouVo);
            }
        }
        // 装载similarYouVo的容器，写死
        List<DimensionsVo> dimensionsVoList = new ArrayList<>();
        dimensionsVoList.add(new DimensionsVo("外向", "80%"));
        dimensionsVoList.add(new DimensionsVo("判断", "70%"));
        dimensionsVoList.add(new DimensionsVo("抽象", "90%"));
        dimensionsVoList.add(new DimensionsVo("理性", "60%"));

        // 查询描述表
        SoulConclusion soulConclusion = testSoulService.findConclusionById(conclusionId);
        // 拿到描述文字
        String conclusion = soulConclusion.getConclusion();
        // 拿到鉴定图片
        String cover = soulConclusion.getCover();
        // 封装reportVo
        SoulReportVo soulReportVo = new SoulReportVo();
        soulReportVo.setConclusion(conclusion);
        soulReportVo.setCover(cover);
        soulReportVo.setDimensions(dimensionsVoList);
        soulReportVo.setSimilarYou(similarYouVoList);
        // 返回vo
        return ResponseEntity.ok(soulReportVo);
    }
}
