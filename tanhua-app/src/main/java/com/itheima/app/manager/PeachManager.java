package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.RandomUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.PickupCount;
import com.itheima.domain.mongo.Sound;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.PeachService;
import com.itheima.vo.PeachblossomVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class PeachManager {

    @DubboReference
    private PeachService peachService;

    @DubboReference
    private UserInfoService userInfoService;

    @Autowired
    private FastFileStorageClient client;

    @Autowired
    private FdfsWebServer webServer;

    // 接收语音
    public ResponseEntity getPeachblossom() {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 拿到sound表中的所有pickup为0的数据集合
        List<Sound> soundList = peachService.getPeachblossom();
        // 判空
        if (CollectionUtil.isNotEmpty(soundList)) {
            // 空集合，装载非自己的语音
            List<Sound> soundList1 = new ArrayList<>();
            // 遍历soundlist，挑出不是自己userid的语音
            for (Sound sound : soundList) {
                if (sound.getUserId() != userId) {
                    // 不是自己的语音添加到集合
                    soundList1.add(sound);
                }
            }
            // 对list1再进行判空
            if (CollectionUtil.isNotEmpty(soundList1)) {
                // 目标语音
                int index;
                // 若只有一个
                if (soundList1.size() == 1) {
                    index = 0;
                } else {
                    // 获取一个0到list.size-1的随机数
                    index = RandomUtil.randomInt(0, (soundList1.size() - 1));
                }

                // 拿到这个随机数指定的sound数据
                Sound sound = soundList1.get(index);
                // 将sound拾取置为1
                sound.setPickup(1);
                // 再存回到数据库
                peachService.saveSound(sound);
                // 发布语音者的userid
                Long soundUserId = sound.getUserId();
                // 找到这个发布者的userinfo
                UserInfo soundUserInfo = userInfoService.findUserInfoById(soundUserId);
                // 判断此用户的pickupcount数据是否存在
                PickupCount pickupCount = peachService.findPickupCount(userId);
                if (pickupCount == null) {
                    // 不存在，创建
                    pickupCount = new PickupCount();
                    // 本次做了听取，所以次数变成了9
                    pickupCount.setCount(9);
                    // 当前用户id
                    pickupCount.setUserId(userId);
                    // 设置更新时间
                    pickupCount.setCreated(System.currentTimeMillis());
                    // 保存到数据库
                    peachService.save(pickupCount);
                } else {
                    // 存在，更新
                    // 取出次数
                    Integer count = pickupCount.getCount();
                    // 减少一次拾取次数
                    pickupCount.setCount(count - 1);
                    // 更新到数据库
                    peachService.save(pickupCount);
                }

                // 封装前端需要的vo
                PeachblossomVo peachblossomVo = new PeachblossomVo();
                // 发布语音者id
                peachblossomVo.setId(soundUserId.intValue());
                // 设置头像
                peachblossomVo.setAvatar(soundUserInfo.getAvatar());
                // 设置昵称
                peachblossomVo.setNickname(soundUserInfo.getNickname());
                // 设置性别
                peachblossomVo.setGender(soundUserInfo.getGender());
                // 设置年龄
                peachblossomVo.setAge(soundUserInfo.getAge());
                // 设置语音地址
                peachblossomVo.setSoundUrl(sound.getSoundUrl());
                // 设置剩余次数
                peachblossomVo.setRemainingTimes(pickupCount.getCount());
                // 返回前端
                return ResponseEntity.ok(peachblossomVo);

            }

        }
        // 集合中无数据返回一个无数据vo
        return ResponseEntity.status(500).body("没有次数啦~");
    }

    // 发送语音
    public void savePeachblossom(MultipartFile soundFile) throws IOException {
        // 获取userId
        Long userId = UserHolder.get().getId();
        // 发送到云端
        StorePath storePath = client.uploadFile(
                soundFile.getInputStream(),
                soundFile.getSize(),
                FileUtil.extName(soundFile.getOriginalFilename()),
                null);
        // 拼接语音存储路径
        String soundUrl = webServer.getWebServerUrl() + storePath.getFullPath();

        // 封装sound实体类
        Sound sound = new Sound();
        sound.setUserId(userId);
        sound.setSoundUrl(soundUrl);
        sound.setPickup(0);
        sound.setCreated(System.currentTimeMillis());

        // 调用service层存入数据库
        peachService.saveSound(sound);
    }
}
