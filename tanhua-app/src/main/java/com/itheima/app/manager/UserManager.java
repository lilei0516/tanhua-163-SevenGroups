package com.itheima.app.manager;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.face.AipFaceTemplate;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import com.itheima.autoconfig.oss.OssTemplate;
import com.itheima.autoconfig.sms.SmsTemplate;
import com.itheima.domain.db.*;
import com.itheima.service.db.*;
import com.itheima.util.ConstantUtil;
import com.itheima.util.JwtUtil;
import com.itheima.vo.ErrorResult;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SettingVo;
import com.itheima.vo.UserInfoVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

@Component
public class UserManager {

    @DubboReference
    private UserService userService;

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private QuestionService questionService;

    @DubboReference
    private NotificationService notificationService;

    @DubboReference
    private BlackListService blackListService;

    @Autowired
    private AipFaceTemplate aipFaceTemplate;

    @Autowired
    private OssTemplate ossTemplate;

    @Autowired
    private SmsTemplate smsTemplatel;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private HuanXinTemplate huanXinTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 新增冻结service
    @DubboReference
    private DongJieService dongJieService;

    //通过手机号查询用户信息
    public ResponseEntity findUserByPhone(String phone) {
        return ResponseEntity.ok(userService.findUserByPhone(phone));
    }

    //保存用户信息
    public ResponseEntity save(User user) {
        int i =1/0;
        return ResponseEntity.ok(userService.save(user));
    }

    //获取验证码
    public void login(Map<String,String> map) {
        //随机生成六位数,(测试期间写死)
        // String code = RandomUtil.randomNumbers(6);
        String code = "123456";
        //发送短信,(测试期间不用)
        // smsTemplatel.sendSms(phone,code);
        //存入redis,五分钟存活时间
        String phone = map.get("phone");
        stringRedisTemplate.opsForValue().set(ConstantUtil.SMS_CODE+phone,code, Duration.ofMinutes(5));
    }

    //登录校验
    public ResponseEntity loginVerification(Map<String, String> map) {
        Map<String, Object> dataMap = new HashMap<>();
        //获取手机号和验证码
        String phone = map.get("phone");
        String verificationCode = map.get("verificationCode");
        //先对redis进行查询，校验验证码
        String codeFromRedis = stringRedisTemplate.opsForValue().get(ConstantUtil.SMS_CODE + phone);
        if(!codeFromRedis.equals(verificationCode)){
            //验证码不相等
            return ResponseEntity.status(500).body(ErrorResult.loginError());
        }
        //验证码无误后，查询数据库手机号是否存在
        User user = userService.findUserByPhone(phone);
        // 操作类型
        String type;
        if (user!=null) {
            /**
             * 操作类型
             * 0101为登录，0102为注册，0201为发动态，0202为浏览动态，0203为动态点赞，0204为动态喜欢，
             * 0205为评论，0206为动态取消点赞，0207为动态取消喜欢，0301为发小视频，0302为小视频点赞，
             * 0303为小视频取消点赞，0304为小视频评论
             */
            //存在，老用户isnew=false
            dataMap.put("isNew",false);
            // 新增代码，用户是否被冻结校验   TODO
            // 获取到用户详情
            UserInfo userInfo = userInfoService.findUserInfoById(user.getId());
            // 判断redis中是否存在倒计时
            Long id = user.getId();
            String djsj = stringRedisTemplate.opsForValue().get("djsj" + id);
            if (djsj != null){  // 存在
                // 根据冻结表查询出记录
                List<DongJie> dongJieList = dongJieService.findDjById(userInfo.getId());
                // 遍历
                for (DongJie dongJie : dongJieList) {
                    // 判断冻结范围
                    if (dongJie.getFreezingRange() == 1 && dongJie.getUserId() == user.getId() && dongJie.getSx() == 1) {
                        return ResponseEntity.status(500).body("用户被冻结！");
                    }
                }
            }else {     // 不存在
                userInfo.setUserStatus("1");
                userInfoService.update(userInfo);
            }
            // 老用户登录行为
            type = "0101";
        }else {
            //不存在，新用户isnew=true，并存入数据库
            dataMap.put("isNew",true);
            user = new User();
            //手机号存入
            user.setPhone(phone);
            //密码存入
            user.setPassword(ConstantUtil.INIT_PASSWORD);
            //返回主键id
            Long userId = userService.save(user);
            //设置主键id
            user.setId(userId);
            // 并且将用户id注册到环信云
            huanXinTemplate.register("HX"+userId);
            // 新用户注册行为
            type = "0102";
        }
        //制作token令牌
        //因为token会存在客户端，要删除敏感数据
        user.setPassword(null);
        //将user转为map
        Map<String, Object> userMap = BeanUtil.beanToMap(user);
        String token = JwtUtil.createToken(userMap);
        //将用户信息存入redis中做续期处理
        String userJSON = JSON.toJSONString(user);
        stringRedisTemplate.opsForValue().set(ConstantUtil.USER_TOKEN+token,userJSON, Duration.ofDays(3));
        //删除redis中的验证码
        stringRedisTemplate.delete(ConstantUtil.SMS_CODE + phone);
        //返回datamap集合（map集合中含有token，isnew）
        dataMap.put("token",token);

        // 创建log对象传到mq
        Log log = new Log();
        log.setUserId(user.getId());
        log.setLogTime(DateUtil.formatDate(new Date()));
        log.setPlace("辽宁抚顺");
        log.setEquipment("iPhone12");
        log.setType(type);
        // 发送给mq
        rabbitTemplate.convertAndSend("tanhua.log",log);

        return ResponseEntity.ok(dataMap);
    }

    //首次登陆，完善资料
    public void loginReginfo(UserInfo userInfo, String token) throws Exception {
        //通过token去user中查询用户id
        User user = findUserByToken(token);
        //设置userinfo的id
        userInfo.setId(user.getId());

        //计算生日
        String birthday = userInfo.getBirthday();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sdf.parse(birthday);
        long age = getAge(parse);
        userInfo.setAge((int) age);

        //将userinfo存入数据库
        userInfoService.save(userInfo);
    }

    //将公共方法通过token查询用户抽取出来
    public User findUserByToken(String token){
        //首先判断token不能为空
        if (StrUtil.isEmpty(token)) {
            //返回null
            return null;
        }
        //查询用户
        String userJSON = stringRedisTemplate.opsForValue().get(ConstantUtil.USER_TOKEN + token);
        //这份json不能为空
        if (StrUtil.isEmpty(userJSON)) {
            //返回null
            return null;
        }
        //将得到json转为user
        User user = JSON.parseObject(userJSON,User.class);
        return user;
    }

    //信息完善补充头像
    public ResponseEntity addPhoto(MultipartFile headPhoto, String token) throws IOException {
        //token判断
        User user = findUserByToken(token);
        //ai审核
        boolean flag = aipFaceTemplate.detect(headPhoto.getBytes());
        //不是人像
        if (flag==false) {
            return ResponseEntity.status(500).body(ErrorResult.faceError());
        }
        //是人像，上传的云端，返回一个路径图片
        String url = ossTemplate.upload(headPhoto.getOriginalFilename(), headPhoto.getInputStream());
        //new一个userinfo实体
        UserInfo userInfo = new UserInfo();
        //设置id
        userInfo.setId(user.getId());
        userInfo.setAvatar(url);
        userInfo.setCoverPic(url);
        userInfoService.addPhoto(userInfo);
        return ResponseEntity.ok(null);
    }

    //根据传来的id查询userinfovo
    public ResponseEntity findUserInfoVo(Long id) throws Exception {
        //查询用户详细信息
        UserInfo userInfo = userInfoService.findUserInfoById(id);
        //封装到userinfovo
        UserInfoVo userInfoVo = new UserInfoVo();
        BeanUtil.copyProperties(userInfo,userInfoVo);

        return ResponseEntity.ok(userInfoVo);
    }

    //更新userinfo
    public void updateUserInfo(UserInfo userInfo, String token) {
        User user = findUserByToken(token);
        //设置id
        userInfo.setId(user.getId());
        //存入
        userInfoService.update(userInfo);
    }

    //通用设置
    public ResponseEntity settings() {
        //从局部线程拿到user
        User user = UserHolder.get();
        //获取userid
        Long id = user.getId();
        //拿到question
        Question question = questionService.findQuestionById(id);
        //拿到notification
        Notification notification = notificationService.findNotificationById(id);
        //如果question不为null，设置值
        SettingVo settingVo = new SettingVo();
        if (question!=null) {
            settingVo.setStrangerQuestion(question.getStrangerQuestion());
        }
        //如果notification不为null，设置值
        if (notification!=null) {
            settingVo.setLikeNotification(notification.getLikeNotification());
            settingVo.setGonggaoNotification(notification.getGonggaoNotification());
            settingVo.setPinglunNotification(notification.getPinglunNotification());
        }
        settingVo.setId(id);
        settingVo.setPhone(user.getPhone());
        return ResponseEntity.ok(settingVo);
    }

    //生日计算
    public long getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) { //出生日期晚于当前时间，无法计算
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);  //当前年份
        int monthNow = cal.get(Calendar.MONTH);  //当前月份
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH); //当前日期
        cal.setTime(birthDay);
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
        int age = yearNow - yearBirth;   //计算整岁数
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;//当前日期在生日之前，年龄减一
            }else{
                age--;//当前月份在生日之前，年龄减一
            } } return age; }

    //设置陌生人问题
    public void setQuestion(Map<String, String> map) {
        //拿到content
        String content = map.get("content");
        //拿到当前用户的id
        Long userId = UserHolder.get().getId();
        //根据id查question表
        Question question = questionService.findQuestionById(userId);
        //判断question是否存在
        if (question==null) {
            //不存在，插入
            question = new Question();
            question.setUserId(userId);
            question.setStrangerQuestion(content);
            questionService.save(question);
        }else {
            //存在，更新
            question.setStrangerQuestion(content);
            questionService.update(question);
        }
    }

    //设置通知
    public void setNotifi(Notification notification) {
        //获取userid
        Long userId = UserHolder.get().getId();
        //通过userid查询通知设置
        Notification notificationById = notificationService.findNotificationById(userId);
        //判断结果是否为空
        if (notificationById==null) {
            //为空，保存
            notification.setUserId(userId);
            notificationService.save(notification);
        }else {
            //不为空，更新
            notificationById.setGonggaoNotification(notification.getGonggaoNotification());
            notificationById.setLikeNotification(notification.getLikeNotification());
            notificationById.setPinglunNotification(notification.getPinglunNotification());
            notificationService.update(notificationById);
        }
    }

    //分页查询黑名单
    public ResponseEntity getBlackList(Integer pageNum, Integer pageSize) {
        //获取当前用户id
        Long userId = UserHolder.get().getId();
        //将参数传入service层
        PageBeanVo pageBeanVo = blackListService.getBlackList(userId,pageNum,pageSize);
        //重新设置itemes
        List<BlackList> items = (List<BlackList>) pageBeanVo.getItems();
        //接收新的items
        ArrayList<UserInfo> newItems = new ArrayList<>();
        //遍历这个items
        if (items!=null && items.size()>0) {
            for (BlackList blackList : items) {
                //获取此条数据的blackuserid
                Long blackUserId = blackList.getBlackUserId();
                //查询此黑名单用户的详细信息
                UserInfo userInfo = userInfoService.findUserInfoById(blackUserId);
                //插入newitems
                newItems.add(userInfo);
            }
        }
        //重新设置items
        pageBeanVo.setItems(newItems);
        //返回给前端
        return ResponseEntity.ok(pageBeanVo);
    }

    //删除指定黑名单用户
    public void deleteOne(Long blackUId) {
        //获取当前用户id
        Long userId = UserHolder.get().getId();
        //将用户id和黑名单用户id传入service层
        blackListService.deleteOne(userId,blackUId);
    }



    // 修改手机号- 1 发送短信验证码
    public void sendVerificationCode() {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.根据用户id查询手机号
        User user = userService.findUserById(userId);
        // 3.调用之前的方法，发送验证码(需要一个map中存入phone字段)
        Map<String,String> map = new HashMap<>();
        map.put("phone",user.getPhone());
        // 4.发送验证码
        this.login(map);
    }


    // 修改手机号 - 2 校验验证码
    public ResponseEntity checkVerificationCode(String yzm) {
        // 声明返回结果的map
        Map<String,Boolean> map = new HashMap<>();
        // 1.获取当前用户手机号
        Long userId = UserHolder.get().getId();
        User user = userService.findUserById(userId);
        // 2.从redis中获取验证码
        String redisYZM = stringRedisTemplate.opsForValue().get(ConstantUtil.SMS_CODE + user.getPhone());
        // 3.校验验证码
        if (redisYZM.equals(yzm)){
            // 验证通过返回true
            map.put("verification",true);
            return ResponseEntity.ok(map);
        }else {
            // 没通过给个提示报错
            return ResponseEntity.status(500).body(ErrorResult.sendSmsError());
        }
    }


    // 修改手机号 - 3 保存手机号
    public void updatePhone(String phone) {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.把新的手机号给对象
        User user = new User();
        user.setId(userId);
        user.setPhone(phone);
        // 3.修改手机号
        userService.updatePhone(user);

    }
}
