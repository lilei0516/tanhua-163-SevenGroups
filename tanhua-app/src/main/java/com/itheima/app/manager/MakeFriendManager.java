package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.RecommendUser;
import com.itheima.domain.mongo.UserLike;
import com.itheima.domain.mongo.UserLocation;
import com.itheima.domain.mongo.Visitor;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CardsService;
import com.itheima.service.mongo.LocationService;
import com.itheima.service.mongo.RecommendUserService;
import com.itheima.service.mongo.VisitorService;
import com.itheima.util.ConstantUtil;
import com.itheima.vo.NearUserVo;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.RecommendUserVo;
import com.itheima.vo.VisitorVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MakeFriendManager {

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private RecommendUserService recommendUserService;

    @DubboReference
    private VisitorService visitorService;

    @DubboReference
    private LocationService locationService;

    @DubboReference
    private QuestionService questionService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 今日佳人推荐
    public ResponseEntity getTodayBest() {
        // 获取当前用户id
        Long toUserId = UserHolder.get().getId();
        // 调用service层获取佳人
        RecommendUser todayBest = recommendUserService.getTodayBest(toUserId);
        // 封装recuservo=佳人userinfo+缘分值
        RecommendUserVo recommendUserVo = new RecommendUserVo();
        UserInfo userInfo = new UserInfo();
        if (todayBest != null) {
            userInfo = userInfoService.findUserInfoById(todayBest.getUserId());
            recommendUserVo.setUserInfo(userInfo);
            recommendUserVo.setFateValue(todayBest.getScore().longValue());
        }
        // 返回给前端
        return ResponseEntity.ok(recommendUserVo);
    }

    // 推荐朋友
    public ResponseEntity getRecommendationList(Integer pageNum, Integer pageSize) {
        // 获取当前userid
        Long toUserId = UserHolder.get().getId();
        // 分页查询推荐用户表，获得一个pbv
        PageBeanVo pageBeanVo =
                recommendUserService.getRecommendationList(toUserId, pageNum, pageSize);
        // 判空遍历
        List<RecommendUser> items = (List<RecommendUser>) pageBeanVo.getItems();
        // 新的items容器
        List<RecommendUserVo> recommendUserVoList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(items)) {
            for (RecommendUser recommendUser : items) {
                RecommendUserVo recommendUserVo = new RecommendUserVo();
                // 查询userinfo
                UserInfo userInfo = userInfoService.findUserInfoById(recommendUser.getUserId());
                // 封装vo
                recommendUserVo.setUserInfo(userInfo);
                recommendUserVo.setFateValue(recommendUser.getScore().longValue());
                // 添加list
                recommendUserVoList.add(recommendUserVo);
            }
        }
        // 重新设置
        pageBeanVo.setItems(recommendUserVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 获取推荐用户详情
    public ResponseEntity getRecommendUserDetail(Integer recommendUserId) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 查询出指定的推荐用户
        RecommendUser recommendUser = null;
        if (recommendUserId == userId.longValue()) {
            recommendUser = new RecommendUser();
            // 如果推荐用户id和自己的id相同
            recommendUser.setScore(100.0);
            recommendUser.setUserId(userId);
            recommendUser.setCreated(System.currentTimeMillis());
            recommendUser.setToUserId(userId);
        } else {
            // 如果不同则查询
            recommendUser = recommendUserService.getRecommendUserDetail(userId, recommendUserId);
            // 如果只是查看好友的动态详细，在推荐表里没有找到
            // 判空
            if (recommendUser == null) {
                recommendUser = new RecommendUser();
                // 需要设置userid
                recommendUser.setUserId(recommendUserId.longValue());
                // 需要设置评分
                recommendUser.setScore(100.0);
            }

        }
        // 获取userinfo
        UserInfo userInfo = userInfoService.findUserInfoById(recommendUser.getUserId());
        // 封装revUservo
        RecommendUserVo recommendUserVo = new RecommendUserVo();
        recommendUserVo.setUserInfo(userInfo);
        recommendUserVo.setFateValue(recommendUser.getScore().longValue());
        return ResponseEntity.ok(recommendUserVo);
    }

    // 最近访客
    public ResponseEntity getVisitors() {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 从redis中获取上一次登录时间
        String lastLoginTime = stringRedisTemplate.opsForValue().get(ConstantUtil.LAST_ACCESS_TIME + userId);
        List<Visitor> visitorList = null;
        // 分情况判断
        if (StrUtil.isNotEmpty(lastLoginTime)) {
            // 非新用户，有记录
            // 调用service查询visitorlist
            visitorList = visitorService.getVisitors(userId, Long.parseLong(lastLoginTime));
        } else {
            // 新用户，无记录
            // 调用service查询visitorlist
            visitorList = visitorService.getVisitors(userId, Long.parseLong("0"));
        }
        // 需要返回的visitorlistvo
        List<VisitorVo> visitorVoList = new ArrayList<>();
        // 对visitorlist判空遍历
        if (CollectionUtil.isNotEmpty(visitorList)) {
            for (Visitor visitor : visitorList) {
                VisitorVo visitorVo = new VisitorVo();
                // 封装信息
                UserInfo userInfo = userInfoService.findUserInfoById(visitor.getVisitorUserId());
                visitorVo.setUserInfo(userInfo);
                visitorVo.setFateValue(visitor.getScore().longValue());
                // 添加
                visitorVoList.add(visitorVo);
            }
        }
        // 记录本次登录时间
        stringRedisTemplate.opsForValue().set(ConstantUtil.LAST_ACCESS_TIME + userId, System.currentTimeMillis() + "");

        // 返回数据
        return ResponseEntity.ok(visitorVoList);

    }

    // 上传地理位置信息
    public void saveNearUserLocation(Double latitude, Double longitude, String addrStr) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service传入参数
        locationService.saveNearUserLocation(latitude, longitude, addrStr, userId);
    }

    // 搜索附近的人
    public ResponseEntity serarchNearUser(String gender, Long distance) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 查询附近的人的id的集合
        List<Long> nearUserIdList = locationService.serarchNearUser(gender, distance, userId);
        // 需要返回的集合
        List<NearUserVo> nearUserVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(nearUserIdList)) {
            for (Long nearUserId : nearUserIdList) {
                NearUserVo nearUserVo = new NearUserVo();
                // 如果是自己就排除
                if (nearUserId == userId) {
                    continue;
                }
                // 查询userinfo
                UserInfo userInfo = userInfoService.findUserInfoById(nearUserId);
                // 如果是同性就排除
                if (!userInfo.getGender().equals(gender)) {
                    continue;
                }
                // 如果
                // 设置属性值
                nearUserVo.setAvatar(userInfo.getAvatar());
                nearUserVo.setNickname(userInfo.getNickname());
                nearUserVo.setUserId(userInfo.getId());
                // 添加到集合中
                nearUserVoList.add(nearUserVo);
            }
        }
        // 返回的是nearuservolist
        return ResponseEntity.ok(nearUserVoList);
    }

    // 聊一下获取陌生人问题
    public ResponseEntity strangerQuestions(Long userId) {
        // 调用陌生人问题查询
        Question question = questionService.findQuestionById(userId);
        // 有可能未设置，使用默认
        if (question == null) {
            question = new Question();
        }
        return ResponseEntity.ok(question.getStrangerQuestion());
    }

    @DubboReference
    private CardsService cardsService;

    // 是否喜欢他（她）:红心高亮
    public ResponseEntity findAlreadyLove(Long likeUserId) {
        // 1.获取当前用户id
        Long userId = UserHolder.get().getId();
        // 2.根据你的id和对方的id从喜欢的表里查询数据
        UserLike userLike = cardsService.findCardsLove(userId, likeUserId);
        // 2.判断,为空就是不喜欢
        if (userLike == null){
            return ResponseEntity.ok(false);
        }
        // 3.不为空就是喜欢
        return ResponseEntity.ok(true);
    }
}
