package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.FocusUser;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.Video;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CommentService;
import com.itheima.service.mongo.VideoService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.DateFormatUtil;
import com.itheima.vo.CommentVo;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.VideoVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class VideoManager {

    @DubboReference
    private CommentService commentService;

    @DubboReference
    private VideoService videoService;

    @DubboReference
    private UserInfoService userInfoService;

    @Autowired
    private FastFileStorageClient client;

    @Autowired
    private FdfsWebServer webServer;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MQVideoManager mqVideoManager;

    // 获取小视频列表
    @Cacheable(value = "tanhua",key = "'video_'+#userId+'_'+#pageNum+'_'+#pageSize")  // 制作缓存
    public PageBeanVo getSmallVideos(Integer pageNum, Integer pageSize, Long userId) {
        // 调用service查询推荐视频表，返回一个装着video的pbv
        PageBeanVo pageBeanVo = videoService.getSmallVideos(pageNum,pageSize,userId);
        List<Video> videoList = (List<Video>) pageBeanVo.getItems();
        // 新的集合
        List<VideoVo> videoVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(videoList)) {
            for (Video video : videoList) {
                // 设置videovo
                VideoVo videoVo = new VideoVo();
                // 查询发视频的用户详情
                UserInfo userInfo = userInfoService.findUserInfoById(video.getUserId());
                // 属性设置，添加到新的集合中
                videoVo.setUserInfo(userInfo);
                videoVo.setVideo(video);

                // 设置点赞次数返回给前端展示
                videoVo.setLikeCount(video.getLikeCount());

                // 设置hasfocus
                if (stringRedisTemplate.hasKey(StrUtil.format(ConstantUtil.VIDEO_LIKE,userId,video.getId()))){
                    // 如果有这个key
                    videoVo.setHasFocus(1);
                }
                videoVoList.add(videoVo);
            }
        }
        // 重新设置items
        pageBeanVo.setItems(videoVoList);
        return pageBeanVo;
    }

    @CacheEvict(value = "tanhua",key = "'video_'+#userId+'_*'")  // 清除缓存（全部清除）
    // 上传小视频
    public void saveSmallVideos(MultipartFile videoThumbnail, MultipartFile videoFile, Long userId) throws IOException {
        // 将封面和视频传到fastdfs上并获取url
        // 图片上传
        StorePath storePath1 = client.uploadFile(
                // 图片流
                videoThumbnail.getInputStream(),
                // 图片大小
                videoThumbnail.getSize(),
                // 图片扩展名
                FileUtil.extName(videoThumbnail.getOriginalFilename()),
                // 图片信息
                null);
        // 图片的完整访问路径
        String picUrl = webServer.getWebServerUrl()+storePath1.getFullPath();

        // 视频上传
        StorePath storePath2 = client.uploadFile(
                // 图片流
                videoFile.getInputStream(),
                // 图片大小
                videoFile.getSize(),
                // 图片扩展名
                FileUtil.extName(videoFile.getOriginalFilename()),
                // 图片信息
                null);
        // 视频的完整访问路径
        String videoUrl = webServer.getWebServerUrl()+storePath2.getFullPath();

        // 设置video属性并保存到数据库
        Video video = new Video();
        // 手动设置一个videoid，因为需要我们把自己推荐给自己
        video.setId(ObjectId.get());
        video.setVid(null);  //TODO 这个在service设置
        video.setCreated(System.currentTimeMillis());
        video.setUserId(userId);
        video.setText("呵呵呵呵呵");
        video.setPicUrl(picUrl);
        video.setVideoUrl(videoUrl);
        videoService.saveSmallVideos(video);

        // 将video发送给mq
        mqVideoManager.sendMovement(userId,video.getId(),MQVideoManager.VIDEO_PUBLISH);

    }

    @CacheEvict(value = "tanhua",key = "'video_'+#userId+'_*'")  // 清除缓存（全部清除）
    // 关注用户
    public void userFocus(Long userId, Long focusUserId) {
        // 创建一个关注表信息
        FocusUser focusUser = new FocusUser();
        // 设置属性
        focusUser.setCreated(System.currentTimeMillis());
        focusUser.setUserId(userId);
        focusUser.setFocusUserId(focusUserId);
        // 保存到数据库
        videoService.userFocus(focusUser);
        // 将关注信息高亮记录在redis当中
        stringRedisTemplate.opsForValue().set(StrUtil.format(ConstantUtil.VIDEO_LIKE,userId,focusUserId),"1");
    }

    @CacheEvict(value = "tanhua",key = "'video_'+#userId+'_*'")  // 清除缓存（全部清除）
    // 取关用户
    public void userUnFocus(Long userId, Long focusUserId) {
        // 删除指定的关注关系
        videoService.userUnFocus(userId,focusUserId);
        // 将redis中的记录删除
        stringRedisTemplate.delete(StrUtil.format(ConstantUtil.VIDEO_LIKE,userId,focusUserId));
    }
    //小视频点赞
    @CacheEvict(value = "tanhua",key = "'video_'+#userId+'_*'")  // 清除缓存（全部清除）
    public ResponseEntity setVideoLike(String videoId, Long userId) {
        // // 1.获取线程内userId
        // Long userId = UserHolder.get().getId();
        // 2.根据videoId查询动态详情
        Video video = videoService.findVideoById(new ObjectId(videoId));
        // 3.封装comment对象
        Comment comment = new Comment();
        comment.setCreated(System.currentTimeMillis()); // 点赞时间
        comment.setPublishId(video.getId()); // 动态id
        comment.setCommentType(4); // 操作类型
        comment.setUserId(userId); // 操作人
        comment.setPublishUserId(video.getUserId()); // 动态发布人id
        // 4.调用rpc保存
        Integer likeCount = commentService.saveComment(comment);
        // 5.redis存储点赞状态
        stringRedisTemplate.opsForValue().set(StrUtil.format(ConstantUtil.VIDEO_LIKE, userId, videoId), "4");
        return ResponseEntity.ok(likeCount);
    }
    //小视频取消点赞
    @CacheEvict(value = "tanhua",key = "'video_'+#userId+'_*'")  // 清除缓存（全部清除）
    public ResponseEntity removeVideoLike(String videoId, Long userId) {
        // // 1.获取线程内userId
        // Long userId = UserHolder.get().getId();
        // 2.调用rpc删除
        Integer likeCount = commentService.removeComment(new ObjectId(videoId), userId, 4);
        // 3.删除redis点赞状态
        stringRedisTemplate.delete(StrUtil.format(ConstantUtil.VIDEO_LIKE, userId, videoId));
        return ResponseEntity.ok(likeCount);
    }
    // 查看视频评论列表
    public ResponseEntity findCommentVo(String publishId, Integer pageNum, Integer pageSize) {
        // 1.调用rpc分页查询评论分页对象
        PageBeanVo pageBeanVo = commentService.findByPage(new ObjectId(publishId), 5, pageNum, pageSize);
        // 2.封装commentVo
        // 2.1 获取 commentList
        List<Comment> commentList = (List<Comment>) pageBeanVo.getItems();
        // 2.2 声明 voList
        List<CommentVo> voList = new ArrayList<>();
        // 2.3 遍历 commentList
        if (CollectionUtil.isNotEmpty(commentList)) {
            for (Comment comment : commentList) {
                // 创建vo
                CommentVo vo = new CommentVo();
                vo.setId(comment.getId().toHexString()); // 评论id
                UserInfo userInfo = userInfoService.findUserInfoById(comment.getUserId());
                vo.setAvatar(userInfo.getAvatar());// 评论人头像
                vo.setNickname(userInfo.getNickname()); // 评论人昵称
                vo.setContent(comment.getContent()); // 评论内容
                vo.setCreateDate(DateFormatUtil.format(new Date(comment.getCreated()))); // 评论时间

                // 封装评论喜欢数（这里需要给前端展示点赞数）
                vo.setLikeCount(comment.getLikeCount());
                // 判断当前用户是否对此动态点赞
                if(stringRedisTemplate.hasKey(StrUtil.format("comment_like:{}_{}"
                        ,UserHolder.get().getId(),comment.getId()))){
                    vo.setHasLiked(1);
                }

                // 添加到voList
                voList.add(vo);
            }
        }
            // 3.将vo设置到分页对象
            pageBeanVo.setItems(voList);

            // 4.返回分页对象
            return ResponseEntity.ok(pageBeanVo);
    }
    //给小视频评论
    public void setMovementComment(String publishId, String content) {
        // 1.获取线程内userId
        Long userId = UserHolder.get().getId();
        // 2.查询动态详情
        Video video = videoService.findVideoById(new ObjectId(publishId));
        // 3.封装comment对象
        Comment comment = new Comment();
        comment.setCreated(System.currentTimeMillis());//评论时间
        comment.setPublishId(video.getId());//id
        comment.setCommentType(5); // 操作类型
        comment.setUserId(userId);// 操作人
        comment.setPublishUserId(video.getUserId()); // 动态发布人
        comment.setContent(content);
        // 4.调用rpc保存
        commentService.saveComment(comment);
    }

    // 对视频评论进行点赞
    public ResponseEntity commentLike(ObjectId objectId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service，获取这条评论
        Comment comment = videoService.commentLike(objectId);
        // 重新设置这条评论的likecount
        comment.setLikeCount(comment.getLikeCount()+1);

        // 新增一条对这条评论的点赞记录
        Comment commentLikeForThis = new Comment();
        commentLikeForThis.setCreated(System.currentTimeMillis());
        commentLikeForThis.setPublishId(comment.getId());
        // 对评论点赞
        commentLikeForThis.setCommentType(6);
        commentLikeForThis.setUserId(userId);
        // 发这条评论的人
        commentLikeForThis.setPublishUserId(comment.getUserId());

        //将两条信息保存到数据库
        videoService.saveComment(comment,commentLikeForThis);
        // 向redis存入喜欢状态码
        stringRedisTemplate.opsForValue()
                .set(StrUtil.format("comment_like:{}_{}"
                        ,userId,comment.getId()),"1");
        // 将最新的点赞记录返回给前端
        Integer likeCount = comment.getLikeCount();
        return ResponseEntity.ok(likeCount);
    }
    // 对视频评论取消点赞
    public ResponseEntity commentDisLike(ObjectId objectId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service，获取这条评论
        Comment comment = videoService.commentLike(objectId);
        // 重新设置这条评论的likecount
        comment.setLikeCount(comment.getLikeCount()-1);

        // 删除这个点赞记录（通过寻找publishid和userid定位,顺便把comment更新）
        videoService.removeLikeCount(objectId,userId,comment);
        // 删除redis中的高亮
        stringRedisTemplate.delete(StrUtil.format("comment_like:{}_{}"
                ,userId,comment.getId()));
        // 返回最新点赞数
        Integer likeCount = comment.getLikeCount();
        return ResponseEntity.ok(likeCount);
    }
}
