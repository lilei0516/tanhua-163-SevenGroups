package com.itheima.app.manager;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.itheima.app.exception.BusinessException;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.autoconfig.oss.OssTemplate;
import com.itheima.domain.db.DongJie;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.service.db.DongJieService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CommentService;
import com.itheima.service.mongo.MovementService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.DateFormatUtil;
import com.itheima.vo.CommentVo;
import com.itheima.vo.MovementVo;
import com.itheima.vo.PageBeanVo;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class MovementManager {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private OssTemplate ossTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MQMovementManager mqMovementManager;

    @DubboReference
    private MovementService movementService;

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private CommentService commentService;

    @DubboReference
    private DongJieService dongJieService;

    // 保存动态
    // public void saveMovement(Movement movement, MultipartFile[] imageContent) throws IOException {
    //     List<String> list = new ArrayList<>();
    //     // 获取当前用户id
    //     Long userId = UserHolder.get().getId();
    //     // 封装用户id
    //     movement.setUserId(userId);
    //     // 封装媒体数据
    //     for (MultipartFile multipartFile : imageContent) {
    //         // 向云端上传图片
    //         String url = ossTemplate.upload(multipartFile.getOriginalFilename(), multipartFile.getInputStream());
    //         // 将返回的url存入容器
    //         list.add(url);
    //     }
    //     // 设置主键
    //     movement.setId(ObjectId.get());
    //     // 将容器存入medias
    //     movement.setMedias(list);
    //     // 设置state，假设都审核通过1
    //     movement.setState(1);
    //     // 设置发布时间（当前系统时间）
    //     movement.setCreated(System.currentTimeMillis());
    //     // 查看权限（暂未加入）
    //     movement.setSeeType(1);  //TODO 假定公开
    //     // 存入数据库
    //     movementService.saveMovement(movement);
    // }

    // 查看自己的所有动态
    public ResponseEntity getMyMovements(Integer pageNum, Integer pageSize, Long userId) {
        // 将分页参数传给service，返回一个pageBeanVo，这个bean里的list存的都是movement详细信息
        PageBeanVo pageBeanVo = movementService.getMyMovements(pageNum,pageSize,userId);
        // 遍历这个vo中的list
        List<Movement> list = (List<Movement>) pageBeanVo.getItems();
        // 新的itemslist
        ArrayList<MovementVo> movementVoList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(list)) {
            for (Movement movement : list) {
                MovementVo movementVo = new MovementVo();
                // 查询用户详情
                UserInfo userInfo = userInfoService.findUserInfoById(userId);
                movementVo.setUserInfo(userInfo);
                movementVo.setMovement(movement);
                // 判断当前用户是否对此动态点赞
                if(stringRedisTemplate.hasKey(StrUtil.format(ConstantUtil.MOVEMENT_LIKE
                        ,UserHolder.get().getId(),movement.getId().toHexString()))){
                    movementVo.setHasLiked(1);
                }
                // 填入集合
                movementVoList.add(movementVo);
            }
        }
        // 重新设置pageBean并返回
        pageBeanVo.setItems(movementVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查看我的好友所有动态
    public ResponseEntity getFriendMovements(Integer pageNum, Integer pageSize) {
        // 获取当前id
        Long userId = UserHolder.get().getId();
        // 拿到一个封装着详细动态的items的pagebeanvo
        PageBeanVo pageBeanVo = movementService.getFriendMovements(pageNum,pageSize,userId);
        // 遍历这个items
        List<Movement> movementList = (List<Movement>) pageBeanVo.getItems();
        // 新的items集合
        List<MovementVo> newList = new ArrayList<>();
        // 判空
        if (CollectionUtil.isNotEmpty(movementList)) {
            for (Movement movement : movementList) {
                // 获取对应的好友id及好友详细信息
                Long userId1 = movement.getUserId();
                UserInfo userInfoById = userInfoService.findUserInfoById(userId1);
                //设置userinfo
                MovementVo movementVo = new MovementVo();
                movementVo.setUserInfo(userInfoById);
                movementVo.setMovement(movement);
                // 判断当前用户是否对此动态点赞
                if(stringRedisTemplate.hasKey(StrUtil.format(ConstantUtil.MOVEMENT_LIKE
                        ,UserHolder.get().getId(),movement.getId().toHexString()))){
                    movementVo.setHasLiked(1);
                }
                //向集合添加
                newList.add(movementVo);
            }
        }
        // 重新设置items
        pageBeanVo.setItems(newList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查看推荐的动态
    public ResponseEntity getRecommendMovements(Integer pageNum, Integer pageSize) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 将分页条件传入service得到一个装着详细动态信息items的pagebeanvo
        PageBeanVo pageBeanVo = movementService.getRecommendMovements(pageNum,pageSize,userId);
        // 拿到这个items
        List<Movement> items = (List<Movement>) pageBeanVo.getItems();
        // 创建一个新的itemslist
        List<MovementVo> movementVoArrayList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(items)) {
            for (Movement movement : items) {
                // 查询用户详细信息
                Long userId1 = movement.getUserId();
                UserInfo userInfoById = userInfoService.findUserInfoById(userId1);
                // 将两个详细信息封装到movementvo
                MovementVo movementVo = new MovementVo();
                movementVo.setUserInfo(userInfoById);
                movementVo.setMovement(movement);
                // 判断当前用户是否对此动态点赞
                if(stringRedisTemplate.hasKey(StrUtil.format(ConstantUtil.MOVEMENT_LIKE
                        ,UserHolder.get().getId(),movement.getId().toHexString()))){
                    movementVo.setHasLiked(1);
                }
                // 判断当前用户是否对此动态喜欢
                if(stringRedisTemplate.hasKey(StrUtil.format(ConstantUtil.MOVEMENT_LOVE
                        ,UserHolder.get().getId(),movement.getId().toHexString()))){
                    movementVo.setHasLoved(1);
                }
                // 将mvoementvo装入新的items集合
                movementVoArrayList.add(movementVo);
            }
        }
        // 重新设置items并且返回
        pageBeanVo.setItems(movementVoArrayList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 练习-发布（保存）动态
    public ResponseEntity saveMovement(Movement movement, MultipartFile[] imageContent) throws IOException {

        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 新增代码，用户是否被冻结校验   TODO
        // 获取到用户详情
        UserInfo userInfo = userInfoService.findUserInfoById(userId);
        // 判断redis中是否存在倒计时
        //Long id = user.getId();
        String djsj = stringRedisTemplate.opsForValue().get("djsj" + userId);
        if (djsj != null){  // 存在
            // 根据冻结表查询出记录
            List<DongJie> dongJieList = dongJieService.findDjById(userInfo.getId());
            // 遍历
            for (DongJie dongJie : dongJieList) {
                // 判断冻结范围
                if (dongJie.getFreezingRange() == 3 && dongJie.getUserId() == userId && dongJie.getSx() == 1) {
                    //throw new BusinessException("用户已被冻结发布动态，禁止发布！");
                    return ResponseEntity.status(500).body("用户已被冻结发布动态，禁止发布！");
                }
            }
        }
        // 用来存储url
        List<String> list = new ArrayList<>();
        // 补全movement数据
        for (MultipartFile multipartFile : imageContent) {
            // 向oss中上传图片
            String url = ossTemplate.upload(multipartFile.getOriginalFilename(), multipartFile.getInputStream());
            // 存入list
            list.add(url);
        }
        // 手动设置一个id
        movement.setId(ObjectId.get());
        // 设置medias属性
        movement.setMedias(list);
        // 设置userid
        movement.setUserId(userId);
        // 设置state为1
        movement.setState(0);  // 未审核状态
        // 设置发布时间
        movement.setCreated(System.currentTimeMillis());
        // 设置seetype为1
        movement.setSeeType(1);

        // 向mq传入此动态id
        rabbitTemplate.convertAndSend("tanhua.movement.state",movement.getId().toHexString());

        // 将movement传入service层
        movementService.saveMovement(movement);

        // 向mq中发送消息
        mqMovementManager.sendMovement(userId,movement.getId(),1);

        return null;
    }

    // 动态点赞
    public ResponseEntity saveCommentLike(String publishId) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 查询动态详情
        Movement movement = movementService.findMovementById(new ObjectId(publishId));
        // 封装comment
        Comment comment = new Comment();
        // 发表时间
        comment.setCreated(System.currentTimeMillis());
        // 动态id
        comment.setPublishId(movement.getId());
        // 操作类型
        comment.setCommentType(1);
        // 操作人
        comment.setUserId(userId);
        // 被操作对象的所属人
        comment.setPublishUserId(movement.getUserId());
        // 调用service并返回动态点赞数
        Integer likeCount = commentService.saveComment(comment);
        // 向redis存储点赞状态码
        stringRedisTemplate.opsForValue()
                .set(StrUtil.format(ConstantUtil.MOVEMENT_LIKE, userId, publishId), "1");
        return ResponseEntity.ok(likeCount);
    }

    // 动态取消点赞
    public ResponseEntity removeCommentLike(String publishId) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 传入所需要的参数，并返回点赞数
        Integer likeCount = commentService.removeComment(new ObjectId(publishId), userId, 1);
        // 删除redis中的状态码
        stringRedisTemplate.delete(StrUtil.format(ConstantUtil.MOVEMENT_LIKE, userId, publishId));
        return ResponseEntity.ok(likeCount);
    }

    // 动态喜欢
    public ResponseEntity saveCommentLove(String publishId) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 根据pubid查询动态详情
        Movement movement = movementService.findMovementById(new ObjectId(publishId));
        // 封装comment
        Comment comment = new Comment();
        // 发表时间
        comment.setCreated(System.currentTimeMillis());
        // 动态id
        comment.setPublishId(movement.getId());
        // 操作类型
        comment.setCommentType(3);
        // 操作人
        comment.setUserId(userId);
        // 被才做对象的所属用户
        comment.setPublishUserId(movement.getUserId());
        // 存入数据库，并返回喜欢数
        Integer loveCount = commentService.saveComment(comment);
        // 向redis存入喜欢状态码
        stringRedisTemplate.opsForValue()
                .set(StrUtil.format(ConstantUtil.MOVEMENT_LOVE,userId,publishId),"1");
        return ResponseEntity.ok(loveCount);
    }

    // 动态取消喜欢
    public ResponseEntity removeCommentLove(String publishId) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 传入所需参数
        Integer loveCount = commentService.removeComment(new ObjectId(publishId), userId, 3);
        // 删除redis中的状态
        stringRedisTemplate.delete(StrUtil.format(ConstantUtil.MOVEMENT_LOVE,userId,publishId));
        // 返回
        return ResponseEntity.ok(loveCount);

    }

    // 单条动态查询
    public ResponseEntity findMovement(String publishId) {
        // 拿到当前动态详情
        Movement movement = movementService.findMovementById(new ObjectId(publishId));
        // 封装给movementvo
        MovementVo movementVo = new MovementVo();
        // 查询userinfo
        UserInfo userInfo = userInfoService.findUserInfoById(movement.getUserId());
        movementVo.setUserInfo(userInfo);
        movementVo.setMovement(movement);
        return ResponseEntity.ok(movementVo);
    }

    // 评论列表查询
    public ResponseEntity findCommentsList(String publishId, Integer pageNum, Integer pageSize) {

        // 分页查询某一条动态下的所有评论
        PageBeanVo pageBeanVo = commentService.findCommentsListByPage(pageNum,pageSize,new ObjectId(publishId),2);
        // 拿到评论内容
        List<Comment> comments = (List<Comment>) pageBeanVo.getItems();
        // 装载commentvo的容器
        List<CommentVo> commentVoList = new ArrayList<>();
        // 判空遍历这个评论集合
        if (CollectionUtil.isNotEmpty(comments)) {
            // 属性封装
            for (Comment comment : comments) {
                // 封装CommentVo
                CommentVo commentVo = new CommentVo();
                // 封装id
                commentVo.setId(comment.getId().toHexString());
                // 封装时间（每个评论者发评论的时间）
                commentVo.setCreateDate(DateFormatUtil.format(new Date(comment.getCreated())));
                // 封装评论
                commentVo.setContent(comment.getContent());
                // 查询每个评论者的详细信息
                UserInfo userInfoById = userInfoService.findUserInfoById(comment.getUserId());
                // 封装头像
                commentVo.setAvatar(userInfoById.getAvatar());
                // 封装昵称
                commentVo.setNickname(userInfoById.getNickname());
                // 封装评论喜欢数
                commentVo.setLikeCount(comment.getLikeCount());
                // 判断当前用户是否对此动态点赞
                if(stringRedisTemplate.hasKey(StrUtil.format("comment_like:{}_{}"
                        ,UserHolder.get().getId(),comment.getId()))){
                    commentVo.setHasLiked(1);
                }
                // 向集合中添加
                commentVoList.add(commentVo);
            }
        }
        // 更新pagebeanvo
        pageBeanVo.setItems(commentVoList);
        // 返回前端
        return ResponseEntity.ok(pageBeanVo);
    }

    // 发布（保存）评论
    public ResponseEntity saveComment(String publishId, String content) {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        // 新增代码，用户是否被冻结校验   TODO
        // 获取到用户详情
        UserInfo userInfo = userInfoService.findUserInfoById(userId);
        // 判断redis中是否存在倒计时
        //Long id = user.getId();
        String djsj = stringRedisTemplate.opsForValue().get("djsj" + userId);
        if (djsj != null){  // 存在
            // 根据冻结表查询出记录
            List<DongJie> dongJieList = dongJieService.findDjById(userInfo.getId());
            // 遍历
            for (DongJie dongJie : dongJieList) {
                // 判断冻结范围
                if (dongJie.getFreezingRange() == 2 && dongJie.getUserId() == userId && dongJie.getSx() == 1) {
                    //throw new BusinessException("用户已被冻结发布动态，禁止发布！");
                    return ResponseEntity.status(500).body("用户已被冻结发布评论，禁止评论！");
                }
            }
        }
        // 封装comment
        Comment comment = new Comment();
        // 封装评论时间
        comment.setCreated(System.currentTimeMillis());
        // 封装动态id
        comment.setPublishId(new ObjectId(publishId));
        // 封装操作类型
        comment.setCommentType(2);
        // 封装操作人
        comment.setUserId(userId);
        // 封装被操作对象的userid
        Movement movement = movementService.findMovementById(new ObjectId(publishId));
        comment.setPublishUserId(movement.getUserId());
        // 封装评论内容
        comment.setContent(content);
        // 存入数据库
        commentService.saveComment(comment);
        return null;
    }

    // 对评论进行点赞
    public ResponseEntity commentLike(ObjectId objectId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service，获取这条评论
        Comment comment = movementService.commentLike(objectId);
        // 重新设置这条评论的likecount
        comment.setLikeCount(comment.getLikeCount()+1);

        // 新增一条对这条评论的点赞记录
        Comment commentLikeForThis = new Comment();
        commentLikeForThis.setCreated(System.currentTimeMillis());
        commentLikeForThis.setPublishId(comment.getId());
        // 对评论点赞
        commentLikeForThis.setCommentType(6);
        commentLikeForThis.setUserId(userId);
        // 发这条评论的人
        commentLikeForThis.setPublishUserId(comment.getUserId());

        //将两条信息保存到数据库
        movementService.saveComment(comment,commentLikeForThis);
        // 向redis存入喜欢状态码
        stringRedisTemplate.opsForValue()
                .set(StrUtil.format("comment_like:{}_{}"
                        ,userId,comment.getId()),"1");
        // 将最新的点赞记录返回给前端
        Integer likeCount = comment.getLikeCount();
        return ResponseEntity.ok(likeCount);
    }

    // 对评论取消点赞
    public ResponseEntity commentDisLike(ObjectId objectId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        // 调用service，获取这条评论
        Comment comment = movementService.commentLike(objectId);
        // 重新设置这条评论的likecount
        comment.setLikeCount(comment.getLikeCount()-1);

        // 删除这个点赞记录（通过寻找publishid和userid定位,顺便把comment更新）
        movementService.removeLikeCount(objectId,userId,comment);
        // 删除redis中的高亮
        stringRedisTemplate.delete(StrUtil.format("comment_like:{}_{}"
                ,userId,comment.getId()));
        // 返回最新点赞数
        Integer likeCount = comment.getLikeCount();
        return ResponseEntity.ok(likeCount);
    }
}
