package com.itheima.app.exception;

import com.itheima.vo.ErrorResult;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobleExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity handlerExceptionMethod(Exception e){
        e.printStackTrace();
        return ResponseEntity.status(500).body(ErrorResult.error());
    }
}
