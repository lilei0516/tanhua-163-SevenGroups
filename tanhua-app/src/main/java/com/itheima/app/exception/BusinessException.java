package com.itheima.app.exception;

public class BusinessException extends RuntimeException{

    // 自定义异常
    public BusinessException(String msg) {
        super(msg);
    }
}
