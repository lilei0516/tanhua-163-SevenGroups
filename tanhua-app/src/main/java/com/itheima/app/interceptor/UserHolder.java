package com.itheima.app.interceptor;

import com.itheima.domain.db.User;

public class UserHolder {

    private static final ThreadLocal<User> THREAD_LOCAL = new ThreadLocal();

    //设置
    public static void set(User user){
        THREAD_LOCAL.set(user);
    }
    //获取
    public static User get(){
        return THREAD_LOCAL.get();
    }
    //删除这个局部线程
    public static void rm(){
        THREAD_LOCAL.remove();
    }

}
