package com.itheima.app.interceptor;

import com.itheima.app.manager.UserManager;
import com.itheima.domain.db.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TokenInterceptor implements HandlerInterceptor {

    @Autowired
    private UserManager userManager;

    //预处理
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //获取请求头中的token
        String token = request.getHeader("Authorization");
        //如果token为空
        if (token==null) {
            //响应401
            response.setStatus(401);
            //拦截
            return false;
        }
        User user = userManager.findUserByToken(token);
        if (user==null) {
            //响应401
            response.setStatus(401);
            //拦截
            return false;
        }
        //存入局部线程
        UserHolder.set(user);
        //放行
        return true;
    }

    //最终处理
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        //删除这个局部线程
        UserHolder.rm();
    }
}
