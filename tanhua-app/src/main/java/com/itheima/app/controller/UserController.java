package com.itheima.app.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.app.manager.UserManager;
import com.itheima.domain.db.Notification;
import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;
import com.itheima.vo.UserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserManager userManager;

    //拦截手机号查询用户请求
    @GetMapping("/user/finduserbyphone")
    public ResponseEntity findUserByPhone(String phone){
        return userManager.findUserByPhone(phone);
    }
    //拦截保存用户请求
    @PostMapping("/user/save")
    public ResponseEntity save(@RequestBody User user){
        return userManager.save(user);
    }
    //获取验证码
    @PostMapping("/user/login")
    public void login(@RequestBody Map<String,String> map){
        userManager.login(map);
    }
    //登录校验
    @PostMapping("/user/loginVerification")
    public ResponseEntity loginVerification(@RequestBody Map<String,String> map){
        return userManager.loginVerification(map);
    }
    //首次登陆，完善资料
    @PostMapping("/user/loginReginfo")
    public void loginReginfo(@RequestBody UserInfo userInfo,
                             @RequestHeader("Authorization") String token) throws Exception {
        userManager.loginReginfo(userInfo,token);
    }
    //用户详细补充头像
    @PostMapping({"/user/loginReginfo/head","/users/header"})
    public ResponseEntity addPhoto(MultipartFile headPhoto,
                         @RequestHeader("Authorization") String token) throws IOException {
        return userManager.addPhoto(headPhoto,token);
    }
    //查询用户资料
    @GetMapping("/users")
    public ResponseEntity users(Long userID,Long huanxinID,
                                @RequestHeader("Authorization") String token) throws Exception {
        if (userID!=null) {
            //根据userid查
            ResponseEntity userInfoVo = userManager.findUserInfoVo(userID);
            //返回
            return userInfoVo;
        }else if(huanxinID!=null){
            //根据环信id查
            ResponseEntity userInfoVo = userManager.findUserInfoVo(huanxinID);
            //返回
            return userInfoVo;
        }else {
            //根据token查
            User user = userManager.findUserByToken(token);
            //返回
            ResponseEntity userInfoVo = userManager.findUserInfoVo(user.getId());
            return userInfoVo;
        }
    }
    //更新用户资料
    @PutMapping("/users")
    public void updateUserInfo(@RequestBody UserInfo userInfo,
                               @RequestHeader("Authorization") String token){
        userManager.updateUserInfo(userInfo,token);
    }

    //通用设置
    @GetMapping("/users/settings")
    public ResponseEntity settings(){
        return userManager.settings();
    }

    //设置陌生人问题
    @PostMapping("/users/questions")
    public void setQuestion(@RequestBody Map<String,String> map){
        userManager.setQuestion(map);
    }

    //设置通知
    @PostMapping("/users/notifications/setting")
    public void setNotifi(@RequestBody Notification notification){
        userManager.setNotifi(notification);
    }

    //黑名单-分页查询
    @GetMapping("/users/blacklist")
    public ResponseEntity getBlackList(
            @RequestParam(name = "page" , defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pagesize" , defaultValue = "5") Integer pageSize){
        return userManager.getBlackList(pageNum,pageSize);
    }

    //黑名单-移除用户
    @DeleteMapping("/users/blacklist/{uid}")
    public void deleteOne(
            @PathVariable(name = "uid") Long blackUId){
        userManager.deleteOne(blackUId);
    }




    // 修改手机号- 1 发送短信验证码
    @PostMapping("/users/phone/sendVerificationCode")
    public void sendVerificationCode(){
        // 调用manager
        userManager.sendVerificationCode();
    }


    // 修改手机号 - 2 校验验证码
    @PostMapping("/users/phone/checkVerificationCode")
    public ResponseEntity checkVerificationCode(@RequestBody Map<String,String> map){
        // 1.获取验证码
        String yzm = map.get("verificationCode");
        // 2.调用manager
        return userManager.checkVerificationCode(yzm);
    }


    // 修改手机号 - 3 保存手机号
    @PostMapping("/users/phone")
    public void updatePhone(@RequestBody Map<String,String> map){
        // 获取手机号
        String phone = map.get("phone");
        // 调用manager
        userManager.updatePhone(phone);
    }

}
