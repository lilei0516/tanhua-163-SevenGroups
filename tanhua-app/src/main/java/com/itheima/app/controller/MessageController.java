package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.MessageManager;
import com.itheima.vo.HuanXinVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class MessageController {

    @Autowired
    private MessageManager messageManager;

    // 获取app端的环信账号
    @GetMapping("/huanxin/user")
    public ResponseEntity huanxinUser() {

        // 创建环信vo
        HuanXinVo huanXinVo = new HuanXinVo();
        huanXinVo.setUsername("HX" + UserHolder.get().getId());
        huanXinVo.setPassword("123456");

        return ResponseEntity.ok(huanXinVo);
    }

    // 聊一下（好友申请）
    @PostMapping("/tanhua/strangerQuestions")
    public void strangerQuestions(@RequestBody Map<String,String> map){
        // 获取回复和用户id（佳人id）
        Long jiarenId = Long.parseLong(map.get("userId"));
        String reply = (String) map.get("reply");
        messageManager.strangerQuestions(reply,jiarenId);
    }

    // 加好友
    @PostMapping("/messages/contacts")
    public void addContacts(@RequestBody Map<String,Long> map){
        Long friendId = map.get("userId");
        messageManager.addContacts(friendId);
    }

    // 查看联系人列表
    @GetMapping("/messages/contacts")
    public ResponseEntity getContacts(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return messageManager.getContacts(pageNum,pageSize);
    }

    // 查看喜欢
    @GetMapping("/messages/likes")
    public ResponseEntity findLikes(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return messageManager.findCommentVoByPage(pageNum,pageSize,1);
    }

    // 查看评论
    @GetMapping("/messages/comments")
    public ResponseEntity findComments(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return messageManager.findCommentVoByPage(pageNum,pageSize,2);
    }

    // 查看喜欢
    @GetMapping("/messages/loves")
    public ResponseEntity findLoves(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return messageManager.findCommentVoByPage(pageNum,pageSize,3);
    }

    // 查看公告
    @GetMapping("/messages/announcements")
    public ResponseEntity findAnnouncements(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return messageManager.findAnnouncements(pageNum,pageSize);
    }

    // 根据环信id查询用户信息
    @GetMapping("/messages/userinfo")
    public ResponseEntity findUserInfoByHuanXinId(String huanxinId){
        // 调用manager查询
        return messageManager.findUserInfoByHuanXinId(huanxinId);
    }
}