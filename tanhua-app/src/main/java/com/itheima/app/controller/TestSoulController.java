package com.itheima.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.itheima.app.manager.TestSoulManager;
import com.itheima.domain.db.Answers;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TestSoulController {

    @Autowired
    private TestSoulManager testSoulManager;

    // 问卷列表查询
    @GetMapping("/testSoul")
    public ResponseEntity getQuestionnaire(){
        return testSoulManager.getQuestionnaire();
    }

    // 提交问卷
    @PostMapping("/testSoul")
    public ResponseEntity saveQuestionnaire(@RequestBody Map<String,List<Answers>> answers){
        // 题目-选项提交答案集合
        List<Answers> answersList = answers.get("answers");
        return testSoulManager.saveQuestionnaire(answersList);
    }

    // 查看报告
    @GetMapping("/testSoul/report/{id}")
    public ResponseEntity getReport(@PathVariable("id") String id){
        // 拿到报告id
        ObjectId reportId = new ObjectId(id);
        return testSoulManager.getReport(reportId);
    }
}
