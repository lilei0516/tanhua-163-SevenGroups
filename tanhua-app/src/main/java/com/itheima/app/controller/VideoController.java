package com.itheima.app.controller;

import com.itheima.app.interceptor.UserHolder;
import com.itheima.app.manager.VideoManager;
import com.itheima.vo.PageBeanVo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
public class VideoController {

    @Autowired
    private VideoManager videoManager;

    // 获取视频列表
    @GetMapping("/smallVideos")
    public ResponseEntity getSmallVideos(
            @RequestParam(name = "page", defaultValue = "1", required = false) Integer pageNum,
            @RequestParam(name = "pagesize", defaultValue = "10", required = false) Integer pageSize) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        PageBeanVo pageBeanVo = videoManager.getSmallVideos(pageNum, pageSize, userId);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 上传视频
    @PostMapping("/smallVideos")
    public void saveSmallVideos(MultipartFile videoThumbnail, MultipartFile videoFile) throws IOException {
        // 获取当前用户id
        Long userId = UserHolder.get().getId();
        videoManager.saveSmallVideos(videoThumbnail, videoFile, userId);
    }

    // 关注用户
    @PostMapping("/smallVideos/{uid}/userFocus")
    public void userFocus(@PathVariable("uid") Long focusUserId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        videoManager.userFocus(userId, focusUserId);
    }

    // 取关用户
    @PostMapping("/smallVideos/{uid}/userUnFocus")
    public void userUnFocus(@PathVariable("uid") Long focusUserId) {
        // 获取当前userid
        Long userId = UserHolder.get().getId();
        videoManager.userUnFocus(userId, focusUserId);
    }

    //小视频点赞
    @PostMapping("/smallVideos/{videoId}/like")
    public ResponseEntity setMovementLike(@PathVariable String videoId) {
        Long userId = UserHolder.get().getId();
        return videoManager.setVideoLike(videoId,userId);
    }

    //小视频取消点赞
    @PostMapping("/smallVideos/{videoId}/dislike")
    public ResponseEntity removeMovementLike(@PathVariable String videoId) {
        Long userId = UserHolder.get().getId();
        return videoManager.removeVideoLike(videoId,userId);
    }

    //查看视频评论列表
    @GetMapping("/smallVideos/{id}/comments")
    public ResponseEntity findCommentVo(
            @PathVariable("id") String publishId,
            @RequestParam(value = "page", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize", defaultValue = "10") Integer pageSize) {
        return videoManager.findCommentVo(publishId, pageNum, pageSize);

    }

    //给小视频评论
    @PostMapping("/smallVideos/{id}/comments")
    public void setMovementComment(
            @RequestBody Map<String, String> param,
            @PathVariable("id") String publishId) {
        String content = param.get("comment");
        // 调用manager
        videoManager.setMovementComment(publishId, content);

    }
    // 对视频评论进行点赞
    @PostMapping("/smallVideos/comments/{commentId}/like")
    public ResponseEntity commentLike(@PathVariable("commentId") String commentId){
        ObjectId objectId = new ObjectId(commentId);
        return videoManager.commentLike(objectId);
    }

    // 对视频评论取消点赞
    @PostMapping("/smallVideos/comments/{commentId}/dislike")
    public ResponseEntity commentDisLike(@PathVariable("commentId") String commentId){
        ObjectId objectId = new ObjectId(commentId);
        return videoManager.commentDisLike(objectId);
    }
}

