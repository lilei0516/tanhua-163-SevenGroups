package com.itheima.app.controller;

import com.itheima.app.manager.MovementManager;
import com.itheima.domain.mongo.Movement;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
public class MovementController {

    @Autowired
    private MovementManager movementManager;

    // 发布动态（保存动态）
    // 多组件提交，自动封装文本数据和文件数据
    // @PostMapping("/movements")
    // public void saveMovement(Movement movement, MultipartFile[] imageContent) throws IOException {
    //     movementManager.saveMovement(movement,imageContent);
    // }

    //查看自己的动态
    @GetMapping("/movements/all")
    public ResponseEntity getMyMovements(
            @RequestParam(name = "page",defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "5") Integer pageSize,
            Long userId){
        return movementManager.getMyMovements(pageNum,pageSize,userId);
    }

    //查看好友的动态
    @GetMapping("/movements")
    public ResponseEntity getFriendMovements(
            @RequestParam(name = "page",defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "5") Integer pageSize){
        return movementManager.getFriendMovements(pageNum,pageSize);
    }

    //查看推荐的动态
    @GetMapping("/movements/recommend")
    public ResponseEntity getRecommendMovements(
            @RequestParam(name = "page",defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "5") Integer pageSize){
        return movementManager.getRecommendMovements(pageNum,pageSize);
    }

    //第二遍-发布（保存）动态
    @PostMapping("/movements")
    public ResponseEntity saveMovement(Movement movement,MultipartFile[] imageContent) throws IOException {
        return movementManager.saveMovement(movement,imageContent);
    }

    // 动态点赞
    @GetMapping("/movements/{id}/like")
    public ResponseEntity saveCommentLike(@PathVariable("id") String publishId){
        return movementManager.saveCommentLike(publishId);
    }

    // 动态取消点赞
    @GetMapping("/movements/{id}/dislike")
    public ResponseEntity removeCommentLike(@PathVariable("id") String publishId){
        return movementManager.removeCommentLike(publishId);
    }

    // 动态喜欢
    @GetMapping("/movements/{id}/love")
    public ResponseEntity saveCommentLove(@PathVariable("id") String publishId){
        return movementManager.saveCommentLove(publishId);
    }

    // 动态取消喜欢
    @GetMapping("/movements/{id}/unlove")
    public ResponseEntity removeCommentLove(@PathVariable("id") String publishId){
        return movementManager.removeCommentLove(publishId);
    }

    // 单条动态查询
    @GetMapping("/movements/{id}")
    public ResponseEntity findMovement(@PathVariable("id") String publishId){
        return movementManager.findMovement(publishId);
    }

    // 评论列表查询
    @GetMapping("/comments")
    public ResponseEntity findCommentsList(
            @RequestParam("movementId") String publishId,
            @RequestParam(name = "page",defaultValue = "1") Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "5") Integer pageSize

    ){
        return movementManager.findCommentsList(publishId,pageNum,pageSize);
    }

    // 发表评论
    @PostMapping("/comments")
    public ResponseEntity saveComment(@RequestBody Map<String,String> map){
        // 拿到动态id
        String publishId = map.get("movementId");
        // 拿到评论内容
        String content = map.get("comment");
        return movementManager.saveComment(publishId,content);
    }

    // 对动态评论进行点赞
    @GetMapping("/comments/{commentId}/like")
    public ResponseEntity commentLike(@PathVariable("commentId") String commentId){
        ObjectId objectId = new ObjectId(commentId);
        return movementManager.commentLike(objectId);
    }

    // 对评论取消点赞
    @GetMapping("/comments/{commentId}/dislike")
    public ResponseEntity commentDisLike(@PathVariable("commentId") String commentId){
        ObjectId objectId = new ObjectId(commentId);
        return movementManager.commentDisLike(objectId);
    }

}
