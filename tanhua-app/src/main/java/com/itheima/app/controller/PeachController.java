package com.itheima.app.controller;

import com.itheima.app.manager.PeachManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class PeachController {

    @Autowired
    private PeachManager peachManager;

    // 接收语音
    @GetMapping("/peachblossom")
    public ResponseEntity getPeachblossom(){
        return peachManager.getPeachblossom();
    }

    // 发送语音
    @PostMapping("/peachblossom")
    public void savePeachblossom(MultipartFile soundFile) throws IOException {
        peachManager.savePeachblossom(soundFile);
    }
}
