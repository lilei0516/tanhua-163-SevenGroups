package com.itheima.app.controller;

import com.itheima.app.manager.CardsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

// app端探花卡片功能
@RestController
public class CardsController {


    @Autowired
    private CardsManager cardsManager;


    // app端探花卡片查询展示
    @GetMapping("/tanhua/cards")
    public ResponseEntity findCards(){
        // 调用manager
        return cardsManager.findCards();
    }


    // app端探花卡片右滑喜欢
    @GetMapping("/tanhua/{likeUserId}/love")
    public void findCardsLove(@PathVariable Long likeUserId){
        System.out.println(likeUserId);
        // 调用manager
        cardsManager.findCardsLove(likeUserId);
    }


    // app端探花卡片左滑不喜欢
    @GetMapping("/tanhua/{noLikeUserId}/unlove")
    public void findCardsNoLove(@PathVariable Long noLikeUserId){
        // 调用manager
        cardsManager.findCardsNoLove(noLikeUserId);
    }
}
