package com.itheima.app.controller;

import com.itheima.app.manager.MakeFriendManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class MakeFriendController {

    @Autowired
    private MakeFriendManager makeFriendManager;

    // 今日佳人推荐
    @GetMapping("/tanhua/todayBest")
    public ResponseEntity getTodayBest(){
        return makeFriendManager.getTodayBest();
    }

    // 推荐朋友
    @GetMapping("/tanhua/recommendation")
    public ResponseEntity getRecommendationList(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return makeFriendManager.getRecommendationList(pageNum,pageSize);
    }

    // 推荐用户详情
    @GetMapping("/tanhua/{recommendUserId}/personalInfo")
    public ResponseEntity getRecommendUserDetail(@PathVariable("recommendUserId")Integer recommendUserId){
        return makeFriendManager.getRecommendUserDetail(recommendUserId);
    }

    // 是否喜欢他（她）:红心高亮
    @GetMapping("/users/{likeUserId}/alreadyLove")
    public ResponseEntity findAlreadyLove(@PathVariable Long likeUserId){
        // 1.调用manager
        return makeFriendManager.findAlreadyLove(likeUserId);

    }

    // 最近访客
    @GetMapping("/movements/visitors")
    public ResponseEntity getVisitors(){
        return makeFriendManager.getVisitors();
    }

    // 附近的人（上传地址）
    @PostMapping("/baidu/location")
    public void saveNearUserLocation(@RequestBody Map<String,String> map){
        String latitude = map.get("latitude");
        String longitude = map.get("longitude");
        String addrStr = map.get("addrStr");
        makeFriendManager.saveNearUserLocation(Double.parseDouble(latitude),
                Double.parseDouble(longitude),addrStr);
    }

    // 搜附近的人
    @GetMapping("/tanhua/search")
    public ResponseEntity serarchNearUser(String gender,Long distance){
        return makeFriendManager.serarchNearUser(gender,distance);
    }

    // 聊一下功能
    @GetMapping("/tanhua/strangerQuestions")
    public ResponseEntity strangerQuestions(Long userId){
        return makeFriendManager.strangerQuestions(userId);
    }




}
