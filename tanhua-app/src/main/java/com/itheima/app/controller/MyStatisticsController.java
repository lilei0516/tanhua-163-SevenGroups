package com.itheima.app.controller;

import com.itheima.app.manager.MyStatisticsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyStatisticsController {

    @Autowired
    private MyStatisticsManager myStatisticsManager;

    // 互相喜欢，喜欢，粉丝 - 统计 数量
    @GetMapping("/users/counts")
    public ResponseEntity findCounts(){
        // 调用manager
        return myStatisticsManager.findCounts();
    }


    // 互相喜欢、喜欢、粉丝、谁看过我 - 查看详情
    @GetMapping("/users/friends/{type}")
    public ResponseEntity findFriends(@RequestParam(value = "page",defaultValue = "1")Integer pageNum,
                                      @RequestParam(value = "pagesize",defaultValue = "10")Integer pageSize,
                                      @PathVariable("type") Integer type){
        return myStatisticsManager.findFriends(pageNum,pageSize,type);

    }


    @Autowired
    CardsController cardsController;

    // 粉丝 - 喜欢
    @PostMapping("/users/fans/{uid}")
    public void likeFans(@PathVariable("uid")Long likeUserId){
        // 调用卡片功能的右滑喜欢，这里面可以实现加好友加动态和加环信好友，同时删除推荐表数据和在喜欢表中加入你喜欢对方
        // 由于对方要的是likeUserId所以你也必须要传个这个名字，实际上是你粉丝的id
        cardsController.findCardsLove(likeUserId);
    }


    // 粉丝 - 取消喜欢
    @DeleteMapping("/users/like/{uid}")
    public void noLikeFans(@PathVariable("uid")Long noLikeUserId){
        // 调用manager
        myStatisticsManager.noLikeFans(noLikeUserId);
    }




}
