package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

// 描述表
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulConclusion implements Serializable {
    private Long id; // 描述主键id
    private String conclusion; // 描述内容
    private String cover; // 图片连接
    private Integer minScore; // 最小分数
    private Integer maxScore; // 最大分数

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
