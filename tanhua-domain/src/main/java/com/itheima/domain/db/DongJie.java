package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Description:     冻结实体类
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-12 15:34
 * @version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DongJie implements Serializable {

    private Long id;
    private Long userId; // 用户id
    //private Integer userStatus;  // 是否被冻结   1代表正常，2代表被冻结
    private Integer freezingTime;  // 冻结时间  1为冻结3天，2为冻结7天，3为永久冻结
    private Integer freezingRange; // 冻结范围  1为冻结登录，2为冻结发言，3为冻结发布动态
    private String  frozenRemarks;   // 冻结备注
    private String reasonsForFreezing;    // 冻结原因
    private Integer sx;  // 是否失效，0是失效，1是不失效

    // 解冻原因
    //private String reasonsForThawing;  // 解冻原因
}
