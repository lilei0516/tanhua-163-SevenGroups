package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

// 选项表
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulOptions implements Serializable {
    private Long id; // 选项主键id
    private Long examinationId;// 题目id
    private String options; // 选项内容
    private Integer score; // 即设分数

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
