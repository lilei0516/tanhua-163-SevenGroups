package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

//用户类
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private Long id;//用户id
    private String phone; //手机号
    private String password; //密码

    /*// 后续添加字段
    private Integer userStatus;  // 是否被冻结   1代表正常，2代表被冻结
    private Integer freezingTime;  // 冻结时间  1为冻结3天，2为冻结7天，3为永久冻结
    private Integer freezingRange; // 冻结范围  1为冻结登录，2为冻结发言，3为冻结发布动态
    private String frozenRemarks;   // 冻结备注*/

    @TableField(fill = FieldFill.INSERT) //插入时自动填充
    private Date created;//创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)//插入和更新时自动更新
    private Date updated;//更新时间

    //环信用户信息
    @TableField(exist=false)
    private String hxUser;
    @TableField(exist=false)
    private String hxPassword;
}