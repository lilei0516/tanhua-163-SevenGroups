package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-13 11:09
 * @version:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Jd implements Serializable {

    private Long id;
    private Long userId; // 用户id
    private String reasonsForThawing;  // 解冻原因
}
