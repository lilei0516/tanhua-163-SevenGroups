package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

// 问卷表
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestionnaire implements Serializable {
    private Long id; // 问卷主键id
    private String name; // 问卷名称
    private String cover; // 封面图片
    private String level; // 问卷级别
    private Integer star; // 问卷星别

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
