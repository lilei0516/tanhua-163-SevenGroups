package com.itheima.domain.db;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

//用户信息(跟用户一对一)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo implements Serializable {

    //设置主键类型为用户输入
    @TableId(type = IdType.INPUT)
    private Long id; //用户id
    private String nickname; //昵称
    private String avatar; //用户头像
    private String birthday; //生日
    private String gender; //性别
    private String city; //城市
    private String income; //收入
    private String education; //学历
    private String profession; //行业
    private Integer marriage; //婚姻状态
    private String tags; //用户标签：多个用逗号分隔
    private String coverPic; // 封面图片
    private Integer age; //年龄
    //
    private String userStatus; // 用户状态
    /*// 后续添加字段
    private Integer userStatus;  // 是否被冻结   1代表正常，2代表被冻结
    private Integer freezingTime;  // 冻结时间  1为冻结3天，2为冻结7天，3为永久冻结
    private Integer freezingRange; // 冻结范围  1为冻结登录，2为冻结发言，3为冻结发布动态
    private String frozenRemarks;   // 冻结备注*/

    @TableField(fill = FieldFill.INSERT)
    private Date created;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;

}