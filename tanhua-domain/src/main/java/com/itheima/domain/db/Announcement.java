package com.itheima.domain.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

// 公告
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Announcement implements Serializable {

    // 前端需要
    private Long id;
    private String title;
    private String description;
    private Date created;
    private Date updated;

}
