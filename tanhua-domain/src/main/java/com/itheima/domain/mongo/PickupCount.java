package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

// 计数表
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "pickup_count")
public class PickupCount implements Serializable {
    private ObjectId id; // 主键id
    private Long userId; // 用户id
    private Integer count; // 拾取次数
    private Long created; //发布时间
}
