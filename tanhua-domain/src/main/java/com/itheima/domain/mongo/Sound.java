package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

// 数据库用户报告表
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "sound")
public class Sound implements Serializable {
    private ObjectId id; // 主键id
    private Long userId; // 用户id
    private String soundUrl; // 语音存放地址
    private Integer pickup;// 拾取标识 0-未被拾取 1-已被拾取
    private Long created; //发布时间
}
