package com.itheima.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

// 数据库用户报告表
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "soul_report")
public class SoulReport implements Serializable {
    private ObjectId id; // 报告id
    private Integer score;// 用户总分score
    private Long userId;// 用户userid
    private Long conclusionId;// 用户描述id（用来推荐相似）
    private Long questionnaireId;// 问卷编号id
    private Long created; //发布时间
}
