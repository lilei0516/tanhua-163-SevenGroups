package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 返回给前端的问卷实体
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulQuestionnaireVo {
    private String id; // 问卷主键id
    private String name; // 问卷名称
    private String cover; // 封面图片
    private String level; // 问卷级别
    private Integer star; // 问卷星别
    private List<SoulExaminationVo> questions; // 试题Vo集合，试题vo装着试题和选项
    private Integer isLock = 1; // 是否锁住 0-解锁 1-锁住 默认锁住
    private String reportId; // 报告id 报告id如果有，那么islock变成0
}
