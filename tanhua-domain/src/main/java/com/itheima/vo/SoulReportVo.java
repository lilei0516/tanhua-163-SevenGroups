package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 前端需要的报告表
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulReportVo {

    private String conclusion; // 鉴定结果
    private String cover; // 鉴定图片
    private List<DimensionsVo> dimensions; // 维度
    private List<SimilarYouVo> similarYou; // 与你相似
}
