package com.itheima.vo;

import cn.hutool.core.bean.BeanUtil;
import com.itheima.domain.db.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

// 封装列表详情的vo
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyStatisticsDetailsVo implements Serializable {

    private Long id;
    private String avatar;  // 头像
    private String nickname;    // 昵称
    private String gender;  // 性别
    private Integer age;    // 年龄
    private String city;    // 城市
    private String education;   // 学历
    private Integer marriage;   // 婚姻状态

    private Integer matchRate;  // 匹配度
    private Boolean alreadyLove;    // 是否喜欢他

    // 设置用户信息
    public void setUserInfo(UserInfo userInfo) {
        if (userInfo != null) {
            BeanUtil.copyProperties(userInfo, this);
        }
    }

}
