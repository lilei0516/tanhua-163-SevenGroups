package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Rwh on 2021/11/11 21:27
 */
//前端需要的实体类
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataVo implements Serializable {

    private String title;//日期
    private Long amount;//数量

}
