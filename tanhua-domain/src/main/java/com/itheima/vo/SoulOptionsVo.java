package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 返回给前端的选项实体
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulOptionsVo {
    private String id; // 试题主键id
    private String option; // 题目
}
