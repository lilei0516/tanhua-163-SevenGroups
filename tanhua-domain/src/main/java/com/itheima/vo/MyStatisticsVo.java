package com.itheima.vo;

import cn.hutool.core.bean.BeanUtil;
import com.itheima.domain.db.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyStatisticsVo implements Serializable {

    // 互相喜欢
    private Integer eachLoveCount;
    // 我喜欢的
    private Integer loveCount;
    // 喜欢我的
    private Integer fanCount;


}
