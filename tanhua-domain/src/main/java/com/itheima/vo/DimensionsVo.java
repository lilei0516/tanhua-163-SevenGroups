package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 维度
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DimensionsVo {
    private String key; // 维度项
    private String value; // 维度值
}
