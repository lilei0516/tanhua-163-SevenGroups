package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 与你相似
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SimilarYouVo {
    private Integer id; // 用户id
    private String avatar; // 用户头像
}
