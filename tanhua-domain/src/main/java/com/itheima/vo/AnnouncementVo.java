package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementVo implements Serializable {
    private String id;
    private String title;
    private String description;
    private String createDate;
}
