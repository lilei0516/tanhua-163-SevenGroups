package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author wzh
 * @Date 2021/11/12 9:15
 * @Version 1.0
 */
//用户地理位置及用户信息
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLocationWithInfoVo implements Serializable {
    private String id;
    private Long userId;//用户id
    private String address;//位置描述
    private Double longitude;//经度
    private Double latitude;//纬度
    private String city;//城市
    private String nickname;//用户昵称
    private String avatar;//用户头像
    private String gender;//用户性别
}
