package com.itheima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 返回给前端的题目实体
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoulExaminationVo {
    private String id; // 试题主键id
    private String question; // 题目
    private List<SoulOptionsVo> options;// 选项集合，装着对应的选项
}
