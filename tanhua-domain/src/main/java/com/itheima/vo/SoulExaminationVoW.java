package com.itheima.vo;

import com.itheima.domain.db.SoulOptions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author wzh
 * @Date 2021/11/12 15:55
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SoulExaminationVoW implements Serializable {
    private Long id;//题目id
    private String question;//问题题目
    private String level;//问题等级
    private List<SoulOptions> optionList;//题目对象集合
}
