package com.itheima.controller;

import com.itheima.domain.db.DongJie;
import com.itheima.manager.DongjieManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-11 16:30
 * @version:
 */
@RestController
public class DongjieController {

    @Autowired
    private DongjieManager dongjieManager;

    // 冻结用户
    @PostMapping("/manage/users/freeze")
    public ResponseEntity freeze(@RequestBody DongJie dongJie){
        dongjieManager.freeze(dongJie);
        return ResponseEntity.ok("ok");
    }

    // 解冻用户
    @PostMapping("/manage/users/unfreeze")
    public ResponseEntity unFreeze( @RequestBody Map<String,Object> map){
        Long userId = ((Integer)map.get("userId")).longValue();
        String reasonsForThawing = (String) map.get("frozenRemarks");
        dongjieManager.unFreeze(userId,reasonsForThawing);
        return ResponseEntity.ok("ok");
    }
}
