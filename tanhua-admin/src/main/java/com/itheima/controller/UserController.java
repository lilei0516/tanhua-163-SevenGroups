package com.itheima.controller;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.date.DateUtil;
import com.itheima.domain.db.Admin;
import com.itheima.interceptor.AdminHolder;
import com.itheima.manager.UserManager;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserManager userManager;

    // 生成验证码图片并且返回
    @GetMapping("/system/users/verification")
    public void getVerification(String uuid, HttpServletResponse response) throws IOException {
        // 调用manager返回一个验证码对象
        LineCaptcha lineCaptcha = userManager.getVerification(uuid);
        // 返回给客户端
        lineCaptcha.write(response.getOutputStream());
    }

    // 登录校验
    @PostMapping("/system/users/login")
    public ResponseEntity login(@RequestBody Map<String,String> map){
        // 将四个值获取到
        String username = map.get("username");
        String password = map.get("password");
        String verificationCode = map.get("verificationCode");
        String uuid = map.get("uuid");
        return userManager.login(username,password,verificationCode,uuid);
    }

    // 管理员基本信息
    @PostMapping("/system/users/profile")
    public ResponseEntity getProfile(){
        Admin admin = AdminHolder.get();
        return ResponseEntity.ok(admin);
    }

    // 用户登出
    @PostMapping("/system/users/logout")
    public void logout(@RequestHeader("Authorization")String token){
        userManager.logout(token);
    }

    // 用户管理，用户列表
    @GetMapping("/manage/users")
    public ResponseEntity getUsers(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize){
        return userManager.getUsers(pageNum,pageSize);
    }

    // 用户详情
    @GetMapping("/manage/users/{userID}")
    public ResponseEntity getUserInfo(@PathVariable("userID") Long userId){
        return userManager.getUserInfo(userId);
    }

    // 动态记录（动态审核）
    @GetMapping("/manage/messages")
    public ResponseEntity getMessages(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize,
            @RequestParam(name = "uid",required = false) Object uid,
            @RequestParam(name = "state",required = false) Object state){
        Long uid1 = null;
        if (uid != null) {
            if(uid instanceof String){
                uid1 = Long.parseLong((String)uid);
            }else{
                uid1=(long) uid;
            }
        }
        Integer state1 = null;
        if ("1".equals(state)||"2".equals(state)||"0".equals(state)) {
            state1 = Integer.parseInt((String)state);
        }
        return userManager.getMessages(pageNum,pageSize,uid1,state1);
    }

    // 动态详情
    @GetMapping("/manage/messages/{publishId}")
    public ResponseEntity findMovementDetail(@PathVariable("publishId") String publishId){
        return userManager.findMovementDetail(publishId);
    }

    // 动态的评论列表
    @GetMapping("/manage/messages/comments")
    public ResponseEntity findComments(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize,
            @RequestParam(name = "messageID") String messageId){
        return userManager.findComments(pageNum,pageSize,messageId);
    }

    // 视频列表
    @GetMapping("/manage/videos")
    public ResponseEntity findVideos(
            @RequestParam(name = "page",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(name = "pagesize",defaultValue = "10",required = false)Integer pageSize,
            Long uid){
        return userManager.findVideos(pageNum,pageSize,uid);
    }

    // 概要统计
    @GetMapping("/dashboard/summary")
    public ResponseEntity summary(){
        return userManager.summary();
    }

    //数据对比图
    @GetMapping("/dashboard/users")
    public ResponseEntity contrastData(
            @RequestParam("sd") Long param1,//今年开始时间
            @RequestParam("ed") Long param2,//今年结束时间
            Integer type){
        //参数类型转换
        String startTime = DateUtil.formatDate(new Date(param1));//开始时间,把long变成string--2021-11-04
        String endTime = DateUtil.formatDate(new Date(param2));//结束时间,把long变成string--2021-11-11
        //调用manager
        return userManager.contrastData(startTime,endTime,type);
    }


    // 动态复审
    @PostMapping("/manage/messages/pass")
    public ResponseEntity repeatExamine(@RequestBody String[] ids){
        // 调用manager
        return userManager.repeatExamine(ids);
    }
}
