package com.itheima.manager;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.domain.db.Admin;
import com.itheima.domain.db.UserInfo;
import com.itheima.domain.mongo.Comment;
import com.itheima.domain.mongo.Movement;
import com.itheima.domain.mongo.Video;
import com.itheima.exception.BusinessException;
import com.itheima.service.db.AdminService;
import com.itheima.service.db.AnalysisByDayService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.CommentService;
import com.itheima.service.mongo.MovementService;
import com.itheima.service.mongo.VideoService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.DateFormatUtil;
import com.itheima.util.JwtUtil;
import com.itheima.vo.*;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;

@Component
public class UserManager {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @DubboReference
    private AdminService adminService;

    @DubboReference
    private UserInfoService userInfoService;

    @DubboReference
    private MovementService movementService;

    @DubboReference
    private CommentService commentService;

    @DubboReference
    private VideoService videoService;

    @DubboReference
    private AnalysisByDayService analysisByDayService;

    // 生成一个验证码图片返回
    public LineCaptcha getVerification(String uuid) {
        LineCaptcha lineCaptcha = new LineCaptcha(200, 100);
        // 存到redis
        stringRedisTemplate.opsForValue().set(uuid,lineCaptcha.getCode(), Duration.ofMinutes(3));
        // 返回给上一层
        return lineCaptcha;
    }

    // 登录校验
    public ResponseEntity login(String username, String password, String verificationCode, String uuid) {
        // 校验验证码
        String codeForRedis = stringRedisTemplate.opsForValue().get(uuid);
        // 拦截一；验证码不匹配
        if (!StrUtil.equals(verificationCode,codeForRedis)) {
            throw new BusinessException("验证码错误！");
        }
        // 拦截二；校验用户名
        Admin admin = adminService.getAdminByUsername(username);
        if (admin == null) {
            throw new BusinessException("用户名不存在！");
        }
        // 拦截三；校验密码
        String pwdMd5ForDB = admin.getPassword();
        String pwdMd5 = SecureUtil.md5(password);
        if (!StrUtil.equals(pwdMd5,pwdMd5ForDB)) {
            throw new BusinessException("密码错误！");
        }
        // 登录成功，删除验证码，生成token
        stringRedisTemplate.delete(uuid);
        // 清除敏感数据
        admin.setPassword(null);
        Map<String, Object> claims = BeanUtil.beanToMap(admin);
        String token = JwtUtil.createToken(claims);
        // 存入到redis当中，以token为key，用户信息为value
        String adminJson = JSON.toJSONString(admin);
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN+token,adminJson,Duration.ofHours(1));
        // 返回给前端
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        return ResponseEntity.ok(map);
    }

    // 管理员信息查询
    public Admin getProfile(String token) {
        // 通过token获取userid
        token = token.replaceAll("Bearer ", "");
        String adminJson = stringRedisTemplate.opsForValue().get(ConstantUtil.ADMIN_TOKEN + token);
        // 判空
        if (adminJson == null) {
            return null;
        }
        Admin admin = JSON.parseObject(adminJson, Admin.class);
        // 信息续期
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN+token,adminJson,Duration.ofHours(1));
        return admin;
    }

    // 管理员登出
    public void logout(String token) {
        // 需要处理
        token = token.replace("Bearer ","");
        // 删除redis中的token
        stringRedisTemplate.delete(ConstantUtil.ADMIN_TOKEN+token);
    }

    // 拿到用户列表
    public ResponseEntity getUsers(Integer pageNum, Integer pageSize) {
        // 拿到pbv
        PageBeanVo pageBeanVo = userInfoService.findUserByPage(pageNum,pageSize);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查看用户详情
    public ResponseEntity getUserInfo(Long userId) {
        UserInfo userInfo = userInfoService.findUserInfoById(userId);
        return ResponseEntity.ok(userInfo);
    }

    // 查看某个用户动态（视频审核公用一套接口）
    public ResponseEntity getMessages(Integer pageNum, Integer pageSize, Long uid, Integer state) {
        // 查询动态详情表
        PageBeanVo pageBeanVo = movementService.findMovementByPage(pageNum,pageSize,uid,state);
        // 获取装着movement的items
        List<Movement> movementList  = (List<Movement>) pageBeanVo.getItems();
        // 容器
        List<MovementVo> movementVoList = new ArrayList<>();
        // 判空遍历
        if (CollectionUtil.isNotEmpty(movementList)) {
            for (Movement movement : movementList) {
                MovementVo movementVo = new MovementVo();
                // 查询userinfo
                UserInfo userInfo = userInfoService.findUserInfoById(movement.getUserId());
                // 封装数据
                movementVo.setUserInfo(userInfo);
                movementVo.setMovement(movement);
                // 格式化时间
                movementVo.setCreateDate(DateUtil.formatDateTime(new Date(movement.getCreated())));
                // 添加到集合
                movementVoList.add(movementVo);
            }
        }
        // 重新设置pbv
        pageBeanVo.setItems(movementVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查询指定动态的详情
    public ResponseEntity findMovementDetail(String publishId) {
        ObjectId objectId = new ObjectId(publishId);
        Movement movement = movementService.findMovementById(objectId);
        // 封装vo
        MovementVo movementVo = new MovementVo();
        // 查询userinfo
        UserInfo userInfo = userInfoService.findUserInfoById(movement.getUserId());
        // 封装数据
        movementVo.setUserInfo(userInfo);
        movementVo.setMovement(movement);
        // 格式化时间
        movementVo.setCreateDate(DateUtil.formatDateTime(new Date(movement.getCreated())));
        // 添加到集合
        return ResponseEntity.ok(movementVo);
    }

    // 查看指定动态下的评论列表
    public ResponseEntity findComments(Integer pageNum, Integer pageSize, String messageId) {
        ObjectId objectId = new ObjectId(messageId);
        PageBeanVo pageBeanVo = commentService.findCommentsListByPage(pageNum, pageSize, objectId, 2);
        // 拿到评论内容
        List<Comment> comments = (List<Comment>) pageBeanVo.getItems();
        // 装载commentvo的容器
        List<CommentVo> commentVoList = new ArrayList<>();
        // 判空遍历这个评论集合
        if (CollectionUtil.isNotEmpty(comments)) {
            // 属性封装
            for (Comment comment : comments) {
                // 封装CommentVo
                CommentVo commentVo = new CommentVo();
                // 封装id
                commentVo.setId(comment.getId().toHexString());
                // 封装时间（每个评论者发评论的时间）
                commentVo.setCreateDate(DateFormatUtil.format(new Date(comment.getCreated())));
                // 封装评论
                commentVo.setContent(comment.getContent());
                // 查询每个评论者的详细信息
                UserInfo userInfoById = userInfoService.findUserInfoById(comment.getUserId());
                // 封装头像
                commentVo.setAvatar(userInfoById.getAvatar());
                // 封装昵称
                commentVo.setNickname(userInfoById.getNickname());
                // 向集合中添加
                commentVoList.add(commentVo);
            }
        }
        // 更新pagebeanvo
        pageBeanVo.setItems(commentVoList);
        // 返回前端
        return ResponseEntity.ok(pageBeanVo);
    }

    // 查询某个用户的视频列表
    public ResponseEntity findVideos(Integer pageNum, Integer pageSize, Long uid) {
        PageBeanVo pageBeanVo = videoService.getMySmallVideos(pageNum, pageSize, uid);
        List<Video> videoList = (List<Video>) pageBeanVo.getItems();
        List<VideoVo> videoVoList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(videoList)) {
            for (Video video : videoList) {
                VideoVo videoVo = new VideoVo();
                UserInfo userInfo = userInfoService.findUserInfoById(video.getUserId());
                videoVo.setUserInfo(userInfo);
                videoVo.setVideo(video);
                videoVoList.add(videoVo);
            }
        }
        // 封装
        pageBeanVo.setItems(videoVoList);
        return ResponseEntity.ok(pageBeanVo);
    }

    // 概要统计
    public ResponseEntity summary() {
        // 远程调用概要统计
        AnalysisSummaryVo analysisSummaryVo = analysisByDayService.summary();
        return ResponseEntity.ok(analysisSummaryVo);
    }

    //数据对比图
    public ResponseEntity contrastData(String startTime, String endTime, Integer type) {
        //1.声明返回结果--一个map集合
        Map<String, Object[]> map=new HashMap<>();
        //2.调用rpc--得到map里面的value,今年的数据
        Object[] thisData=analysisByDayService.findData(startTime,endTime,type);
        //得到去年的时间
        Date parse = DateUtil.parse(startTime);
        String lastStartTime = DateUtil.offsetMonth(parse, -12).toDateStr();//去年的开始时间
        Date parse1 = DateUtil.parse(endTime);
        String lastEndTime = DateUtil.offsetMonth(parse1, -12).toDateStr();//去年的结束时间
        //3.调用rpc--得到map里面的value,去年的数据
        Object[] lastData=analysisByDayService.findData(lastStartTime,lastEndTime,type);
        //4.封装map
        map.put("thisYear",thisData);
        map.put("lastYear",lastData);
        //5.返回结果
        return ResponseEntity.ok(map);
    }



    // 动态复审
    public ResponseEntity repeatExamine(String[] ids) {
        // 1.遍历id列表
        if (ids.length>0){
            for (String id : ids) {
                // 根据id查询动态详情
                Movement movement = movementService.findMovementById(new ObjectId(id));
                // 修改动态状态
                movement.setState(1);
                // 修改
                movementService.update(movement);
            }
        }
        return null;
    }
}
