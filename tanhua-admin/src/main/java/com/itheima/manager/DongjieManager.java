package com.itheima.manager;

import com.itheima.domain.db.DongJie;
import com.itheima.domain.db.Jd;
import com.itheima.domain.db.UserInfo;
import com.itheima.exception.BusinessException;
import com.itheima.service.db.DongJieService;
import com.itheima.service.db.JdService;
import com.itheima.service.db.UserInfoService;
import com.itheima.util.ConstantUtil;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-11 16:30
 * @version:
 */
@Component
public class DongjieManager {

    @DubboReference
    private DongJieService dongJieService;

    @DubboReference
    private UserInfoService userInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @DubboReference
    private JdService jdService;

    // 冻结
    public void freeze(DongJie dongJie) {
        // 1.判断用户是否存在
        if (dongJie.getUserId() == null) {
            throw new BusinessException("用户不存在！");
        }
        // 2.判断用户是否被冻结
        UserInfo userInfo = userInfoService.findUserInfoById(dongJie.getUserId());
        if (userInfo.getUserStatus().equals("2")) {
            throw new BusinessException("用户被冻结！");
        }
        // 3.创建一个冻结对象
        DongJie dj = new DongJie();
        dj.setUserId(userInfo.getId());
        dj.setFreezingTime(dongJie.getFreezingTime());  // 设置冻结时间
        dj.setFreezingRange(dongJie.getFreezingRange());  // 设置冻结范围
        dj.setReasonsForFreezing(dongJie.getReasonsForFreezing());  // 设置冻结原因
        dj.setFrozenRemarks(dongJie.getFrozenRemarks());   // 设置冻结备注
        dj.setSx(1);  // 设置是否失效
        // 4.调用service
        dongJieService.dongJie(dj);
        // 5.设置userInfo
        //userInfo.setUserStatus("2");
        // 判断冻结时间
        if (dongJie.getFreezingTime() == 1){
            stringRedisTemplate.opsForValue().set("djsj"+dj.getUserId(),"ok"+dj.getUserId(),Duration.ofHours(3));
        }else if (dongJie.getFreezingTime() == 2){
            stringRedisTemplate.opsForValue().set("djsj"+dj.getUserId(),"ok"+dj.getUserId(),Duration.ofHours(7));
        }
    }

    // 用户解冻
    public void unFreeze(Long userId, String reasonsForThawing) {
        // 1.根据id修改用户状态
        UserInfo userInfo = userInfoService.findUserInfoById(userId);
        userInfo.setUserStatus("1");
        userInfoService.update(userInfo);
        // 2.传入解冻原因
        /*Jd jiedong = new Jd();
        jiedong.setUserId(userId);
        jiedong.setReasonsForThawing(reasonsForThawing);
        jdService.jiedong(jiedong);*/
        // 更新解冻状态
        DongJie jd = new DongJie();
        jd.setUserId(userInfo.getId());
        jd.setSx(0);
        dongJieService.update(jd);
        // 3.删除redis中的数据
        stringRedisTemplate.delete("djsj"+userId);
    }
}
