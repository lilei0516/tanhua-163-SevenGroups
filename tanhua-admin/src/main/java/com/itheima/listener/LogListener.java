package com.itheima.listener;

import com.itheima.autoconfig.lvwang.GreenTemplate;
import com.itheima.domain.db.Log;
import com.itheima.domain.mongo.Movement;
import com.itheima.service.db.LogService;
import com.itheima.service.mongo.MovementService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component  // 放入容器
public class LogListener {

    @DubboReference
    private LogService logService;

    @DubboReference
    private MovementService movementService;

    @Autowired
    private GreenTemplate greenTemplate;

    // 日志监听器
    @RabbitListener(queuesToDeclare = @Queue("tanhua.log"))
    public void listenLog(Log log){
        System.out.println("日志监听；"+log);
        logService.save(log);
    }

    // 动态审核监听器
    @RabbitListener(queuesToDeclare = @Queue("tanhua.movement.state"))
    public void listenMovement(String movementId){
        // 去数据库查询这个动态详情
        ObjectId objectId = new ObjectId(movementId);
        Movement movement = movementService.findMovementById(objectId);
        // 获取文本
        String textContent = movement.getTextContent();
        // 获取图片
        List<String> medias = movement.getMedias();
        // 审核上传1
        Boolean checkText = greenTemplate.checkText(textContent);
        // 审核上传2
        Boolean checkImage = greenTemplate.checkImage(medias);

        if (checkText&&checkImage) {
            // 如果都通过了，设置状态码为1，更新
            movement.setState(1);
        }else {
            // 如果没通过，设置状态码为0，更新
            movement.setState(2);
        }
        movementService.update(movement);
    }

}
