package com.itheima.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

// 全局异常处理器
@ControllerAdvice
public class GlobalExceptionAdvice {

    // 处理我们的业务异常
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity handleBusinessException(Exception e){
        // 向控制台打印异常
        e.printStackTrace();
        // 将异常信息返回给前端
        Map<String,String> erroMsg = new HashMap<>();
        erroMsg.put("message",e.getMessage());
        return ResponseEntity.status(500).body(erroMsg);
    }

    // 处理普通异常
    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e){
        // 向控制台打印异常
        e.printStackTrace();
        // 将异常信息返回给前端
        Map<String,String> erroMsg = new HashMap<>();
        erroMsg.put("message","网络繁忙！");
        return ResponseEntity.status(500).body(erroMsg);
    }
}
