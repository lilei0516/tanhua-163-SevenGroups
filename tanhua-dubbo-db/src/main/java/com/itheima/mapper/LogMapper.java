package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Log;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface LogMapper extends BaseMapper<Log> {
    // 自定义查询

    // 注册或登录
    @Select("SELECT  COUNT(DISTINCT user_id) FROM `tb_log` WHERE `type`=#{tpye} AND `log_time` = #{logtime}")
    String findNumRegisteredOrNumLogin(@Param("tpye") String type,@Param("logtime") String logtime);

    // 活跃人数
    @Select("SELECT  COUNT(DISTINCT user_id) FROM `tb_log` WHERE `log_time` = #{logtime}")
    String findNumActive(@Param("logtime") String logtime);

    // 次日留存用户
    @Select("SELECT  COUNT(DISTINCT user_id) FROM `tb_log` WHERE `log_time` = #{today}\n" +
            "AND `user_id` IN (SELECT  DISTINCT user_id FROM `tb_log` WHERE `type`='0102' AND `log_time` = #{yesterday})")
    String findNumRetention1d(@Param("today") String today,@Param("yesterday") String yesterday);
}