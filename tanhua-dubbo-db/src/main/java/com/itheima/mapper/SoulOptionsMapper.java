package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.SoulOptions;

// 选项详情
public interface SoulOptionsMapper extends BaseMapper<SoulOptions> {
}
