package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.SoulConclusion;

//  描述详情
public interface SoulConclusionMapper extends BaseMapper<SoulConclusion> {
}
