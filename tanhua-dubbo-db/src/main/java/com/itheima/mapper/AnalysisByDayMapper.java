package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.AnalysisByDay;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AnalysisByDayMapper extends BaseMapper<AnalysisByDay> {

    // 累计用户熟练
    @Select("SELECT SUM(num_registered) FROM `tb_analysis_by_day`")
    Long findCumulativeUsers();

    // 过去7天/30天活跃用户数量
    @Select("SELECT SUM(`num_active`) FROM `tb_analysis_by_day` WHERE `record_date` BETWEEN #{pastday} AND #{today}")
    Long findActiveUsers(@Param("pastday") String pastday,@Param("today") String today);

    //查询新增用户数
    @Select("SELECT num_registered FROM tb_analysis_by_day WHERE record_date=#{today}\n" +
            "ORDER BY created DESC LIMIT 0,1")
    Long findNumRegistered(String today);

    //查询活跃用户数
    @Select("SELECT num_active FROM tb_analysis_by_day WHERE record_date=#{today}\n" +
            "ORDER BY created DESC LIMIT 0,1")
    Long findActiveUsers2(String today);

    //查询次日留存数
    @Select("SELECT num_retention1d FROM tb_analysis_by_day WHERE record_date=#{today}\n" +
            "ORDER BY created DESC LIMIT 0,1")
    Long findNumRetention1d(String today);


}