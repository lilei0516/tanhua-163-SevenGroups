package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.SoulQuestionnaire;

// 问卷详情
public interface SoulQuestionnaireMapper extends BaseMapper<SoulQuestionnaire> {
}
