package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.Jd;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-13 11:11
 * @version:
 */
public interface JdMapper extends BaseMapper<Jd> {
}
