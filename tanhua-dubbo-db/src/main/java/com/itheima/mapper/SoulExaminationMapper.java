package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.SoulExamination;

// 题目详情
public interface SoulExaminationMapper extends BaseMapper<SoulExamination> {
}
