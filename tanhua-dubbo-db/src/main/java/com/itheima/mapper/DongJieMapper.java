package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.domain.db.DongJie;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-12 16:13
 * @version:
 */
// 冻结用户
public interface DongJieMapper extends BaseMapper<DongJie> {
}
