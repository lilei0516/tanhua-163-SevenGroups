package com.itheima.job;

import com.itheima.service.db.AnalysisByDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class AnalysisJob {

    // 每一小时执行一次分析功能
    @Autowired
    private AnalysisByDayService analysisByDayService;

     @Scheduled(cron = "0 0 0/1 * * ?")
    // @Scheduled(cron = "0/30 * * * * ?")
    public void analysisByDay(){
        System.out.println("日志统计分析开始");
        analysisByDayService.logToAnalysisByDay();
        System.out.println("日志统计分析" +
                "结束");
    }

}
