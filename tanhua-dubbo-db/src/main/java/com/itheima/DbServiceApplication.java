package com.itheima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("com.itheima.mapper")
@SpringBootApplication(exclude = MongoAutoConfiguration.class)
@EnableScheduling  // 开启定时任务功能
public class DbServiceApplication {
    // dev分支提交测试
    public static void main(String[] args) {
        SpringApplication.run(DbServiceApplication.class,args);
    }
}
