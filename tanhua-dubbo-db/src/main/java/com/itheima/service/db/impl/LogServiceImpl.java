package com.itheima.service.db.impl;

import com.itheima.domain.db.Log;
import com.itheima.mapper.LogMapper;
import com.itheima.service.db.LogService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public void save(Log log) {
        // 将数据插入到数据库log表中
        logMapper.insert(log);
    }
}
