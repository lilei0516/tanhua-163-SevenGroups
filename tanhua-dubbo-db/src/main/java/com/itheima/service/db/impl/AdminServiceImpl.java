package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Admin;
import com.itheima.mapper.AdminMapper;
import com.itheima.service.db.AdminService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    // 从数据库中查询管理员账号
    @Override
    public Admin getAdminByUsername(String username) {
        // 创建条件对象
        QueryWrapper<Admin> qw = new QueryWrapper<>();
        qw.eq("username",username);
        // 查询指定数据
        Admin admin = adminMapper.selectOne(qw);
        return admin;
    }

}
