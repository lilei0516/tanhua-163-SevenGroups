package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.UserInfo;
import com.itheima.mapper.UserInfoMapper;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    //将userinfo存入数据库
    @Override
    public void save(UserInfo userInfo) {
        userInfoMapper.insert(userInfo);
    }

    //补充头像
    @Override
    public void addPhoto(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);
    }

    //查询用户详细信息
    @Override
    public UserInfo findUserInfoById(Long id) {
        UserInfo userInfo = userInfoMapper.selectById(id);
        return userInfo;
    }

    //用户信息更新
    @Override
    public void update(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);
    }
    //分页查询用户详细信息
    @Override
    public PageBeanVo findUserByPage(Integer pageNum, Integer pageSize) {
        Page page = new Page<>(pageNum,pageSize);
        // 分页查询userinfo
        Page selectPage = userInfoMapper.selectPage(page, null);
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum, pageSize, selectPage.getTotal(), selectPage.getRecords());
        return pageBeanVo;
    }


    // app探花卡片功能查询个人信息
    @Override
    public UserInfo findCards(Long id, Long[] notId) {

        // 1.设置条件（notId的数组里面存储的登录人好友的id，用来排除推荐用户中的好友）
        QueryWrapper<UserInfo> qw = new QueryWrapper<>();
        qw.eq("id",id);
        qw.notIn("id",notId);

        UserInfo userInfo = userInfoMapper.selectOne(qw);
        return userInfo;
    }
}
