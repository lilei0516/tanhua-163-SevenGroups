package com.itheima.service.db.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.DongJie;
import com.itheima.domain.db.UserInfo;
import com.itheima.mapper.DongJieMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.DongJieService;
import com.itheima.service.db.UserInfoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-11 16:50
 * @version:
 */
@DubboService
public class DongJieServiceImpl implements DongJieService {

    @Autowired
    private DongJieMapper dongJieMapper;

    @DubboReference
    private UserInfoService userInfoService;

    // 冻结用户
    @Override
    public void dongJie(DongJie dongJie) {
        // 1.判断用户是否存在
        //Long userId = dongJie.getUserId();
        //QueryWrapper<DongJie> qw = new QueryWrapper<>();
        //qw.eq("user_id",userId);
        //DongJie dongJie1 = dongJieMapper.selectOne(qw);
        // 2.判断userId是否已经被加入
        //if (dongJie1 != null) {  // 存在，修改
        //dongJieMapper.updateById(dongJie);
        //}else {     // 不存在，新增
        //dongJie1 = new DongJie();
        dongJieMapper.insert(dongJie);
        UserInfo userInfo = userInfoService.findUserInfoById(dongJie.getUserId());
        userInfo.setUserStatus("2");
        userInfoService.update(userInfo);
    //}
    }

    // 更新冻结表
    @Override
    public void update(DongJie dongJie) {
        // 设置查询条件
        QueryWrapper<DongJie> qw = new QueryWrapper<>();
        qw.eq("user_id",dongJie.getUserId());
        dongJieMapper.update(dongJie,qw);
    }

    // 根据用户id查询冻结表
    @Override
    public List<DongJie> findDjById(Long userId) {
        QueryWrapper<DongJie> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        List<DongJie> dongJies = dongJieMapper.selectList(qw);
        return dongJies;
    }
}
