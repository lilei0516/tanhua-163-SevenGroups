package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.BlackList;
import com.itheima.mapper.BlackListMapper;
import com.itheima.service.db.BlackListService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class BlackListServiceImpl implements BlackListService {

    @Autowired
    private BlackListMapper blackListMapper;

    //分页查询黑名单
    @Override
    public PageBeanVo getBlackList(Long userId,Integer pageNum, Integer pageSize) {
        //设置分页查询
        Page<BlackList> page = new Page<>(pageNum,pageSize);
        //设置查询条件
        QueryWrapper<BlackList> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        Page<BlackList> blackListPage = blackListMapper.selectPage(page, qw);
        //封装pageBean
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum, pageSize, blackListPage.getTotal(), blackListPage.getRecords());
        return pageBeanVo;
    }

    //删除黑名单中的指定用户
    @Override
    public void deleteOne(Long userId, Long blackUId) {
        //创建条件对象
        QueryWrapper<BlackList> qw = new QueryWrapper<>();
        qw.eq("user_id",userId);
        qw.eq("black_user_id",blackUId);
        blackListMapper.delete(qw);
    }
}
