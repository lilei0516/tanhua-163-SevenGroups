package com.itheima.service.db.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.User;
import com.itheima.domain.db.UserInfo;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;

@DubboService
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Long save(User user) {
        //用户输入了手机号和密码
        //对密码进行加密
        String md5Pwd = SecureUtil.md5(user.getPassword());
        user.setPassword(md5Pwd);
        //设置创建时间和修改时间
        // user.setCreated(new Date());
        // user.setUpdated(new Date());
        userMapper.insert(user);
        //mp主键自动返回
        return user.getId();
    }

    @Override
    public User findUserByPhone(String phone) {
        //创建查询对象
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        //设置条件
        userQueryWrapper.eq("phone",phone);
        //查询
        User user = userMapper.selectOne(userQueryWrapper);
        return user;
    }



    // 根据用户id查询手机号
    @Override
    public User findUserById(Long userId) {
        // 1.构建条件
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("id",userId);
        // 2.查询数据并返回
        User user = userMapper.selectOne(qw);
        return user;
    }


    // 修改手机号
    @Override
    public void updatePhone(User user) {
        // 1.修改
        userMapper.updateById(user);
    }


}
