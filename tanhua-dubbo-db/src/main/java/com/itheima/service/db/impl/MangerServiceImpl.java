package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Admin;
import com.itheima.domain.db.SoulExamination;
import com.itheima.domain.db.SoulOptions;
import com.itheima.domain.db.SoulQuestionnaire;
import com.itheima.mapper.AdminMapper;
import com.itheima.mapper.SoulExaminationMapper;
import com.itheima.mapper.SoulOptionsMapper;
import com.itheima.mapper.SoulQuestionnaireMapper;
import com.itheima.service.db.ManagerService;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SoulExaminationVoW;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Author wzh
 * @Date 2021/11/12 15:24
 * @Version 1.0
 */
@DubboService
public class MangerServiceImpl implements ManagerService {
    @Autowired
    SoulExaminationMapper soulExaminationMapper;
    @Autowired
    SoulQuestionnaireMapper soulQuestionnaireMapper;
    @Autowired
    private SoulOptionsMapper soulOptionsMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Override
    public PageBeanVo findTestByPage(Integer pageNum, Integer pageSize,Integer lev) {
        Page<SoulExamination> page = new Page<>(pageNum,pageSize);
        //查询
        QueryWrapper<SoulExamination> qw = new QueryWrapper<>();
        if (lev!=null){
            qw.eq("questionnaire_id",lev);
        }
        page = soulExaminationMapper.selectPage(page,qw);
        //返回
        return new PageBeanVo(pageNum,pageSize,page.getTotal(),page.getRecords());
    }
    //根据questionnaire_id查询题目等级
    @Override
    public SoulQuestionnaire findById(Long id) {
        SoulQuestionnaire soulQuestionnaire = soulQuestionnaireMapper.selectById(id);
        return soulQuestionnaire;
    }
    //根据题目id删除对应题目
    @Override
    public void deleteByQuestionId(Long id) {
        soulExaminationMapper.deleteById(id);
        //清空其对应的所有选项
        QueryWrapper<SoulOptions> qw = new QueryWrapper<>();
        qw.eq("examination_id",id);
        soulOptionsMapper.delete(qw);
    }
    //查看题目详情
    @Override
    public SoulExaminationVoW findQuestionDetail(Long id) {
        SoulExaminationVoW voW = new SoulExaminationVoW();
        //首先查看题目等级
        SoulExamination soulExamination = soulExaminationMapper.selectById(id);
        SoulQuestionnaire soulQuestionnaire = findById(soulExamination.getQuestionnaireId());
        voW.setLevel(soulQuestionnaire.getLevel());//题目等级
        //查看题目信息

        voW.setQuestion(soulExamination.getQuestion());//题目
        voW.setId(soulExamination.getId());//题目id
        //查询指定题目对应选项集合
        QueryWrapper<SoulOptions> qw = new QueryWrapper<>();
        qw.eq("examination_id",id);
        //查询
        List<SoulOptions> soulOptionsList = soulOptionsMapper.selectList(qw);
        voW.setOptionList(soulOptionsList);//封装对应题目选项集合
        return voW;
    }
    //修改题目
    @Override
    public void updateQuestion(SoulExamination soulExamination) {
        soulExaminationMapper.updateById(soulExamination);
    }
    //修改选项内容
    @Override
    public void updateOption(SoulOptions soulOptions) {
        soulOptionsMapper.updateById(soulOptions);
    }
    //删除选项
    @Override
    public void deleteOption(Long id) {
        soulOptionsMapper.deleteById(id);
    }
    //添加选项
    @Override
    public void saveOption(SoulOptions soulOptions) {
        soulOptionsMapper.insert(soulOptions);
    }
    //添加测试题目
    @Override
    public void saveTest(SoulExamination soulExamination) {
        soulExaminationMapper.insert(soulExamination);
    }

}
