package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Question;
import com.itheima.mapper.QuestionMapper;
import com.itheima.service.db.QuestionService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    // 根据用户id查询question
    @Override
    public Question findQuestionById(Long id) {
        QueryWrapper<Question> qw = new QueryWrapper<>();
        qw.eq("user_id",id);
        Question question = questionMapper.selectOne(qw);
        return question;
    }

    // 保存question
    @Override
    public void save(Question question) {
        questionMapper.insert(question);
    }

    // 更新question
    @Override
    public void update(Question question) {
        questionMapper.updateById(question);
    }
}
