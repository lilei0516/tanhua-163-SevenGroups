package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Notification;
import com.itheima.mapper.NotificationMapper;
import com.itheima.service.db.NotificationService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationMapper notificationMapper;

    // 查询指定用户通知设置
    @Override
    public Notification findNotificationById(Long id) {
        QueryWrapper<Notification> qw = new QueryWrapper<>();
        qw.eq("user_id",id);
        Notification notification = notificationMapper.selectOne(qw);
        return notification;
    }

    // 保存通知设置
    @Override
    public void save(Notification notification) {
        notificationMapper.insert(notification);
    }

    // 更新通知设置
    @Override
    public void update(Notification notification) {
        notificationMapper.updateById(notification);
    }
}
