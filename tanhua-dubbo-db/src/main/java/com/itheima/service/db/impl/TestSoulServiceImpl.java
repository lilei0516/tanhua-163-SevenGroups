package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.SoulConclusion;
import com.itheima.domain.db.SoulExamination;
import com.itheima.domain.db.SoulOptions;
import com.itheima.domain.db.SoulQuestionnaire;
import com.itheima.mapper.SoulConclusionMapper;
import com.itheima.mapper.SoulExaminationMapper;
import com.itheima.mapper.SoulOptionsMapper;
import com.itheima.mapper.SoulQuestionnaireMapper;
import com.itheima.service.db.TestSoulService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@DubboService
public class TestSoulServiceImpl implements TestSoulService {

    @Autowired
    private SoulQuestionnaireMapper questionnaireMapper;

    @Autowired
    private SoulExaminationMapper examinationMapper;

    @Autowired
    private SoulOptionsMapper optionsMapper;

    @Autowired
    private SoulConclusionMapper conclusionMapper;

    // 拿到问卷列表
    @Override
    public List<SoulQuestionnaire> findQuestionnaire() {
        // 创建条件
        QueryWrapper<SoulQuestionnaire> qw = new QueryWrapper<>();
        // 查询问卷数据库
        List<SoulQuestionnaire> questionnaireList = questionnaireMapper.selectList(qw);
        return questionnaireList;
    }

    // 拿到试题列表
    @Override
    public List<SoulExamination> findExamination(Long questionnaireId) {
        // 创建条件
        QueryWrapper<SoulExamination> qw = new QueryWrapper<>();
        qw.eq("questionnaire_id",questionnaireId);
        List<SoulExamination> examinationList = examinationMapper.selectList(qw);
        return examinationList;
    }

    // 拿到选项列表
    @Override
    public List<SoulOptions> findOptions(Long examinationId) {
        // 创建条件
        QueryWrapper<SoulOptions> qw = new QueryWrapper<>();
        qw.eq("examination_id",examinationId);
        List<SoulOptions> optionsList = optionsMapper.selectList(qw);
        return optionsList;
    }

    // 查询这个题的得分
    @Override
    public Integer findScore(String questionId, String optionId) {
        // 创建条件
        QueryWrapper<SoulOptions> qw = new QueryWrapper<>();
        qw.eq("examination_id",questionId);
        qw.eq("id",optionId);
        // 执行查询
        SoulOptions option = optionsMapper.selectOne(qw);
        return option.getScore();
    }

    @Override
    // 查询试卷id
    public Long findQuestionnaireId(String questionId) {
        SoulExamination examination = examinationMapper.selectById(questionId);
        return examination.getQuestionnaireId();
    }

    // 查询描述id
    @Override
    public Long findConclusionId(Integer totalScore) {
        // SELECT * FROM `tb_soul_conclusion` WHERE min_score <= 50  AND max_score>=50
        QueryWrapper<SoulConclusion> qw = new QueryWrapper<>();
        // 小于等于
        qw.le("min_score",totalScore);
        // 大于等于
        qw.ge("max_score",totalScore);
        // 创建条件
        SoulConclusion conclusion = conclusionMapper.selectOne(qw);
        // 返回描述id
        return conclusion.getId();
    }

    // 根据id查询描述表
    @Override
    public SoulConclusion findConclusionById(Long conclusionId) {
        SoulConclusion soulConclusion = conclusionMapper.selectById(conclusionId);
        return soulConclusion;
    }
}
