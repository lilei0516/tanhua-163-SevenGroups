package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.domain.db.Announcement;
import com.itheima.mapper.AnnouncementMapper;
import com.itheima.service.db.AnnouncementService;
import com.itheima.vo.PageBeanVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class AnnouncementServiceImpl implements AnnouncementService {

    @Autowired
    private AnnouncementMapper announcementMapper;

    // 查询mysql中的公告表
    @Override
    public PageBeanVo findAnnouncementsByPage(Integer pageNum, Integer pageSize) {
        // 创建分页对象
        Page<Announcement> page = new Page<>(pageNum,pageSize);
        // 创建条件对象
        QueryWrapper<Announcement> qw = new QueryWrapper<>();
        qw.orderByDesc("created");
        // 执行sql
        Page<Announcement> announcementPage = announcementMapper.selectPage(page, qw);
        // 设置pbv
        PageBeanVo pageBeanVo = new PageBeanVo(pageNum,pageSize,announcementPage.getTotal(),announcementPage.getRecords());
        return pageBeanVo;
    }
}
