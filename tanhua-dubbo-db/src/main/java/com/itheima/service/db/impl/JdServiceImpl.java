package com.itheima.service.db.impl;

import com.itheima.domain.db.Jd;
import com.itheima.mapper.JdMapper;
import com.itheima.service.db.JdService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Description:
 *
 * @author: 巩乾豪
 * @CreateTime: 2021-11-13 11:16
 * @version:
 */
@DubboService
public class JdServiceImpl implements JdService {

    @Autowired
    private JdMapper jdMapper;

    @Override
    public void jiedong(Jd jd) {
        jdMapper.insert(jd);
    }
}
