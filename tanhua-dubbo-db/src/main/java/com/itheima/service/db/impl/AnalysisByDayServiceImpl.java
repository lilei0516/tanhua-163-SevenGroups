package com.itheima.service.db.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.AnalysisByDay;
import com.itheima.mapper.AnalysisByDayMapper;
import com.itheima.mapper.LogMapper;
import com.itheima.service.db.AnalysisByDayService;
import com.itheima.util.ComputeUtil;
import com.itheima.vo.AnalysisSummaryVo;
import com.itheima.vo.DataVo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@DubboService
public class AnalysisByDayServiceImpl implements AnalysisByDayService {

    @Autowired
    private LogMapper logMapper;

    @Autowired
    private AnalysisByDayMapper analysisByDayMapper;

    // 数据分析
    @Override
    public void logToAnalysisByDay() {
        // 准备基础数据
        // 今天
        String today = DateUtil.offsetDay(new Date(),0).toDateStr();
        // 昨天
        String yesterday = DateUtil.offsetDay(new Date(),-1).toDateStr();
        // 今天的注册人数
        String numRegistered = logMapper.findNumRegisteredOrNumLogin("0102",today);
        // 今日的活跃
        String numActive = logMapper.findNumActive(today);
        // 今天的登录用户数
        String numLogin = logMapper.findNumRegisteredOrNumLogin("0101",today);
        // 次日留存
        String numRetention1d = logMapper.findNumRetention1d(today,yesterday);

        // 先去查询表里有没有今天的数据
        QueryWrapper<AnalysisByDay> qw = new QueryWrapper<>();
        qw.eq("record_date",today);
        AnalysisByDay analysisByDay = analysisByDayMapper.selectOne(qw);

        if (analysisByDay==null) {
            // 没有今天的记录，新增
            AnalysisByDay analysisByDay1 = new AnalysisByDay();
            analysisByDay1.setRecordDate(new Date());
            analysisByDay1.setNumRegistered(Integer.parseInt(numRegistered));
            analysisByDay1.setNumActive(Integer.parseInt(numActive));
            analysisByDay1.setNumLogin(Integer.parseInt(numLogin));
            analysisByDay1.setNumRetention1d(Integer.parseInt(numRetention1d));
            // 新增到数据库
            analysisByDayMapper.insert(analysisByDay1);

        }else{
            // 有今天的记录，更新
            analysisByDay.setNumRegistered(Integer.parseInt(numRegistered));
            analysisByDay.setNumActive(Integer.parseInt(numActive));
            analysisByDay.setNumLogin(Integer.parseInt(numLogin));
            analysisByDay.setNumRetention1d(Integer.parseInt(numRetention1d));
            // 更新到库
            analysisByDayMapper.updateById(analysisByDay);
        }

    }

    @Override
    public AnalysisSummaryVo summary() {
        // 基础数据
        // 今天日期
        String today = DateUtil.offsetDay(new Date(),0).toDateStr();
        // 昨天日期
        String yesterday = DateUtil.offsetDay(new Date(),-1).toDateStr();
        // 前七天
        String past7Day = DateUtil.offsetDay(new Date(),-7).toDateStr();
        // 前30天
        String past30Day = DateUtil.offsetDay(new Date(),-30).toDateStr();
        // 今日内容
        QueryWrapper<AnalysisByDay> qw1 = new QueryWrapper<>();
        qw1.eq("record_date",today);
        AnalysisByDay todayData = analysisByDayMapper.selectOne(qw1);
        // 昨天内容
        QueryWrapper<AnalysisByDay> qw2 = new QueryWrapper<>();
        qw2.eq("record_date",yesterday);
        AnalysisByDay yesterdayData = analysisByDayMapper.selectOne(qw2);

        // 设置返回前端数据
        AnalysisSummaryVo analysisSummaryVo = new AnalysisSummaryVo();
        // 累计用户
        analysisSummaryVo.setCumulativeUsers(analysisByDayMapper.findCumulativeUsers());
        // 过去30天活跃用户数
        analysisSummaryVo.setActivePassMonth(analysisByDayMapper.findActiveUsers(past30Day,today));
        // 过去7天活跃用户
        analysisSummaryVo.setActivePassWeek(analysisByDayMapper.findActiveUsers(past7Day,today));
        // 今日新增用户数量
        analysisSummaryVo.setNewUsersToday(todayData.getNumRegistered().longValue());
        // 今日新增用户涨跌率，单位百分数，正数为涨，负数为跌   BigDecimal : 商业数字格式
        analysisSummaryVo.setNewUsersTodayRate(ComputeUtil.computeRate(todayData.getNumRegistered(),yesterdayData.getNumRegistered()));
        // 今日登录次数
        analysisSummaryVo.setLoginTimesToday(todayData.getNumLogin().longValue());
        // 今日登录次数涨跌率，单位百分数，正数为涨，负数为跌
        analysisSummaryVo.setLoginTimesTodayRate(ComputeUtil.computeRate(todayData.getNumLogin(),yesterdayData.getNumLogin()));
        // 今日活跃用户数量
        analysisSummaryVo.setActiveUsersToday(todayData.getNumActive().longValue());
        // 今日活跃用户涨跌率，单位百分数，正数为涨，负数为跌
        analysisSummaryVo.setActiveUsersTodayRate(ComputeUtil.computeRate(todayData.getNumActive(),yesterdayData.getNumActive()));
        return analysisSummaryVo;
    }

    //查找数据
    @Override
    public Object[] findData(String startTime, String endTime, Integer type) {
        //声明集合
        List<DataVo> voList=new ArrayList<>();
        //循环
        while (DateUtil.parse(startTime).getTime()<DateUtil.parse(endTime).getTime()) {//开始时间小于结束时间
            //创建vo
            DataVo vo = new DataVo();
            //1.封装数据
            switch (type) {
                case 101:{//查询新增数量
                    Long cumulativeUsers = analysisByDayMapper.findNumRegistered(startTime);
                    vo.setTitle(startTime);
                    vo.setAmount(cumulativeUsers);
                    //添加到集合
                    voList.add(vo);
                    break;
                }
                case 102:{//查询活跃用户数量
                    Long activeUsers = analysisByDayMapper.findActiveUsers2(startTime);
                    vo.setTitle(startTime);
                    vo.setAmount(activeUsers);
                    //添加到集合
                    voList.add(vo);
                    break;
                }
                case 103:{//查询今日留存数
                    Long numRetention1d = analysisByDayMapper.findNumRetention1d(startTime);
                    vo.setTitle(startTime);
                    vo.setAmount(numRetention1d);
                    //添加到集合
                    voList.add(vo);
                    break;
                }
            }
            //2.时间偏移后一天
            startTime = DateUtil.offsetDay(DateUtil.parse(startTime), 1).toDateStr();
        }
        //把集合变为数组,并返回结果
        Object[] arr = voList.toArray();
        return arr;
    }
}
