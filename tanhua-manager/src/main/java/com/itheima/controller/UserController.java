package com.itheima.controller;

import cn.hutool.captcha.LineCaptcha;
import com.itheima.domain.db.SoulExamination;
import com.itheima.domain.db.SoulOptions;
import com.itheima.manager.UserManager;
import com.itheima.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * @Author wzh
 * @Date 2021/10/12 11:43
 * @Version 1.0
 */

@RestController
@RequestMapping("/manager")
public class UserController {
   @Autowired
   private UserManager userManager;
    //发验证码
    @GetMapping("/sendSms")
    public void sendSms(HttpServletResponse response) throws IOException {
       //调用manager
        LineCaptcha lineCaptcha = userManager.sendSms();
        lineCaptcha.write(response.getOutputStream());//返回验证码
        //return "ok";
    }
    //登录
    @PostMapping ("/login")
    public ResponseEntity login(@RequestBody Map<String,String> param){
        //获得参数
        String username = param.get("username");//用户的手机号
        String password = param.get("password");//用户输入的密码
        String code = param.get("code");//用户输入的验证码
        //调用manager
        return userManager.login(username,password,code);
    }
    //查询用户详细列表
    @GetMapping("/findByPage")
    public ResponseEntity findByPageUser(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                         @RequestParam(value = "pageSize" ,defaultValue = "10") Integer pageSize){
        //调用manager
        return userManager.findByPageUser(pageNum,pageSize);
    }
    //查看用户的信息及地理位置
    @GetMapping("/findById")
    public ResponseEntity findByUserId(Long userId){
        //调用manager
      return  userManager.findByUserId(userId);
    }
    //分页查询测灵魂题目
    @GetMapping("/findTestByPage")
    public ResponseEntity findTestByPage(@RequestParam(value = "pagenum",defaultValue = "1")Integer pageNum,
                                         @RequestParam(value = "pagesize" ,defaultValue = "10") Integer pageSize,
                                         String level){
        //转换类型
        Integer lev =null;
        if (level!=null&&level.length()>0){
            lev = Integer.parseInt(level);
        }

        //调用manager
       return userManager.findTestByPage(pageNum,pageSize,lev);
    }
    //添加测灵魂题目
    @PostMapping("/saveTest")
    public void saveTest(@RequestBody SoulExamination soulExamination){
        //调用manager
        userManager.saveTest(soulExamination);
    }
    //根据题目id删除对应题目  单条记录删除
    @GetMapping("/deleteById")
    public String deleteByQuestionId(Long id){
        //调用manager
        userManager.deleteByQuestionId(id);
        return "ok";
    }

    //多条记录删除
    @PostMapping("/matchDelete")
    public String matchDelete(Long[] ids){
        System.out.println(Arrays.toString(ids));
        //调用maanger
        userManager.matchDelete(ids);
        return "ok";
    }
    //查看题目及选项详情  根据题目id
    @GetMapping("/findQuestionDetail")
    public ResponseEntity findQuestionDetail(Long id){
     //调用manager
    return userManager.findQuestionDetail(id);
    }
    //修改题目
    @PostMapping("/updateQuestion")
    public void updateQuestion(@RequestBody SoulExamination soulExamination){
        //调用manager
        userManager.updateQuestion(soulExamination);
    }
    //添加选项
    @PostMapping("/saveOption")
    public void saveOption(@RequestBody SoulOptions soulOptions){
        //调用manager
        userManager.saveOption(soulOptions);
    }
    //修改选项
    @PostMapping("/updateOption")
    public void updateOption(@RequestBody SoulOptions soulOptions){
        //调用manager
        userManager.updateOption(soulOptions);
    }
    //删除选项
    @GetMapping("/deleteOption")
    public void deleteOptions(Long id){
        //调用manager
        userManager.deleteOption(id);
    }
   /* //修改管理员密码
    @GetMapping("/updatePW")
    public void updatePW(String newPw){
        //调用manager
        userManager.updatePW(newPw);
    }*/
    //令牌验证
    @GetMapping("/verify")
    public String verify(@RequestHeader("Authorization") String token){
        //解析令牌
        try {
            Map map = JwtUtil.parseToken(token);
            System.out.println(map);
        } catch (Exception e) {
            e.printStackTrace();
            return "401";//权限不足
        }
        return "ok";
    }
   /* //根据用户id 查找用户地理位置
    @GetMapping("/findLocation")
    public ResponseEntity findLocation(Long userId){
        //调用manager
       return userManager.findLocation(userId);
    }*/
}
