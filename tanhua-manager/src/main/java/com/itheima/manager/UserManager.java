package com.itheima.manager;

import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import com.itheima.domain.db.*;
import com.itheima.service.db.AdminService;
import com.itheima.service.db.ManagerService;
import com.itheima.service.db.UserInfoService;
import com.itheima.service.mongo.LocationService;
import com.itheima.util.ConstantUtil;
import com.itheima.util.JwtUtil;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SoulExaminationVoW;
import com.itheima.vo.UserLocationWithInfoVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author wzh
 * @Date 2021/11/11 18:09
 * @Version 1.0
 */
@Component
public class UserManager {
    @DubboReference
    private AdminService adminService;
    @DubboReference
    private UserInfoService userInfoService;
    @DubboReference
    private LocationService locationService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    //生成验证码
    public LineCaptcha sendSms(){
        //生成随机六位验证码
        /*String randomCode = RandomUtil.randomNumbers(6);
        randomCode = "123456";//写死验证码*/
        LineCaptcha lineCaptcha = new LineCaptcha(200,100);
        //将代码存入redis中,并设置两分钟后自动销毁
        stringRedisTemplate.opsForValue().set("sms_admin",lineCaptcha.getCode(),2, TimeUnit.MINUTES);
        return lineCaptcha;
    }

    //登录验证
    public ResponseEntity login(String username, String password, String code) {
        //创建返回结果
        Map<String,Object> result = new HashMap<>();
        //根据用户名取出redis中存储的验证码
        String codeFromRedis = stringRedisTemplate.opsForValue().get("sms_" + username);
        //对比验证码
        if (!StrUtil.equals(code,codeFromRedis)) {//验证码不匹配
            result.put("flag",0);
            return ResponseEntity.ok(result);
        }
        //根据用户名查找对应用户账号信息
        Admin admin = adminService.getAdminByUsername(username);
        //判空
        if (admin==null){//用户不存在
            result.put("flag",0);
            return ResponseEntity.ok(result);
        }
        //校验密码
        //对用户输入的密码进行加密
        String md5Pwd = SecureUtil.md5(password);
        //对比密码
        if (!StrUtil.equals(md5Pwd,admin.getPassword())) {//密码不匹配
            result.put("flag",0);
            return ResponseEntity.ok(result);
        }
        //登录成功，删除验证码
        stringRedisTemplate.delete("sms_"+username);
        //清除敏感数据
        admin.setPassword("");
        Map<String, Object> claims = BeanUtil.beanToMap(admin);
        //创建token
        String token = JwtUtil.createToken(claims);
        //将admin存入redis
        String json = JSON.toJSONString(admin);
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN+token,json,Duration.ofDays(7));
        //返回
        result.put("flag",1);
        result.put("token",token);
        return ResponseEntity.ok(result);
    }
    //查询用户详细列表
    public ResponseEntity findByPageUser(Integer pageNum, Integer pageSize) {
        PageBeanVo pageBeanVo = userInfoService.findUserByPage(pageNum, pageSize);
        return ResponseEntity.ok(pageBeanVo);
    }
    //查看用户的信息及地理位置
    public ResponseEntity findByUserId(Long userId) {
        //查询用户信息
        UserInfo userInfo = userInfoService.findUserInfoById(userId);
        //查询用户地理位置信息vo
        UserLocationWithInfoVo vo = locationService.findByUserId(userId);
        //封装
        vo.setUserId(userId);//用户id
        vo.setNickname(userInfo.getNickname());//用户昵称
        vo.setGender(userInfo.getGender());//用户性别
        vo.setCity(userInfo.getCity());//用户所在城市
        vo.setAvatar(userInfo.getAvatar());//用户头像
        //返回vo
        return ResponseEntity.ok(vo);
    }
    @DubboReference
    private ManagerService managerService;
    //分页查询测灵魂题目
    public ResponseEntity findTestByPage(Integer pageNum, Integer pageSize,Integer lev) {
        PageBeanVo pageBeanVo = managerService.findTestByPage(pageNum, pageSize,lev);
        //获得pageBeanVo的items
        List<SoulExamination> soulExaminationList = (List<SoulExamination>) pageBeanVo.getItems();
        //新建返回vo
        List<SoulExaminationVoW> voWList = new ArrayList<>();
        //遍历
        if (CollectionUtil.isNotEmpty(soulExaminationList)) {
            for (SoulExamination soulExamination : soulExaminationList) {
                //根据questionnaire_id查询题目等级
                SoulQuestionnaire soulQuestionnaire = managerService.findById(soulExamination.getQuestionnaireId());
                SoulExaminationVoW vo = new SoulExaminationVoW();
                vo.setId(soulExamination.getId());//题目id
                vo.setLevel(soulQuestionnaire.getLevel());//题目等级
                vo.setQuestion(soulExamination.getQuestion());//题目
                //添加集合
                voWList.add(vo);
            }
        }
         pageBeanVo.setItems(voWList);
        return ResponseEntity.ok(pageBeanVo);
    }
    //根据题目id删除对应题目
    public void deleteByQuestionId(Long id) {
        managerService.deleteByQuestionId(id);
    }
    //多条记录删除
    public void matchDelete(Long[] ids) {
        //遍历删除
        for (Long id : ids) {
            managerService.deleteByQuestionId(id);
        }
    }
    //查看题目及选项详情  根据题目id
    public ResponseEntity findQuestionDetail(Long id) {
        //远程调用
        SoulExaminationVoW voW = managerService.findQuestionDetail(id);
        //返回
        return ResponseEntity.ok(voW);
    }
    //根据id修改题目
    public void updateQuestion(SoulExamination soulExamination) {
        soulExamination.setUpdated(new Date());//修改时间
        //远程调用
        managerService.updateQuestion(soulExamination);
    }
    //修改选项内容
    public void updateOption(SoulOptions soulOptions) {
        soulOptions.setUpdated(new Date());//修改时间
        managerService.updateOption(soulOptions);
    }
    //删除选项
    public void deleteOption(Long id) {
        //远程调用
        managerService.deleteOption(id);
    }
    //添加选项
    public void saveOption(SoulOptions soulOptions) {
        soulOptions.setCreated(new Date());//设置创建时间
        soulOptions.setUpdated(new Date());//设置更新时间
        //远程调用
        managerService.saveOption(soulOptions);
    }
    //添加测灵魂题目
    public void saveTest(SoulExamination soulExamination) {
        //封装
        soulExamination.setCreated(new Date());
        soulExamination.setUpdated(new Date());
        //远程调用
        managerService.saveTest(soulExamination);
    }
    // 管理员信息查询
    public Admin getProfile(String token) {
       /* // 通过token获取userid
        token = token.replaceAll("Bearer ", "");*/
        String adminJson = stringRedisTemplate.opsForValue().get(ConstantUtil.ADMIN_TOKEN + token);
        // 判空
        if (adminJson == null) {
            return null;
        }
        Admin admin = JSON.parseObject(adminJson, Admin.class);
        // 账户续期
        stringRedisTemplate.opsForValue().set(ConstantUtil.ADMIN_TOKEN+token,adminJson, Duration.ofDays(7));
        return admin;
    }
   /* //根据用户id 查找用户地理位置
    public ResponseEntity findLocation(Long id) {
        //远程调用
        Map<String, Double> userMap = locationService.findLocation(id);
        return ResponseEntity.ok(userMap);
    }*/
    /*//修改管理员密码
    public void updatePW(String newPw) {
    }*/
}
