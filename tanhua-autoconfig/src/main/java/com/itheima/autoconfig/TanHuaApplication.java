package com.itheima.autoconfig;

import com.itheima.autoconfig.face.AipFaceProperties;
import com.itheima.autoconfig.face.AipFaceTemplate;
import com.itheima.autoconfig.huanxin.HuanXinProperties;
import com.itheima.autoconfig.huanxin.HuanXinTemplate;
import com.itheima.autoconfig.lvwang.GreenProperties;
import com.itheima.autoconfig.lvwang.GreenTemplate;
import com.itheima.autoconfig.oss.OssProperties;
import com.itheima.autoconfig.oss.OssTemplate;
import com.itheima.autoconfig.sms.SmsProperties;
import com.itheima.autoconfig.sms.SmsTemplate;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        SmsProperties.class,
        OssProperties.class,
        AipFaceProperties.class,
        HuanXinProperties.class,
        GreenProperties.class
})
public class TanHuaApplication {

    @Bean
    public SmsTemplate getSmsTemplate(SmsProperties smsProperties){
        return new SmsTemplate(smsProperties);
    }

    @Bean
    public OssTemplate getOssTemplate(OssProperties ossProperties){
        return new OssTemplate(ossProperties);
    }

    @Bean
    public AipFaceTemplate getAipFaceTemplate(AipFaceProperties aipFaceProperties){
        return new AipFaceTemplate(aipFaceProperties);
    }

    @Bean
    public HuanXinTemplate getHuanXinTemplate(HuanXinProperties huanXinProperties){
        return new HuanXinTemplate(huanXinProperties);
    }

    @Bean
    public GreenTemplate getGreenTemplate(GreenProperties greenProperties){
        return new GreenTemplate(greenProperties);
    }

}
