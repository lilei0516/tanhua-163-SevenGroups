package com.itheima.autoconfig.oss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "tanhua.oss")
public class OssProperties {
    private String accessKey;
    private String secret;
    private String bucketName;
    private String url;
    private String Endpoint;
}
